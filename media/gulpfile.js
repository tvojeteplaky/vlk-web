var gulp = require('gulp')
, uglify = require("gulp-uglify")
, concat = require("gulp-concat");
var $    = require('gulp-load-plugins')();
var sourcemaps = require('gulp-sourcemaps');
var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];
var javascriptPaths = [
  "bower_components/jquery/dist/jquery.js",
  "bower_components/what-input/what-input.js",
  "bower_components/OwlCarousel/owl-carousel/owl.carousel.min.js",
  "bower_components/foundation-sites/dist/foundation.js",
  "bower_components/foundation-datepicker/js/foundation-datepicker.js",
  "bower_components/foundation-datepicker/js/locales/foundation-datepicker.cs.js",
  "./javascript/app.js"
];
gulp.task('sass', function() {
  return gulp.src('scss/app.scss')
    //.pipe(sourcemaps.init())
    .pipe($.sass({
      includePaths: sassPaths,
       outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('css'));
});

gulp.task('default', ['sass','javascript'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(['javascript/*.js'],['javascript']);
});

gulp.task('javascript', function () {
    gulp.src(javascriptPaths) // path to your files
    .pipe(concat('app.min.js')) // concat files
    .pipe(uglify()) // minify files
    .pipe(gulp.dest('./js/'));
});
