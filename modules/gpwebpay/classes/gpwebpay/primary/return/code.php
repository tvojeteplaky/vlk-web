<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 14.9.14
 * Time: 13:21
 */

class Gpwebpay_Primary_Return_Code {
    const CODE_OK = 0;
    const CODE_FIELD_TOO_LONG = 1;
    const CODE_FIELD_TOO_SHORT = 2;
    const CODE_FIELD_INVALID_CONTENT = 3;
    const CODE_FIELD_IS_NULL = 4;
    const CODE_MISSING_REQUIRED_FIELD = 5;
    const CODE_FIELD_NOT_EXISTS = 6;
    const CODE_UNKNOWN_MERCHANT = 11;
    const CODE_DUPLICATE_ORDER_NUMBER = 14;
    const CODE_OBJECT_NOT_FOUND = 15;
    const CODE_AMOUNT_EXCEEDED_APPROVED_DEPOSIT = 17;
    const CODE_TOTAL_SUM_EXCEEDED_DEPOSITED_AMOUNT = 18;
    const CODE_NOT_VALID_STATE = 20;
    const CODE_OPERATION_NOT_ALLOWED = 25;
    const CODE_AC_TECHNICAL_PROBLEM = 26;
    const CODE_INCORRECT_ORDER_TYPE = 27;
    const CODE_3D_DECLINED = 28;
    const CODE_AC_DECLINED = 30;
    const CODE_WRONG_DIGEST = 31;
    const CODE_SESSION_EXPIRED = 35;
    const CODE_PAYMENT_CANCELLED = 50;
    const CODE_TECHNICAL_PROBLEM = 1000;

    /**
     * @param int $code
     * @throws Kohana_Exception
     * @return string
     */
    public static function getMessage($code){
        if (!is_int($code)) {
            throw new Kohana_Exception(__("Code musí být typu int: ").$code);
        } else {
            $code = (int) $code;
        }
        switch($code) {
            case Gpwebpay_Primary_Return_Code::CODE_OK: return __("OK");
            case Gpwebpay_Primary_Return_Code::CODE_FIELD_TOO_LONG: return __("Pole příliš dlouhé");
            case Gpwebpay_Primary_Return_Code::CODE_FIELD_TOO_SHORT: return __("Pole příliš krátké");
            case Gpwebpay_Primary_Return_Code::CODE_FIELD_INVALID_CONTENT: return __("Chybný obsah pole");
            case Gpwebpay_Primary_Return_Code::CODE_FIELD_IS_NULL: return __("Pole je prázdné");
            case Gpwebpay_Primary_Return_Code::CODE_MISSING_REQUIRED_FIELD: return __("Chybí povinné pole");
            case Gpwebpay_Primary_Return_Code::CODE_FIELD_NOT_EXISTS: return __("Pole neexistuje");
            case Gpwebpay_Primary_Return_Code::CODE_UNKNOWN_MERCHANT: return __("Neznámý obchodník");
            case Gpwebpay_Primary_Return_Code::CODE_DUPLICATE_ORDER_NUMBER: return __("Duplikátní číslo objednávky");
            case Gpwebpay_Primary_Return_Code::CODE_OBJECT_NOT_FOUND: return __("Objekt nenalezen");
            case Gpwebpay_Primary_Return_Code::CODE_AMOUNT_EXCEEDED_APPROVED_DEPOSIT: return __("Částka k úhradě překročila autorizovanou částku");
            case Gpwebpay_Primary_Return_Code::CODE_TOTAL_SUM_EXCEEDED_DEPOSITED_AMOUNT: return __("Součet kreditovaných částek překročil uhrazenou částku");
            case Gpwebpay_Primary_Return_Code::CODE_NOT_VALID_STATE: return __("Objekt není ve stavu odpovídajícím této operaci");
            case Gpwebpay_Primary_Return_Code::CODE_OPERATION_NOT_ALLOWED: return __("Uživatel není oprávněn k provedení operace");
            case Gpwebpay_Primary_Return_Code::CODE_AC_TECHNICAL_PROBLEM: return __("Technický problém při spojení s autorizačním centrem");
            case Gpwebpay_Primary_Return_Code::CODE_INCORRECT_ORDER_TYPE: return __("Chybný typ objednávky");
            case Gpwebpay_Primary_Return_Code::CODE_3D_DECLINED: return __("Zamítnuto v 3D");
            case Gpwebpay_Primary_Return_Code::CODE_AC_DECLINED: return __("Zamítnuto v autorizačním centru");
            case Gpwebpay_Primary_Return_Code::CODE_WRONG_DIGEST: return __("Chybný podpis");
            case Gpwebpay_Primary_Return_Code::CODE_SESSION_EXPIRED: return __("Expirovaná session");
            case Gpwebpay_Primary_Return_Code::CODE_PAYMENT_CANCELLED: return __("Držitel karty zrušil platbu");
            case Gpwebpay_Primary_Return_Code::CODE_TECHNICAL_PROBLEM: return __("Technický problém");
        }

        return __("neočekávaný status code: ").$code;
    }

    /**
     * @param int $prCode
     * @param int $srCode
     * @throws Kohana_Exception
     * @return string
     */
    public static function getFullMessage($prCode, $srCode){
        if (!is_int($prCode) || !is_int($srCode)) {
            throw new Kohana_Exception(__("Kódy musí být typu int: ").$prCode.", ".$srCode);
        } else {
            $prCode = (int) $prCode;
            $srCode = (int) $srCode;
        }

        return Gpwebpay_Primary_Return_Code::getMessage($prCode) . " - ".Gpwebpay_Secondary_Return_Code::getMessage($srCode);
    }
} 