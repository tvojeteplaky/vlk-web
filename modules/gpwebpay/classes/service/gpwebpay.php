<?php defined('SYSPATH') or die('No direct script access.');

require_once dirname(__FILE__) . '/../GpwebpayAutoload.php';

class Service_Gpwebpay
{
    /**
     * @var Kohana_Config
     */
    protected static $config;
    /**
     * @var GpwebpayServiceQuery
     */
    private static $gpwebpayServiceQuery;

    /**
     * @var Gpwebpay_Signature
     */
    private static $gpwebpaySignature;
    /**
     * @return Kohana_Config
     */
    public static function getConfig()
    {
        if ( ! isset(self::$config))
        {
            self::$config = Kohana::config('gpwebpay');
        }

        return self::$config;
    }

    /**
     * @return Gpwebpay_Signature
     */
    public static function getGpwebpaySignature()
    {
        if ( ! isset(self::$gpwebpaySignature))
        {
            self::$gpwebpaySignature = new Gpwebpay_Signature(self::getPrivateKeyWeb(), self::getPrivateKeyWebPassword(), self::getPublicKeyGpwebpay());
        }
        return self::$gpwebpaySignature;
    }

    /**
     * @return string
     */
    public static function getWsdlUrl() {
        $conf =  Service_Gpwebpay::getConfig();
        return $conf != null && $conf["GPWEBPAY_WSDL"] != null ? $conf["GPWEBPAY_WSDL"] : "";
    }

    /**
     * @return string
     */
    public static function getPrivateKeyWeb() {
        $conf =  Service_Gpwebpay::getConfig();
        return ($conf != null && $conf["PRIVATE_KEY_WEB"] != null) ? $conf["PRIVATE_KEY_WEB"] : "";
    }

    /**
     * @return string
     */
    public static function getPublicKeyGpwebpay() {
        $conf =  Service_Gpwebpay::getConfig();
        return ($conf != null && $conf["PUBLIC_KEY_GPWEBPAY"] != null) ? $conf["PUBLIC_KEY_GPWEBPAY"] : "";
    }
    /**
     * @return string
     */
    public static function getPrivateKeyWebPassword() {
        $conf =  Service_Gpwebpay::getConfig();
        return ($conf != null && $conf["PRIVATE_KEY_WEB_PASSWORD"] != null) ? $conf["PRIVATE_KEY_WEB_PASSWORD"] : "";
    }

    /**
     * @return string
     */
    public static function getBaseUrlCreateOrderRequest() {
        $conf =  Service_Gpwebpay::getConfig();
        return ($conf != null && $conf["BASE_URL_CREATE_ORDER_REQUEST"] != null) ? $conf["BASE_URL_CREATE_ORDER_REQUEST"] : "";
    }

    /**
     * @return string
     */
    public static function getBaseUrlCreateOrderResponse() {
        $conf =  Service_Gpwebpay::getConfig();
        return ($conf != null && $conf["BASE_URL_CREATE_ORDER_RESPONSE"] != null) ? $conf["BASE_URL_CREATE_ORDER_RESPONSE"] : "";
    }

    /**
     * @return GpwebpayServiceQuery
     */
    public static function getGpwebpayServiceQuery() {
        if ( ! isset(self::$gpwebpayServiceQuery))
        {
            self::$gpwebpayServiceQuery = new GpwebpayServiceQuery();
        }
        return self::$gpwebpayServiceQuery;
    }

    /**
     * @return string
     */
    public static function getMerchantNumber() {
        $conf =  Service_Gpwebpay::getConfig();
        return $conf != null && $conf["MERCHANTNUMBER"] != null ? $conf["MERCHANTNUMBER"] : "";
    }

    /**
     * @param SoapFault[] $error
     * @param string $request
     * @throws Gpwebpay_Exception
     */
    private static function catchError($error, $request = null) {
        /** @var SoapFault $err */
        $err = reset($error);
        Kohana::$log->add(Kohana::ERROR,Kohana::exception_text($err));
        Kohana::$log->add(Kohana::ERROR,$request);
        throw new Gpwebpay_Exception($err);
    }


    /**
     * @param string $order_code
     * @return GpwebpayStructOrderStateResponse|null
     */
    public static function getQueryStatus($order_code) {
        $digest = self::getGpwebpaySignature()->sign(implode("|", array(self::getMerchantNumber(), $order_code)));
        if(self::getGpwebpayServiceQuery()->queryOrderState(self::getMerchantNumber(), $order_code, $digest)) {
            $result = self::getGpwebpayServiceQuery()->getResult();
            Kohana::$log->add(Kohana::INFO,self::getGpwebpayServiceQuery()->getLastRequest());
            return $result;
        } else {
            self::catchError(self::getGpwebpayServiceQuery()->getLastError(), self::getGpwebpayServiceQuery()->getLastRequest());
        }

        return null;
    }

    /**
     * @param Gpwebpay_Create_Order $createOrder
     * @return string url for redirect
     */
    public static function getUrlForNewPayment($createOrder) {
        $mn = $createOrder->getMerchantNumber();
        if(empty($mn)) {
            $createOrder->setMerchantNumber(self::getMerchantNumber());
        }
        $url = $createOrder->getUrl();
        if(empty($url)) {
            $createOrder->setUrl(self::getBaseUrlCreateOrderResponse());
        }
        $digest = self::getGpwebpaySignature()->sign($createOrder->getParamsTextForDigest());
        return $createOrder->getParamsTextForRequestUrl(self::getBaseUrlCreateOrderRequest(), $digest);
    }

    /**
     *
     * @param Gpwebpay_Create_Order $createOrder
     * @param string $digest
     * @return boolean
     */
    public static function verifyCreateOrderResponse($createOrder, $digest) {
        return self::getGpwebpaySignature()->verify($createOrder->getParamsTextForDigest(), $digest);
    }
}

?>