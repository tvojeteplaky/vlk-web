<?php
/**
 * File for class GpwebpayServiceCredit
 * @package Gpwebpay
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * This class stands for GpwebpayServiceCredit originally named Credit
 * @package Gpwebpay
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
class GpwebpayServiceCredit extends GpwebpayWsdlClass
{
    /**
     * Method to call the operation originally named credit
     * @uses GpwebpayWsdlClass::getSoapClient()
     * @uses GpwebpayWsdlClass::setResult()
     * @uses GpwebpayWsdlClass::saveLastError()
     * @param string $_merchantNumber
     * @param string $_orderNumber
     * @param long $_amount
     * @param string $_digest
     * @return GpwebpayStructOrderResponse
     */
    public function credit($_merchantNumber,$_orderNumber,$_amount,$_digest)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->credit($_merchantNumber,$_orderNumber,$_amount,$_digest));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named creditReversal
     * @uses GpwebpayWsdlClass::getSoapClient()
     * @uses GpwebpayWsdlClass::setResult()
     * @uses GpwebpayWsdlClass::saveLastError()
     * @param string $_merchantNumber
     * @param string $_orderNumber
     * @param int $_creditNumber
     * @param string $_digest
     * @return GpwebpayStructOrderResponse
     */
    public function creditReversal($_merchantNumber,$_orderNumber,$_creditNumber,$_digest)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->creditReversal($_merchantNumber,$_orderNumber,$_creditNumber,$_digest));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see GpwebpayWsdlClass::getResult()
     * @return GpwebpayStructOrderResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
