<?php
defined('SYSPATH') or die('No direct script access.');

/**
 *
 * Vygeneruje list variant s možností editovat
 *
 * @package    Hana
 * @author     Ondřej Brabec
 * @copyright  (c) 2015 Ondřej Brabec
 */
class AutoForm_Item_Modals extends AutoForm_Item
{
    private $up_orm;
    
    private $variants_orm;
    
    private $lang;

    private $price_table;
    
    public function pregenerate($data_orm) {
        
        $this->subject_id = $data_orm->id;
        $this->lang = Session::instance()->get("admlang");
        if (!$this->lang) $this->lang = 1;
        
        //die(var_dump($this->subject_id));
        $this->up_orm = orm::factory('product')->where('products.id', "=", $this->subject_id)->find();
        $this->variants_orm = orm::factory('product')->where('parent_id', "=", $this->subject_id);
        
        //die(var_dump($this->variants_orm->find()));
        $variant_id = isset($_GET["variant_id"]) ? $_GET["variant_id"] : null;
        $this->price_table = $this->generatePriceTable();
        $price_table = $this->price_table;
        $result_data = array();
        
        //die(var_dump($price_table));
        
        if (isset($_POST["variant_action_add"])) {
            
            $result_data["hana_form_action"] = "variant_add";
            $result_data['variant_id'] = isset($_POST['variant_id']) ? $_POST['variant_id'] : 0;
            $result_data["count"] = count($_POST['code']);
            for ($i = 0; $i < $_POST['count']; $i++) {
                $result_data['variants'][$i]['code'] = $_POST['code'][$i];
                $result_data['variants'][$i]['nazev'] = $_POST['nazev'][$i];
                $result_data['variants'][$i]['doplnek'] = $_POST['doplnek'][$i];
                $result_data['variants'][$i]['in_stock'] = $_POST['in_stock'][$i];
                $index = 0;
                foreach ($price_table["cats"] as $key => $row) {
                    
                    foreach ($_POST['price_category_variant'][$key] as $ind => $value) {
                        if ($i == $ind) $result_data['variants'][$i]['price_categories'][$key]["cena"] = $value;
                    }
                    
                    /*$result_data['variants'][$i]['price_categories'][$key] = $row;
                        $result_data['variants'][$i]['price_categories'][$key]["cena"] = $_POST['price_category_variant'][$key][];
                        $index++;*/
                }
            }
            
            //die(var_dump($result_data));
            
        }
        
        // pozadavek na smazani
        elseif (isset($_GET["variant_action_delete"]) && $_GET["variant_action_delete"]) {
            $result_data["hana_form_action"] = "variant_delete";
            $result_data["variant_id"] = $_GET["variant_id"];
        }
        $result_data["price_category"] = parent::pregenerate($data_orm);
        
        return $result_data;
    }
    
    public function generate($data, $template = false) {
        //die("d");
        $price_table = $this->price_table;
        $variants_template = new View("admin/edit/variants");
        $variants_template->variants = orm::factory("product")->where("parent_id", "=", $this->up_orm->id)->where("doplnek", "=", 0)->find_all();
        $variants_template->doplnky = orm::factory("product")->where("parent_id", "=", $this->up_orm->id)->where("doplnek", "=", 1)->find_all();
        $variants_template->entity_name = $this->entity_name;
        $variants_template->admin_path = $this->data_src["href"];
        $variants_template->price_table = $price_table;
        $result_data = $variants_template->render();
        
        if (isset($_GET[$this->entity_name . "_edit"])) {
            $variant_form = new View("admin/edit/variants_modal");
            $variant_form->price_table = $price_table;
            $variant_id = isset($_GET['variant_id']) ? $_GET['variant_id'] : 0;
            $data = $this->variants_orm->where('products.id', '=', $variant_id)->find();
            $variant_form->entity_name = $this->entity_name;
            if ($data->id) {
                $variant_form->data = $data;
            } 
            else {
                $variant_form->data = false;
                $variant_form->count_variants = count($variants_template->variants);
            }
            
            $result_data.= $variant_form->render();
        }
        
        return $result_data;
    }
    
    private function generatePriceTable() {
        
        $result_data = "";
        $price_categories = orm::factory("price_category")->find_all();
        
        foreach ($price_categories as $pcategory) {
            
            $result_data["cats"][$pcategory->id] = $pcategory->as_array();
            $variants = orm::factory("product")->where('products.parent_id', "=", $this->subject_id)->find_all();
            foreach ( $variants as $key => $variant) {
                
                //die(var_dump($variant));
                $priceobj = $variant->prices($pcategory->kod);
                
                if (is_object($priceobj)) {
                    
                    $cena = $priceobj->get_raw_price_value();
                    
                    //echo($cena);
                    
                } 
                else {
                    
                    $cena = "";
                }
                
                //die(var_dump($variant));
                
                if (!isset($_GET["variants_edit"])) {
                    
                    $result_data["many"][$variant->id][$pcategory->id] = $pcategory->as_array();
                    $result_data["many"][$variant->id][$pcategory->id]["cena"] = $cena;
                    
                    $result_data["many"][$variant->id][$pcategory->id]["kratky_popis"] = $pcategory->price_type->kratky_popis;
                } 
                else {
                    $result_data["many"][$variant->id][$pcategory->id] = $pcategory->as_array();
                    $result_data["many"][$variant->id][$pcategory->id]["cena"] = $cena;
                    
                    $result_data["many"][$variant->id][$pcategory->id]["kratky_popis"] = $pcategory->price_type->kratky_popis;
                    if (isset($_GET['variant_id']) && $_GET["variant_id"] == $variant->id) {
                        $result_data["one"][$pcategory->id] = $pcategory->as_array();
                        $result_data["one"][$pcategory->id]["cena"] = $cena;
                        
                        $result_data["one"][$pcategory->id]["kratky_popis"] = $pcategory->price_type->kratky_popis;
                    } 
                    elseif (!isset($_GET['variant_id'])) {
                        
                        $result_data["one"][$pcategory->id] = $pcategory->as_array();
                        $result_data["one"][$pcategory->id]["cena"] = 0;
                        $result_data["one"][$pcategory->id]["kratky_popis"] = $pcategory->price_type->kratky_popis;
                    }
                }
            }
            
        }
        
        return $result_data;
        
        /*$result_data.="<tr><td>$pcategory->kod</td><td>".($pcategory->popis?"($pcategory->popis)":"")."</td>";
        
            if($pcategory->price_type->kod=="sleva_procentem")
            {
                $cena_procentem=$this->up_orm->prices($pcategory->kod)->get_raw_price_value();
                if($cena_procentem!==false)
                {
                    $result_data.="<td>".$cena_procentem." ".$pcategory->price_type->kratky_popis."</td>";
                    if($this->up_orm->prices($pcategory->kod)->get_raw_price_with_tax())
                    {
                        $result_data.="<td>(".$this->up_orm->prices($pcategory->kod)->get_raw_price_with_tax()." s DPH)</td>\n";
        //                        $priceholder2=new Model_Product_Priceholder($this->up_orm,$pcategory->kod);
        //                        echo($priceholder2->get_raw_price_with_tax());
                    }
                    else
                    {
                        $result_data.="<td>Základní cena D0 nebyla vyplněna, nebo je nulová.</td>";
                    }
                }
                else
                {
                    $result_data.="<td>Základní cena D0 nebyla vyplněna, nebo je nulová.</td>";
                    $result_data.="<td>&nbsp;</td>\n";
                }
            }
            else
            {
                $priceobj=$this->up_orm->prices($pcategory->kod);
                if(is_object($priceobj))
                {
                    $cena=$priceobj->get_raw_price_value();
                }
                else
                {
                    $cena="";
                }
                        
                $result_data.="<td><input type=\"text\" name=\"price_category[$pcategory->id]\" class=\"shortInput\" value=\"$cena\"> ".$pcategory->price_type->kratky_popis."</td>";
                $result_data.="<td>&nbsp;</td>\n";
            }
            
            $result_data.="</tr>\n";*/
    }
}
