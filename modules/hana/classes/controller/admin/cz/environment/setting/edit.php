<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace nastaveni.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */

class Controller_Admin_Cz_Environment_Setting_Edit extends Controller_Hana_Edit
{
    protected $with_route=false;
    protected $back_link_url;
    protected $back_link_text="Zpět";
    protected $cloneable=false;

    public function before() {
        $this->orm=new Model_Owner();

        $this->back_link_url = url::base()."admin/".i18n::lang()."/environment/module/default";
        $this->send_link_url = url::base()."admin/".i18n::lang()."/environment/module/default";

        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("default_title")->type("edit")->label("Hlavní název stránek")->set();
        $this->auto_edit_table->row("default_description")->type("edit")->label("Výchozí popis")->set();
        $this->auto_edit_table->row("default_keywords")->type("edit")->label("Výchozí klíčová slova")->set();
        $this->auto_edit_table->row("firma")->type("edit")->label("Jméno (název firmy)")->set();
        $this->auto_edit_table->row("ulice")->type("edit")->label("Ulice")->set();
        $this->auto_edit_table->row("mesto")->type("edit")->label("Město")->set();
        $this->auto_edit_table->row("psc")->type("edit")->label("PSČ")->set();
        $this->auto_edit_table->row("copyright")->type("edit")->label("Copyright")->set();
        $this->auto_edit_table->row("tel")->type("edit")->label("Telefon")->set();
        $this->auto_edit_table->row("fax")->type("edit")->label("Fax")->set();
        $this->auto_edit_table->row("email")->type("edit")->label("E-mail")->set();
        $this->auto_edit_table->row("ic")->type("edit")->label("IČ")->set();
        $this->auto_edit_table->row("dic")->type("edit")->label("DIČ")->set();
        $this->auto_edit_table->row("zapis")->type("textarea")->label("Zápis")->set();
        $this->auto_edit_table->row("facebook")->type("edit")->label("Facebook")->set();
        $this->auto_edit_table->row("google")->type("edit")->label("Google+")->set();
        $this->auto_edit_table->row("linkedin")->type("edit")->label("LinkedIn")->set();
        $this->auto_edit_table->row("youtube")->type("edit")->label("YouTube")->set();
        $this->auto_edit_table->row("sitemap")->type("checkbox")->label("sitemap.xml")->condition("(vygenerovat nový sitemap.xml při uložení)")->value(0)->set();
        $this->auto_edit_table->row("ga_script")->type("edit")->label("Google Analytics kód")->condition("kód ve tvaru: UA-XXXXX-X")->set();
        $this->auto_edit_table->row("latlng")->type('edit')->label("Souřadnice")->condition('Ve formátu {"lat":xx.xxxxxx,"lng":yy.yyyyyy}, dostupné na adrese http://www.latlong.net/')->set();
    //  $this->auto_edit_table->row("ior")->type("edit")->label("ior kód")->condition("kód pro nahrávání vozidel")->set();
    //  $this->auto_edit_table->row("ip_address")->type("text")->label("IP Adresa")->value($_SERVER['SERVER_ADDR'])->set();

    }


    protected function _form_action_main_postvalidate($data) {
       parent::_form_action_main_postvalidate($data);

       // vygeneruju sitemapu na pozadani
       if($data["sitemap"]) Service_Sitemap::get_google_sitemap();

    }

    /**
     * Akce na smazani obrazku !
     * @param <type> $data
     */
    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir);
    }
}