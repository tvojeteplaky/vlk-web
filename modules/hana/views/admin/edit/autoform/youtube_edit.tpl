<div class="row" class="autoform youtube-edit">
    <div class="col-xs-12">
        {if !empty($youtubes)}
            <div class="row">
                <div class="col-xs-12">
                    <ul class="sortable grid items big">
                        {foreach $youtubes as $youtube}
                            <li data-id="{$youtube.id}" class="item">
                                {if $youtube.code}
                                    <iframe src="https://www.youtube.com/embed/{$youtube.code}" frameborder="0" allowfullscreen></iframe>
                                {/if}
                                <div class="btn-group btn-group-sm" role="group">
                                    <button data-id="{$youtube.id}" type="button" class="btn btn-primary width-50 edit">Upravit</button>
                                    <button data-id="{$youtube.id}" type="button" class="btn btn-warning width-50 delete">Smazat</button>
                                </div>
                            </li>
                        {/foreach}
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>
        {/if}
        <div class="row add">
            <div class="col-xs-12">
                <a href="#" class="btn btn-default" data-toggle="modal" data-target="#youtube-edit-modal">{translate str="Přidat"}</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="youtube-edit-modal" tabindex="-1" role="dialog" aria-labelledby="Youtube edit modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Video</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" id="youtube-edit-form" data-action="?" data-method="post">
                    <div class="form-group">
                        <label for="nazev" class="col-sm-2 control-label">Název</label>
                        <div class="col-sm-10">
                            <input type="text" id="nazev" value="" class="form-control" name="form[nazev]" placeholder="Název">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-2 control-label">Kód</label>
                        <div class="col-sm-10">
                            <input type="text" value="" class="form-control" id="code" name="form[code]" placeholder="Kód">
                        </div>
                    </div>
                    <div class="form-group">
                        <label form="zobrazit" class="col-sm-2 control-label">Zobrazit</label>
                        <div class="col-sm-10">
                            <input type="checkbox" value="1" checked name="form[zobrazit]" id="zobrazit">
                        </div>
                    </div>
                    <input type="hidden" name="form[product_id]" value="{$product.id}">
                    <input type="hidden" name="form[id]" value="0">
                    <input type="hidden" name="form[action]" value="new">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{translate str="Zavřit"}</button>
                <button type="submit" form="youtube-edit-form" class="btn btn-primary" id="youtube-edit-action">{translate str="Odelat"}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{$media_path}admin/js/autoform/youtube-edit.js"></script>
<script type="text/javascript">
    $(function (){
        YoutubeEdit.init($('#youtube-edit-form'), $('#youtube-edit-action'), $('.item'));
    });
</script>