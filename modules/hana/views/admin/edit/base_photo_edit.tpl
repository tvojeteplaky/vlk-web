{* sablona standardniho administracniho "editu" *}


<div id="PhotoPreviewBox">
    <ul class="sortable grid items big">
        {foreach $photos as $item}
            <li data-id="{$item.id}" class="item">
                <a href="{$item.src_ad}" class="fancybox" rel="gallery" title="{$item.nazev}">
                    <img src="{$item.src_at}" alt="náhled vloženého obrázku" title="{$item.nazev}" alt="{$item.nazev}"/>
                </a>

                <div class="tools glyphicon-list">

                    {if $item.move_up}
                        <a href="{$admin_path}?photoedit_action_reorder={$entity_name}&amp;photo_id={$item.id}&amp;reorder_direction=up" class="ajaxelement img92  pull-left" title="vyměnit s předchozím">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                    {else}
                    {/if}

                    <a class="ajaxelement" href="{$admin_path}?photoedit_action_visibility={$entity_name}&amp;photo_id={$item.id}&amp;state_value={if $item.zobrazit}0{else}1{/if}" title="{if $item.zobrazit}Viditelné{else}Skryté{/if}">
                        <span class="glyphicon {if $item.zobrazit}glyphicon-eye-open active{else}glyphicon-eye-close inactive{/if}"></span>
                    </a>

                    <a class="ajaxelement" href="{$admin_path}?main_gallery_editphoto={$item.id}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>

                    <a href="{$admin_path}?photoedit_action_delete={$entity_name}&amp;photo_id={$item.id}" class="ajaxelement confirmDelete">
                        <span class="glyphicon glyphicon-remove text-danger"></span>
                    </a>

                    <input type="checkbox" name="selitem[{$item.id}]"/>


                    {if $item.move_down}
                        <a href="{$admin_path}?photoedit_action_reorder={$entity_name}&amp;photo_id={$item.id}&amp;reorder_direction=down" class="ajaxelement img92 pull-right" title="vyměnit s následujícím">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    {else}
                    {/if}

                </div>
            </li>
        {/foreach}
        <div class="clearfix"></div>
    </ul>
    <div class="clear"></div>
</div>

{if !empty($photos)}
    <div class="row">
        <div class="col-md-12 text-right" id="photo-delete">
            <div class="panel panel-default">
                <div class="panel-body">
                    <input type="hidden" name="delitem_gallery" value="{$entity_name}">
                    <label>vybrat vše <input type="checkbox" name="sellAll" class="sellAll" id="sellAll"/></label>
                    <button type="submit" class="btn btn-warning" name="delitem" value="smazat označené" onclick="javascript: return confirm('Opravdu smazat vybrané položky?'); ">smazat označené</button>
                </div>
            </div>
        </div>
    </div>
{/if}


{literal}
    <script type="text/javascript">
        $(function () {

            $('.sortable').sortable().bind('sortupdate', function () {
                var items = $("#PhotoPreviewBox ul .item");
                var data = "form[action]=reorder";
                items.each(function (i) {
                    if (data != '')
                        data += '&';
                    data += 'newOrder[]=' + $(this).attr('data-id');
                });

                $.post(
                        '',
                        data,
                        function (data) {

                        }
                );
            });

            $("input#sellAll").on("change", function() {
                var check = $(this).prop("checked");
                $("#PhotoPreviewBox .item input[type=checkbox]").prop("checked", check);
            });


        });
    </script>
{/literal}