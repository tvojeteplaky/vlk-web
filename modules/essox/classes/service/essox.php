<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 1.10.2014
 * Time: 10:30
 */

class Service_Essox {
    /**
    * @var Kohana_Config
    */
    protected static $config;

    /**
     * @return Kohana_Config
     */
    public static function getConfig()
    {
        if ( ! isset(self::$config))
        {
            self::$config = Kohana::config('essox');
        }

        return self::$config;
    }

    /**
     * @return string
     */
    public static function getBaseEssoxUrl() {
        $conf =  self::getConfig();
        return $conf != null && $conf["BASE_ESSOX_URL"] != null ? $conf["BASE_ESSOX_URL"] : "";
    }

    /**
     * @return string
     */
    public static function getEssoxUsername() {
        $conf =  self::getConfig();
        return ($conf != null && $conf["USERNAME"] != null) ? $conf["USERNAME"] : "";
    }

    /**
     * @return string
     */
    public static function getEssoxCode() {
        $conf =  self::getConfig();
        return ($conf != null && $conf["CODE"] != null) ? $conf["CODE"] : "";
    }

    /**
     * @return int
     */
    public static function getEssoxOM() {
        $conf =  self::getConfig();
        return ($conf != null && $conf["OM_NUMBER"] != null) ? $conf["OM_NUMBER"] : "";
    }

    /**
     * Get url for shopper redirecting to essox payment gate.
     *
     * @param int $price set rounded price to crowns
     * @return string
     */
    public static function getEssoxPaymentUrl($price)
    {
        $userName = self::getEssoxUsername();
        $code = self::getEssoxCode();
        $price = round($price);
        $seed = time().rand();
        $hash = sha1($userName.'#'.$code.'#'.$price.'#'.$seed);
        return self::getBaseEssoxUrl().'/Login.aspx?a='.$userName.'&b='.$price.'&c='.$seed.'&d='.$hash;
    }

    /**
     * Get url for essox calculator.
     *
     * @param float $price
     * @return string
     */
    public static function getEssoxCalculatorUrl($price = null)
    {
        $om = self::getEssoxOM();
        $price = ($price !== null) ? number_format($price, 1, ",", "") : null;
        return self::getBaseEssoxUrl().'/calculator.aspx?id='.$om.($price !== null ? '&p='.$price : '');
    }

    /**
     * Get default js code for populating essox calculator.
     *
     * @param float $price
     * @return string
     */
    public static function getEssoxCalculatorJsWindowOpen($price = null) {
        return "window.open('".self::getEssoxCalculatorUrl($price)."', '_blank', 'toolbar=0, resizable=1, status=1, width=650, height=600')";
    }

    /**
     * Check if price is enough to pay by essox payment gate.
     *
     * @param $price
     * @return bool
     */
    public static function isEssoxPaymentPossibleDueToPrice($price) {
        return $price >= 3000;
    }

} 