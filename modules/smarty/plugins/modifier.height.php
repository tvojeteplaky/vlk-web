<?php
/**
 * @param height $url
 * @return int
 */
function smarty_modifier_height($url)
{
    $full = DOCROOT.$url;
    if (!file_exists($full))
        return false;
    $img = Image::factory($full);
    return $img->height;
}