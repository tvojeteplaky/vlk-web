<?php defined('SYSPATH') or die('No direct script access.');

class Service_Feed
{

    public static $default_address;
    public static $template_folder = "feed/";

    public static function init()
    {
        self::$default_address = (isset($_SERVER["REQUEST_SCHEME"]) ? $_SERVER["REQUEST_SCHEME"] : "http")."://".$_SERVER["SERVER_NAME"]."/";
    }

    /**
     * Vytvoří soubor a uloží jej
     * @param $content
     * @param $path
     */
    public static function save_file($content, $path)
    {
        $root = preg_replace("!{$_SERVER['SCRIPT_NAME']}$!", '', $_SERVER['SCRIPT_FILENAME']);
        $file = fopen($root.$path, "w");
        fwrite($file, $content);
        fclose($file);
    }

}