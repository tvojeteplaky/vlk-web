-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vygenerováno: Pon 18. čec 2016, 15:49
-- Verze serveru: 5.5.49-0ubuntu0.14.04.1
-- Verze PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `dgstudio_pujcovnavlk`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_preferences`
--

CREATE TABLE IF NOT EXISTS `admin_preferences` (
  `id` int(10) unsigned NOT NULL,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_settings`
--

CREATE TABLE IF NOT EXISTS `admin_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email_podpora_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `logo_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `kohana_debug_mode` tinyint(4) NOT NULL DEFAULT '0',
  `smarty_console` tinyint(4) NOT NULL DEFAULT '0',
  `shutdown` tinyint(4) NOT NULL DEFAULT '0',
  `disable_login` int(11) NOT NULL DEFAULT '0',
  `interni_poznamka` text COLLATE utf8_czech_ci,
  `admin_theme_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `nazev_dodavatele`, `email_podpora_dodavatele`, `logo_dodavatele`, `kohana_debug_mode`, `smarty_console`, `shutdown`, `disable_login`, `interni_poznamka`, `admin_theme_id`) VALUES
(1, 'dgstudio s.r.o.', 'info@dgstudio.cz', NULL, 0, 0, 0, 0, NULL, 6);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure`
--

CREATE TABLE IF NOT EXISTS `admin_structure` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `submodule_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_controller` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `admin_menu_section_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `global_access_level` tinyint(4) NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=82 ;

--
-- Vypisuji data pro tabulku `admin_structure`
--

INSERT INTO `admin_structure` (`id`, `module_code`, `submodule_code`, `module_controller`, `admin_menu_section_id`, `poradi`, `parent_id`, `zobrazit`, `global_access_level`, `available_languages`) VALUES
(1, 'environment', 'module', 'default', 1, 1, 0, 1, 0, 5),
(2, 'environment', 'setting', 'edit/1/1', 3, 15, 1, 1, 0, 5),
(3, 'environment', 'samodules', 'list', 3, 4, 1, 1, 3, 5),
(4, 'environment', 'salanguages', 'list', 3, 14, 1, 0, 0, 1),
(5, 'environment', 'routes', 'list', 3, 6, 1, 1, 3, 1),
(6, 'environment', 'mainsetup', 'edit/1/1', 3, 1, 1, 1, 1, 1),
(7, 'environment', 'module', 'list', 3, 2, 1, 1, 3, 1),
(8, 'environment', 'registry', 'list', 3, 5, 1, 1, 3, 1),
(9, 'page', 'item', 'list', 1, 2, 0, 1, 0, 1),
(10, 'page', 'item', 'list', 3, 1, 9, 1, 0, 1),
(11, 'page', 'unrelated', 'list', 3, 2, 9, 1, 0, 1),
(12, 'static', 'item', 'list', 3, 3, 9, 1, 0, 1),
(13, 'user', 'item', 'list', 1, 24, 0, 1, 0, 1),
(14, 'article', 'item', 'list', 1, 3, 0, 1, 0, 1),
(15, 'product', 'item', 'list', 1, 27, 0, 0, 0, 1),
(16, 'product', 'item', 'list', 2, 2, 15, 1, 0, 1),
(17, 'product', 'category', 'list', 2, 3, 15, 1, 0, 1),
(18, 'product', 'manufacturer', 'list', 2, 5, 15, 1, 0, 1),
(19, 'product', 'order', 'list', 2, 1, 15, 1, 0, 1),
(20, 'product', 'shipping', 'list', 2, 6, 15, 1, 0, 1),
(21, 'product', 'payment', 'list', 2, 7, 15, 1, 0, 1),
(22, 'product', 'price', 'list', 2, 8, 15, 1, 0, 1),
(23, 'product', 'tax', 'list', 2, 9, 15, 1, 0, 1),
(24, 'page', 'system', 'list', 3, 4, 9, 1, 3, 1),
(25, 'email', 'queue', 'list', 1, 25, 0, 1, 0, 1),
(26, 'email', 'queue', 'list', 3, 1, 25, 1, 0, 1),
(27, 'email', 'type', 'list', 3, 2, 25, 1, 0, 1),
(28, 'email', 'receiver', 'list', 3, 3, 25, 1, 0, 1),
(29, 'email', 'smtp', 'edit/1/1', 3, 4, 25, 1, 0, 1),
(30, 'product', 'shopper', 'list', 2, 4, 15, 1, 0, 1),
(31, 'product', 'orderstates', 'list', 2, 10, 15, 1, 0, 1),
(32, 'product', 'eshopsettings', 'edit/1/1', 2, 12, 15, 1, 0, 1),
(33, 'product', 'voucher', 'list', 2, 11, 15, 0, 0, 1),
(34, 'environment', 'langstrings', 'list', 3, 16, 1, 1, 1, 1),
(39, 'box', 'item', 'list', 1, 14, 0, 1, 0, 1),
(43, 'article', 'category', 'list', 2, 1, 14, 0, 0, 1),
(65, 'branch', 'item', 'list', 1, 23, 0, 1, 0, 1),
(67, 'branch', 'salesman', 'list', 2, 2, 65, 1, 0, 1),
(68, 'slider', 'slider', 'edit/1/1', 1, 13, 0, 1, 0, 1),
(70, 'slider', 'item', 'list', 2, 1, 68, 1, 0, 1),
(75, 'catalogue', 'item', 'list', 1, 5, 0, 1, 0, 1),
(76, 'catalogue', 'category', 'list', 2, 1, 75, 1, 0, 1),
(77, 'service', 'item', 'list', 0, 15, 0, 1, 0, 1),
(78, 'team', 'item', 'list', 0, 22, 0, 1, 0, 1),
(79, 'catalogue', 'shopper', 'list', 0, 2, 75, 1, 0, 1),
(80, 'catalogue', 'price', 'list', 0, 3, 75, 1, 0, 1),
(81, 'reason', 'item', 'list', 0, 28, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure_data`
--

CREATE TABLE IF NOT EXISTS `admin_structure_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_structure_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `admin_module_id` (`admin_structure_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=107 ;

--
-- Vypisuji data pro tabulku `admin_structure_data`
--

INSERT INTO `admin_structure_data` (`id`, `admin_structure_id`, `language_id`, `nazev`, `nadpis`, `title`, `description`, `keywords`, `popis`) VALUES
(1, 1, 1, 'Základní', 'Základní', 'Základní', '', '', NULL),
(6, 6, 1, 'Nastavení projektu', 'Základní nastavení', 'Základní nastavení', '', '', NULL),
(3, 3, 1, 'Struktura administrace', 'Struktura administrace', 'Struktura administrace', '', '', NULL),
(7, 7, 1, 'Instalátor modulů', 'Instalátor modulů', 'Instalátor modulů', '', '', NULL),
(8, 8, 1, 'Registry modulů', 'Registry modulů', 'Registry modulů', '', '', NULL),
(5, 5, 1, 'Záznamy rout', 'Záznamy rout', 'Záznamy rout', '', '', NULL),
(4, 4, 1, 'Nastavení jazyků', 'Nastavení jazyků', 'Nastavení jazyků', '', '', NULL),
(2, 2, 1, 'Obecné nastavení', 'Obecné nastavení', 'Obecné nastavení', '', '', NULL),
(9, 9, 1, 'Stránky', 'Stránky', 'Stránky', '', '', NULL),
(10, 10, 1, 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', '', '', NULL),
(11, 11, 1, 'Nezařazené stránky', 'Nezařazené stránky', 'Nezařazené stránky', '', '', NULL),
(12, 12, 1, 'Statický obsah', 'Statický obsah', 'Statický obsah', '', '', NULL),
(13, 13, 1, 'Administrátoři', 'Uživatelé', 'Uživatelé', '', '', NULL),
(14, 14, 1, 'Novinky', 'Novinky', 'Novinky', '', NULL, NULL),
(24, 24, 1, 'Systémové stránky', 'Systémové stránky', 'Systémové stránky', '', NULL, NULL),
(25, 25, 1, 'Email', 'Email', 'Email', '', NULL, NULL),
(26, 26, 1, 'Fronta emailů', 'Fronta emailů', 'Fronta emailů', '', NULL, NULL),
(27, 27, 1, 'Typy emailů', 'Typy emailů', 'Typy emailů', '', NULL, NULL),
(28, 28, 1, 'Příjemci emailů', 'Příjemci emailů', 'Příjemci emailů', '', NULL, NULL),
(29, 29, 1, 'Nastavení SMTP', 'Nastavení SMTP', 'Nastavení SMTP', '', NULL, NULL),
(34, 34, 1, 'Statické překlady', 'Statické překlady', 'Statické překlady', '', NULL, NULL),
(92, 68, 1, 'Slider', 'Slider', 'Slider', '', NULL, NULL),
(15, 15, 1, 'E-shop', 'E-shop', 'E-shop', '', NULL, NULL),
(16, 16, 1, 'Seznam produktů', 'Seznam produktů', 'Seznam produktů', '', NULL, NULL),
(47, 1, 4, 'Defaults', 'Defaults', 'Defaults', '', NULL, NULL),
(48, 3, 4, 'Administration structure', 'Samodules', 'Samodules', '', NULL, NULL),
(49, 2, 4, 'Default setting', 'Default setting', 'Default setting', '', NULL, NULL),
(62, 39, 1, 'Boxy', 'Boxy', 'Boxy', '', NULL, NULL),
(66, 43, 1, 'Kategorie', 'Kategorie', 'Kategorie', '', NULL, NULL),
(94, 70, 1, 'Slajdy', 'Slajdy', 'Slajdy', '', NULL, NULL),
(89, 65, 1, 'Kontakty', 'Pobočky', 'Pobočky', '', NULL, NULL),
(91, 67, 1, 'Lidé', 'Lidé', 'Lidé', '', NULL, NULL),
(105, 80, 1, 'Cenové skupiny', 'Cenové skupiny', 'Cenové skupiny', '', NULL, NULL),
(99, 75, 1, 'Katalog', 'Katalog', 'Katalog', '', NULL, NULL),
(100, 76, 1, 'Kategorie', 'Kategorie', 'Kategorie', '', NULL, NULL),
(17, 17, 1, 'Kategorie produktů', 'Kategorie produktů', 'Kategorie produktů', '', NULL, NULL),
(18, 18, 1, 'Výrobci', 'Výrobci', 'Výrobci', '', NULL, NULL),
(19, 19, 1, 'Objednávky', 'Objednávky', 'Objednávky', '', NULL, NULL),
(20, 20, 1, 'Doprava', 'Doprava', 'Doprava', '', NULL, NULL),
(21, 21, 1, 'Platba', 'Platba', 'Platba', '', NULL, NULL),
(22, 22, 1, 'Cenové skupiny', 'Cenové skupiny', 'Cenové skupiny', '', NULL, NULL),
(23, 23, 1, 'DPH', 'DPH', 'DPH', '', NULL, NULL),
(30, 30, 1, 'Uživatelé', 'Uživatelé', 'Uživatelé', '', NULL, NULL),
(31, 31, 1, 'Stavy objednávek', 'Stavy objednávek', 'Stavy objednávek', '', NULL, NULL),
(32, 32, 1, 'Nastavení eshopu', 'Nastavení eshopu', 'Nastavení eshopu', '', NULL, NULL),
(33, 33, 1, 'Slevové kupóny', 'Slevové kupóny', 'Slevové kupóny', '', NULL, NULL),
(102, 77, 1, 'Naše služby', 'Naše služby', 'Naše služby', '', NULL, NULL),
(103, 78, 1, 'Tým', 'Tým', 'Tým', '', NULL, NULL),
(104, 79, 1, 'Klienti', 'Klienti', 'Klienti', '', NULL, NULL),
(106, 81, 1, 'Proč my', 'Proč my', 'Proč my', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_themes`
--

CREATE TABLE IF NOT EXISTS `admin_themes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `nazev_css` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `inverse` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=8 ;

--
-- Vypisuji data pro tabulku `admin_themes`
--

INSERT INTO `admin_themes` (`id`, `nazev`, `popis`, `nazev_css`, `inverse`) VALUES
(1, 'Základní', NULL, 'bootstrap', 0),
(2, 'Darkly', NULL, 'bootstrap-darkly', 0),
(3, 'Lumen', NULL, 'bootstrap-lumen', 0),
(4, 'Paper', NULL, 'bootstrap-paper', 0),
(5, 'Sandstone', NULL, 'bootstrap-sandstone', 0),
(6, 'Yeti', NULL, 'bootstrap-yeti', 1),
(7, 'Cosmo', NULL, 'bootstrap-cosmo', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `poradi` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `author` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `show_contactform` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`id`, `date`, `poradi`, `photo_src`, `available_languages`, `author`, `show_contactform`) VALUES
(1, '2016-07-07', 1, 'specialni-novinka-aktualita-z-pujcovny', 1, '', 0),
(2, '2016-07-07', 2, 'specialni-novinkabra-aktualita-z-pujcovny', 1, '', 1),
(3, '2016-07-07', 3, 'specialni-novinkabra-aktualita-z-pujcovny', 1, '', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `articles_article_categories`
--

CREATE TABLE IF NOT EXISTS `articles_article_categories` (
  `article_id` int(10) unsigned NOT NULL,
  `article_category_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_categories`
--

CREATE TABLE IF NOT EXISTS `article_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `poradi` int(10) unsigned NOT NULL,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_category_data`
--

CREATE TABLE IF NOT EXISTS `article_category_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_category_id` int(10) unsigned NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `nadpis` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_data`
--

CREATE TABLE IF NOT EXISTS `article_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `autor` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`,`article_id`) USING BTREE,
  KEY `article_id` (`article_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `article_data`
--

INSERT INTO `article_data` (`id`, `language_id`, `article_id`, `route_id`, `nazev`, `nadpis`, `autor`, `email`, `uvodni_popis`, `popis`) VALUES
(1, 1, 1, 10, 'SPECIÁLNÍ NOVINKA A AKTUALITA Z PŮJČOVNY 1', 'SPECIÁLNÍ NOVINKA<br>A AKTUALITA Z PŮJČOVNY 1', '', '', '<p>Ta celá mít proti starověkého. Vím i severo-východ k polarizovaný dobyvačných. Hole 80 ℃ vypnutou zrnko nechala....</p>\n', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean mattis, erat et semper blandit, est odio fringilla dolor, ac pellentesque tellus urna at ligula. Vivamus ullamcorper, leo at volutpat dapibus, metus justo ornare ante, vel posuere tortor nisl quis ligula. Sed malesuada fringilla sapien. Nulla blandit ultrices diam. Morbi sem. Donec nibh. Duis luctus egestas nibh. In mauris massa, vestibulum in, congue vitae, laoreet vitae, justo. Nullam congue dignissim arcu. Sed et quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>\n'),
(2, 1, 2, 11, 'SPECIÁLNÍ NOVINKA A AKTUALITA Z PŮJČOVNY 2', 'SPECIÁLNÍ NOVINKA<br>A AKTUALITA Z PŮJČOVNY 2', '', '', '<p>Ta celá mít proti starověkého. Vím i severo-východ k polarizovaný dobyvačných. Hole 80 ℃ vypnutou zrnko nechala....</p>\n', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean mattis, erat et semper blandit, est odio fringilla dolor, ac pellentesque tellus urna at ligula. Vivamus ullamcorper, leo at volutpat dapibus, metus justo ornare ante, vel posuere tortor nisl quis ligula. Sed malesuada fringilla sapien. Nulla blandit ultrices diam. Morbi sem. Donec nibh. Duis luctus egestas nibh. In mauris massa, vestibulum in, congue vitae, laoreet vitae, justo. Nullam congue dignissim arcu. Sed et quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>\n'),
(3, 1, 3, 12, 'SPECIÁLNÍ NOVINKA A AKTUALITA Z PŮJČOVNY 3', 'SPECIÁLNÍ NOVINKA<br>A AKTUALITA Z PŮJČOVNY 3', '', '', '<p>Ta celá mít proti starověkého. Vím i severo-východ k polarizovaný dobyvačných. Hole 80 ℃ vypnutou zrnko nechala....</p>\n', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean mattis, erat et semper blandit, est odio fringilla dolor, ac pellentesque tellus urna at ligula. Vivamus ullamcorper, leo at volutpat dapibus, metus justo ornare ante, vel posuere tortor nisl quis ligula. Sed malesuada fringilla sapien. Nulla blandit ultrices diam. Morbi sem. Donec nibh. Duis luctus egestas nibh. In mauris massa, vestibulum in, congue vitae, laoreet vitae, justo. Nullam congue dignissim arcu. Sed et quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>\n');

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photos`
--

CREATE TABLE IF NOT EXISTS `article_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `article_id` (`article_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photo_data`
--

CREATE TABLE IF NOT EXISTS `article_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `article_photo_id` (`article_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `boxes`
--

CREATE TABLE IF NOT EXISTS `boxes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(256) COLLATE utf8_czech_ci DEFAULT NULL,
  `background_src` varchar(256) COLLATE utf8_czech_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `box_data`
--

CREATE TABLE IF NOT EXISTS `box_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `box_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `link` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `link_nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `address` text COLLATE utf8_czech_ci,
  `lng` decimal(9,7) NOT NULL,
  `lat` decimal(9,7) NOT NULL,
  `ic` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `dic` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `icp` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `headquarter` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `branches`
--

INSERT INTO `branches` (`id`, `available_languages`, `poradi`, `address`, `lng`, `lat`, `ic`, `dic`, `icp`, `photo_src`, `headquarter`) VALUES
(1, 1, 1, 'Šrámkova 1267,\n<br>\nZlín-Malenovice', 0.0000000, 0.0000000, '26920140', 'CZ26920140', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `branch_data`
--

CREATE TABLE IF NOT EXISTS `branch_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `comment` text COLLATE utf8_czech_ci,
  `comment2` text COLLATE utf8_czech_ci,
  `phone` text COLLATE utf8_czech_ci,
  `fax` text COLLATE utf8_czech_ci,
  `email` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `opening_hours` text COLLATE utf8_czech_ci,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `branch_data`
--

INSERT INTO `branch_data` (`id`, `branch_id`, `language_id`, `zobrazit`, `nazev`, `nadpis`, `comment`, `comment2`, `phone`, `fax`, `email`, `popis`, `opening_hours`) VALUES
(1, 1, 1, 1, 'Půjčovna nářadí Vlk s.r.o.', 'Půjčovna nářadí Vlk s.r.o.', 'Půjčovna nářadí Vlk s.r.o.,  Šrámkova 1267,  763 02  ZLÍN - Malenovice,', 'Společnost je zapsaná v obchodní rejstříku, okresní soud Brno,<br>oddíl C, vložka 45543', 'Tel.: 800 737 330<br>Mob.: 602 716 120', '602 716 120', '', NULL, 'PO - PÁ    6:30 - 17:00');

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue`
--

CREATE TABLE IF NOT EXISTS `email_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `queue_to_email` varchar(255) NOT NULL,
  `queue_to_name` varchar(255) DEFAULT NULL,
  `queue_cc_email` varchar(255) DEFAULT NULL,
  `queue_cc_name` varchar(255) DEFAULT NULL,
  `email_queue_body_id` int(11) NOT NULL,
  `queue_sent` tinyint(4) NOT NULL DEFAULT '0',
  `queue_sent_date` datetime DEFAULT NULL,
  `queue_priority` int(11) NOT NULL DEFAULT '0',
  `queue_date_to_be_send` datetime DEFAULT NULL,
  `queue_create_date` datetime NOT NULL,
  `queue_errors_count` tinyint(4) NOT NULL DEFAULT '0',
  `queue_error` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Vypisuji data pro tabulku `email_queue`
--

INSERT INTO `email_queue` (`id`, `queue_to_email`, `queue_to_name`, `queue_cc_email`, `queue_cc_name`, `email_queue_body_id`, `queue_sent`, `queue_sent_date`, `queue_priority`, `queue_date_to_be_send`, `queue_create_date`, `queue_errors_count`, `queue_error`) VALUES
(2, 'ondrej.brabec@dgstudio.cz', 'Ondřej Brabec - Developer', NULL, NULL, 2, 0, NULL, 2, '2016-06-29 14:03:02', '2016-06-29 14:03:02', 1, '<strong>SMTP Error: Data not accepted.</strong><br />\n'),
(3, 'ondrej.brabec@dgstudio.cz', 'Ondřej Brabec - Developer', NULL, NULL, 3, 0, NULL, 2, '2016-06-29 14:04:21', '2016-06-29 14:04:21', 1, '<strong>SMTP Error: Data not accepted.</strong><br />\n'),
(4, 'brabe.ondrej@gmail.com', 'Ondřej Brabec - Developer', NULL, NULL, 4, 1, '2016-06-30 21:47:05', 2, '2016-06-30 21:47:04', '2016-06-30 21:47:04', 0, NULL),
(5, 'ondrej.brabec@dgstudio.cz', 'Ondřej Brabec - Developer', NULL, NULL, 5, 1, '2016-07-02 20:04:28', 2, '2016-07-02 20:04:28', '2016-07-02 20:04:28', 0, NULL),
(6, 'ondrej.brabec@dgstudio.cz', 'Ondřej Brabec - Developer', NULL, NULL, 6, 1, '2016-07-02 20:05:01', 2, '2016-07-02 20:05:00', '2016-07-02 20:05:00', 0, NULL),
(7, 'ondrej.brabec@dgstudio.cz', 'Ondřej Brabec - Developer', NULL, NULL, 7, 1, '2016-07-02 20:05:12', 2, '2016-07-02 20:05:11', '2016-07-02 20:05:11', 0, NULL),
(8, 'rbunka@centrum.cz', 'rbunka@centrum.cz', NULL, NULL, 8, 1, '2016-07-16 21:48:25', 2, '2016-07-16 21:48:20', '2016-07-16 21:48:20', 0, NULL),
(9, 'rbunka@centrum.cz', 'rbunka@centrum.cz', NULL, NULL, 9, 1, '2016-07-16 22:10:23', 2, '2016-07-16 22:10:20', '2016-07-16 22:10:20', 0, NULL),
(10, 'rbunka@centrum.cz', 'rbunka@centrum.cz', NULL, NULL, 10, 1, '2016-07-18 15:43:12', 2, '2016-07-18 15:43:08', '2016-07-18 15:43:08', 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue_bodies`
--

CREATE TABLE IF NOT EXISTS `email_queue_bodies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `queue_subject` varchar(255) DEFAULT NULL,
  `queue_from_email` varchar(255) NOT NULL,
  `queue_from_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `queue_body` text NOT NULL,
  `queue_attached_file` varchar(255) DEFAULT NULL,
  `queue_newsletter_id` int(11) DEFAULT NULL,
  `queue_shopper_id` bigint(20) DEFAULT NULL,
  `queue_branch_id` bigint(20) DEFAULT NULL,
  `queue_order_id` bigint(20) DEFAULT NULL,
  `queue_user_id` bigint(20) DEFAULT NULL,
  `queue_email_type_id` int(11) DEFAULT NULL,
  `queue_send_by_cron` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_type_id` (`queue_email_type_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Vypisuji data pro tabulku `email_queue_bodies`
--

INSERT INTO `email_queue_bodies` (`id`, `queue_subject`, `queue_from_email`, `queue_from_name`, `queue_body`, `queue_attached_file`, `queue_newsletter_id`, `queue_shopper_id`, `queue_branch_id`, `queue_order_id`, `queue_user_id`, `queue_email_type_id`, `queue_send_by_cron`) VALUES
(1, '', '', '', '<p>Dobrý den, <br>\nbyla vám přidělena sleva 5 a \nvaše přihlašovací údaje jsou <br>\njmeno: kaktus.ob@gmail.com <br>\nheslo: H1596321 <br>\ns pozdravem Půjčovna Vlk\n	</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, '', '', '', '<p>Dobrý den, <br>\nbyla vám přidělena sleva 5 a \nvaše přihlašovací údaje jsou <br>\njmeno: ondrej.brabec@dgstudio.cz <br>\nheslo: 7878754455 <br>\ns pozdravem Půjčovna Vlk\n	</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, '', '', '', '<p>Dobrý den, <br>\nbyla vám přidělena sleva 5 % a \nvaše přihlašovací údaje jsou <br>\njmeno: ondrej.brabec@dgstudio.cz <br>\nheslo: 55465456 <br>\ns pozdravem Půjčovna Vlk\n	</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 'Nový zákaznický účet', 'info@dgstudio.cz', 'Dgstudion test', '<p>Dobrý den, <br>\nbyla vám přidělena sleva 5 % a \nvaše přihlašovací údaje jsou <br>\njmeno: brabe.ondrej@gmail.com <br>\nheslo: 55465456 <br>\ns pozdravem Půjčovna Vlk\n	</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, 'Změna stávající slevy', 'info@dgstudio.cz', 'Dgstudion test', '<p>Dobrý den, <br>\nVaše sleva byla změněna z 10 na 5\ns pozdravem Půjčovna Vlk\n	</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 'Změna stávající slevy', 'info@dgstudio.cz', 'Dgstudion test', '<p>Dobrý den, <br>\nVaše sleva byla změněna z 5 na 10\ns pozdravem Půjčovna Vlk\n	</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, 'Změna stávající slevy', 'info@dgstudio.cz', 'Dgstudion test', '<p>Dobrý den, <br>\nVaše sleva byla změněna z 10% na 5%\ns pozdravem Půjčovna Vlk\n	</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 'Kontaktní formulář', 'roman.bunka@dgstudio.cz', 'DG ', '<table cellpadding="5" style="border: none; border-collapse: collapse;">\r\n    <tr>\r\n        <td><strong>Zpráva z kontaktního formuláře www stránek</strong></td>\r\n        <td>Půjčovna Vlk</td>\r\n    </tr>\r\n    <tr>\r\n        <td><strong>Název stránky</strong></td>\r\n        <td>Kontakty</td>\r\n    </tr>\r\n    <tr>\r\n        <td><strong>URL</strong></td>\r\n        <td>http://pujcovnavlk/kontakty</td>\r\n    </tr>\r\n\r\n    <tr>\r\n        <td><strong>Jméno a příjmení</strong></td>\r\n        <td>DG  Studio</td>\r\n    </tr>\r\n\r\n    <tr>\r\n        <td><strong>E-mail odesílatele</strong></td>\r\n        <td>roman.bunka@dgstudio.cz</td>\r\n    </tr>\r\n\r\n            <tr>\r\n            <td><strong>Telefon</strong></td>\r\n            <td>123456789</td>\r\n        </tr>\r\n    \r\n\r\n    <tr>\r\n        <td colspan="2">&nbsp;</td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="2">Text dotazu:</td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="2">Test formulare.</td>\r\n    </tr>\r\n</table>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, 'Kontaktní formulář', 'rbunka@centrum.cz', 'Test', '<table cellpadding="5" style="border: none; border-collapse: collapse;">\r\n    <tr>\r\n        <td><strong>Zpráva z kontaktního formuláře www stránek</strong></td>\r\n        <td>Půjčovna Vlk</td>\r\n    </tr>\r\n    <tr>\r\n        <td><strong>Název stránky</strong></td>\r\n        <td>Kontakty</td>\r\n    </tr>\r\n    <tr>\r\n        <td><strong>URL</strong></td>\r\n        <td>http://pujcovnavlk/kontakty</td>\r\n    </tr>\r\n\r\n    <tr>\r\n        <td><strong>Jméno a příjmení</strong></td>\r\n        <td>Test DG</td>\r\n    </tr>\r\n\r\n    <tr>\r\n        <td><strong>E-mail odesílatele</strong></td>\r\n        <td>rbunka@centrum.cz</td>\r\n    </tr>\r\n\r\n            <tr>\r\n            <td><strong>Telefon</strong></td>\r\n            <td>123456789</td>\r\n        </tr>\r\n    \r\n\r\n    <tr>\r\n        <td colspan="2">&nbsp;</td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="2">Text dotazu:</td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="2">Test</td>\r\n    </tr>\r\n</table>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 'Služby formulář', 'rbunka@centrum.cz', 'Tester', '<table cellpadding="5" style="border: none; border-collapse: collapse;">\r\n    <tr>\r\n        <td><strong>Zpráva z kontaktního formuláře www stránek</strong></td>\r\n        <td>Půjčovna Vlk</td>\r\n    </tr>\r\n    <tr>\r\n        <td><strong>Název stránky</strong></td>\r\n        <td>Naše služby</td>\r\n    </tr>\r\n    <tr>\r\n        <td><strong>URL</strong></td>\r\n        <td>http://pujcovnavlk/nase-sluzby</td>\r\n    </tr>\r\n\r\n    <tr>\r\n        <td><strong>Jméno a příjmení</strong></td>\r\n        <td>Tester</td>\r\n    </tr>\r\n\r\n    <tr>\r\n		<td><strong>Výběr služby: </strong></td>\r\n    	<td>Práce s bagry</td>\r\n    </tr>\r\n\r\n    <tr>\r\n        <td><strong>E-mail odesílatele</strong></td>\r\n        <td>rbunka@centrum.cz</td>\r\n    </tr>\r\n\r\n            <tr>\r\n            <td><strong>Telefon</strong></td>\r\n            <td>123456789</td>\r\n        </tr>\r\n    \r\n\r\n    <tr>\r\n        <td colspan="2">&nbsp;</td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="2">Text dotazu:</td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="2">Test formulare sluzby.</td>\r\n    </tr>\r\n</table>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_receivers`
--

CREATE TABLE IF NOT EXISTS `email_receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `email_receivers`
--

INSERT INTO `email_receivers` (`id`, `nazev`, `email`, `user_id`) VALUES
(1, 'rbunka@centrum.cz', 'rbunka@centrum.cz', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` int(11) NOT NULL,
  `mailer` varchar(16) NOT NULL DEFAULT 'mail',
  `host` varchar(32) NOT NULL DEFAULT 'localhost',
  `port` int(11) NOT NULL DEFAULT '25',
  `SMTPSecure` varchar(32) DEFAULT NULL,
  `SMTPAuth` tinyint(4) NOT NULL DEFAULT '0',
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `SMTPDebug` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `email_settings`
--

INSERT INTO `email_settings` (`id`, `mailer`, `host`, `port`, `SMTPSecure`, `SMTPAuth`, `username`, `password`, `SMTPDebug`) VALUES
(1, 'smtp', 'mail.dghost.cz', 25, '', 1, 'smtp@dghost.cz', 'smtpmageror', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types`
--

CREATE TABLE IF NOT EXISTS `email_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `template` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `from_nazev` varchar(255) DEFAULT '',
  `from_email` varchar(255) DEFAULT '',
  `use_email_queue` tinyint(4) NOT NULL DEFAULT '0',
  `send_by_cron` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `email_types`
--

INSERT INTO `email_types` (`id`, `nazev`, `code`, `template`, `subject`, `from_nazev`, `from_email`, `use_email_queue`, `send_by_cron`) VALUES
(1, 'Nový uživatel', 'add_user', '', 'Nový zákaznický účet', 'Dgstudion test', 'info@dgstudio.cz', 1, 0),
(2, 'Změna slevy', 'change_discount', '', 'Změna stávající slevy', 'Dgstudion test', 'info@dgstudio.cz', 1, 0),
(3, 'Kontaktní formulář', 'form_contact', '', 'Kontaktní formulář', '', '', 1, 0),
(4, 'Služby formulář', 'form_service', '', 'Služby formulář', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types_receivers`
--

CREATE TABLE IF NOT EXISTS `email_types_receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_type_id` int(11) NOT NULL,
  `email_receiver_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email_type_id` (`email_type_id`,`email_receiver_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `email_types_receivers`
--

INSERT INTO `email_types_receivers` (`id`, `email_type_id`, `email_receiver_id`) VALUES
(1, 3, 1),
(2, 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `eshop_settings`
--

CREATE TABLE IF NOT EXISTS `eshop_settings` (
  `id` int(11) NOT NULL,
  `billing_data_nazev` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_ulice` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_mesto` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_psc` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_ic` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_dic` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_telefon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_fax` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_banka` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_iban` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_cislo_uctu` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_konst_s` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_spec_s` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_swift` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `present_enabled` tinyint(4) NOT NULL,
  `present_price_threshold` decimal(10,0) NOT NULL,
  `billing_data_due_date` int(11) DEFAULT NULL,
  `shipping_free_threshold` decimal(6,0) DEFAULT NULL,
  `first_purchase_discount` decimal(4,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `eshop_settings`
--

INSERT INTO `eshop_settings` (`id`, `billing_data_nazev`, `billing_data_email`, `billing_data_ulice`, `billing_data_mesto`, `billing_data_psc`, `billing_data_ic`, `billing_data_dic`, `billing_data_telefon`, `billing_data_fax`, `billing_data_banka`, `billing_data_iban`, `billing_data_cislo_uctu`, `billing_data_konst_s`, `billing_data_spec_s`, `billing_data_swift`, `present_enabled`, `present_price_threshold`, `billing_data_due_date`, `shipping_free_threshold`, `first_purchase_discount`) VALUES
(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `gift_boxes`
--

CREATE TABLE IF NOT EXISTS `gift_boxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `invoice_settings`
--

CREATE TABLE IF NOT EXISTS `invoice_settings` (
  `id` int(11) NOT NULL,
  `next_invoice_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `invoice_settings`
--

INSERT INTO `invoice_settings` (`id`, `next_invoice_code`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `language_strings`
--

CREATE TABLE IF NOT EXISTS `language_strings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `language_string_data`
--

CREATE TABLE IF NOT EXISTS `language_string_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `language_string_id` int(11) NOT NULL,
  `string` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_string_id`) USING BTREE,
  KEY `language_id_2` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `photo` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `poradi` int(11) NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturer_data`
--

CREATE TABLE IF NOT EXISTS `manufacturer_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `manufacturer_id` (`manufacturer_id`),
  KEY `language_id` (`language_id`),
  KEY `route_id` (`route_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `verze` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `autor` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `poznamka` text COLLATE utf8_czech_ci,
  `poradi` int(11) NOT NULL DEFAULT '0',
  `admin_zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `available` tinyint(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `kod` (`kod`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=16 ;

--
-- Vypisuji data pro tabulku `modules`
--

INSERT INTO `modules` (`id`, `kod`, `nazev`, `verze`, `datum`, `autor`, `url`, `email`, `popis`, `poznamka`, `poradi`, `admin_zobrazit`, `available`) VALUES
(1, 'page', 'Stránky', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul zobrazující klasický statický obsah ve stránkách.', NULL, 1, 1, 1),
(2, 'link', 'Odkazy', '1', NULL, 'Pavel Herink', NULL, NULL, 'Odkaz na externí stránku.', NULL, 2, 1, 1),
(3, 'article', 'Články', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul článků a novinek.', NULL, 3, 1, 1),
(4, 'contact', 'Kontakty', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul kontaktního formuláře.', NULL, 4, 1, 1),
(5, 'search', 'Vyhledávání', '1', '2016-06-29', 'Pavel Herink', '', '', '', NULL, 6, 1, 1),
(6, 'newsletter', 'Newsletter', '', '2016-06-29', '', '', '', '', NULL, 7, 0, 0),
(7, 'product', 'Produkty', '', '2016-06-29', '', '', '', '', NULL, 5, 0, 0),
(8, 'sitemap', 'Mapa stránek', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 0),
(9, 'reference', 'Reference', '', '2016-06-29', '', '', '', '', NULL, 9, 0, 0),
(11, 'shoppingcart', 'Košík', '', '2016-06-29', '', '', '', '', NULL, 11, 0, 0),
(10, 'order', 'Objednávka', '', '2016-06-29', '', '', '', '', NULL, 10, 0, 0),
(12, 'catalogue', 'Katalog', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 1, 1),
(13, 'user', 'Uživatel - front', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 0, 1),
(14, 'service', 'Služby', '', '2016-06-29', '', '', '', '', NULL, 14, 1, 1),
(15, 'team', 'Tým', '', '2016-06-29', '', '', '', '', NULL, 15, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `module_actions`
--

CREATE TABLE IF NOT EXISTS `module_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `kod` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `povoleno` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`,`povoleno`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `generovan` tinyint(4) NOT NULL DEFAULT '0',
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletter_data`
--

CREATE TABLE IF NOT EXISTS `newsletter_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `autor` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`,`newsletter_id`),
  KEY `newsletter_id` (`newsletter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletter_recipients`
--

CREATE TABLE IF NOT EXISTS `newsletter_recipients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `allowed` tinyint(3) unsigned NOT NULL,
  `shopper_id` int(10) unsigned NOT NULL,
  `email` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shopper_id` (`shopper_id`),
  KEY `allowed` (`allowed`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mode` varchar(16) NOT NULL DEFAULT 'eshop',
  `order_code` char(13) NOT NULL,
  `order_code_invoice` varchar(12) DEFAULT NULL,
  `order_date` datetime NOT NULL,
  `order_date_finished` date DEFAULT NULL,
  `order_delivery_date` datetime DEFAULT NULL,
  `order_date_tax` datetime DEFAULT NULL,
  `order_date_payment` datetime DEFAULT NULL,
  `order_payment_id` tinyint(4) NOT NULL DEFAULT '0',
  `order_shipping_id` tinyint(4) NOT NULL,
  `order_const_symbol` varchar(4) NOT NULL DEFAULT '0',
  `order_state_id` tinyint(4) NOT NULL DEFAULT '0',
  `order_price_no_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_price_lower_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_price_higher_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_no_vat_rate` int(11) NOT NULL DEFAULT '0',
  `order_lower_vat_rate` int(11) NOT NULL DEFAULT '10',
  `order_higher_vat_rate` int(11) NOT NULL DEFAULT '20',
  `order_lower_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_higher_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_without_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_with_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_shipping_price` decimal(6,2) NOT NULL DEFAULT '0.00',
  `order_payment_price` decimal(6,2) NOT NULL,
  `order_total` decimal(9,2) NOT NULL,
  `order_voucher_id` int(11) DEFAULT NULL,
  `order_voucher_discount` decimal(10,2) DEFAULT NULL,
  `order_discount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_correction` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_CZK` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_weight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_shopper_id` int(11) DEFAULT '0',
  `order_shopper_branch` int(11) DEFAULT '0',
  `order_shopper_name` varchar(255) NOT NULL,
  `order_shopper_code` char(6) DEFAULT NULL,
  `order_shopper_email` varchar(100) NOT NULL,
  `order_shopper_phone` varchar(20) NOT NULL,
  `order_shopper_ic` varchar(64) DEFAULT NULL,
  `order_shopper_dic` varchar(20) DEFAULT NULL,
  `order_shopper_street` varchar(50) DEFAULT NULL,
  `order_shopper_city` varchar(50) DEFAULT NULL,
  `order_shopper_zip` varchar(10) DEFAULT NULL,
  `order_shopper_note` text NOT NULL,
  `order_branch_name` varchar(255) DEFAULT NULL,
  `order_branch_code` char(6) DEFAULT NULL,
  `order_branch_street` varchar(50) DEFAULT NULL,
  `order_branch_city` varchar(50) DEFAULT NULL,
  `order_branch_zip` varchar(50) DEFAULT NULL,
  `order_branch_phone` varchar(20) DEFAULT NULL,
  `order_branch_email` varchar(100) DEFAULT NULL,
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `last_modified` datetime NOT NULL,
  `order_shopper_custommer_code` varchar(255) DEFAULT NULL,
  `post_track_trace_code` varchar(255) DEFAULT NULL,
  `is_payu` tinyint(4) NOT NULL DEFAULT '0',
  `payu_session_code` varchar(255) DEFAULT NULL,
  `payu_status_code` varchar(255) DEFAULT NULL,
  `payu_status_message` varchar(255) DEFAULT NULL,
  `payu_error_message` varchar(255) DEFAULT NULL,
  `gp_webpay_status` int(2) DEFAULT NULL,
  `gp_webpay_prcode` int(11) DEFAULT NULL,
  `gp_webpay_srcode` int(11) DEFAULT NULL,
  `gp_webpay_text_status` varchar(255) DEFAULT NULL,
  `gift_box_code` varchar(255) DEFAULT NULL,
  `engine_id` int(11) DEFAULT NULL,
  `actions` tinyint(4) NOT NULL,
  `order_montaz_price` decimal(9,2) NOT NULL,
  `order_montaz` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Vypisuji data pro tabulku `orders`
--

INSERT INTO `orders` (`id`, `mode`, `order_code`, `order_code_invoice`, `order_date`, `order_date_finished`, `order_delivery_date`, `order_date_tax`, `order_date_payment`, `order_payment_id`, `order_shipping_id`, `order_const_symbol`, `order_state_id`, `order_price_no_vat`, `order_price_lower_vat`, `order_price_higher_vat`, `order_no_vat_rate`, `order_lower_vat_rate`, `order_higher_vat_rate`, `order_lower_vat`, `order_higher_vat`, `order_total_without_vat`, `order_total_with_vat`, `order_shipping_price`, `order_payment_price`, `order_total`, `order_voucher_id`, `order_voucher_discount`, `order_discount`, `order_correction`, `order_total_CZK`, `order_weight`, `order_shopper_id`, `order_shopper_branch`, `order_shopper_name`, `order_shopper_code`, `order_shopper_email`, `order_shopper_phone`, `order_shopper_ic`, `order_shopper_dic`, `order_shopper_street`, `order_shopper_city`, `order_shopper_zip`, `order_shopper_note`, `order_branch_name`, `order_branch_code`, `order_branch_street`, `order_branch_city`, `order_branch_zip`, `order_branch_phone`, `order_branch_email`, `export`, `last_modified`, `order_shopper_custommer_code`, `post_track_trace_code`, `is_payu`, `payu_session_code`, `payu_status_code`, `payu_status_message`, `payu_error_message`, `gp_webpay_status`, `gp_webpay_prcode`, `gp_webpay_srcode`, `gp_webpay_text_status`, `gift_box_code`, `engine_id`, `actions`, `order_montaz_price`, `order_montaz`) VALUES
(18, 'eshop', '', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 0, 0, '0', 0, 0.00, 0.00, 0.00, 0, 10, 20, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0, 0, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2016-02-02 01:30:43', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `varianta_popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `varianta_id` int(11) NOT NULL,
  `jednotka` char(2) COLLATE utf8_czech_ci NOT NULL,
  `hmotnost` decimal(5,2) NOT NULL,
  `pocet_na_sklade` decimal(6,2) NOT NULL,
  `min_order_quantity` decimal(3,2) DEFAULT NULL,
  `tax_code` varchar(25) CHARACTER SET utf8 NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `units` decimal(6,2) NOT NULL,
  `price_without_tax` decimal(10,2) NOT NULL,
  `price_with_tax` decimal(10,2) NOT NULL,
  `total_price_with_tax` decimal(10,2) NOT NULL,
  `item_change` tinyint(4) DEFAULT '0',
  `total_weight` decimal(6,2) NOT NULL DEFAULT '0.00',
  `gift` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_item_changes`
--

CREATE TABLE IF NOT EXISTS `order_item_changes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `change_order` bigint(20) NOT NULL,
  `change_item` bigint(20) NOT NULL DEFAULT '0',
  `change_date` datetime DEFAULT NULL,
  `change_units_from` decimal(6,2) NOT NULL DEFAULT '0.00',
  `change_units_to` decimal(6,2) NOT NULL DEFAULT '0.00',
  `change_type` varchar(10) NOT NULL DEFAULT 'internal',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_states`
--

CREATE TABLE IF NOT EXISTS `order_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `order_state_type_id` int(11) NOT NULL,
  `send_mail` tinyint(4) NOT NULL DEFAULT '0',
  `readonly` tinyint(4) NOT NULL DEFAULT '0',
  `poradi` tinyint(4) NOT NULL,
  `smazano` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_state_type_id` (`order_state_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_state_data`
--

CREATE TABLE IF NOT EXISTS `order_state_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_state_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email_text` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `order_state_id` (`order_state_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_state_types`
--

CREATE TABLE IF NOT EXISTS `order_state_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `poradi` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `owner_data`
--

CREATE TABLE IF NOT EXISTS `owner_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `firma` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `mesto` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `psc` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `stat` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `tel` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  `fax` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `www` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ga_script` text COLLATE utf8_czech_ci NOT NULL,
  `zapis` text COLLATE utf8_czech_ci,
  `facebook` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `google` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `youtube` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `linkedin` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `ior` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `owner_data`
--

INSERT INTO `owner_data` (`id`, `default_title`, `default_description`, `default_keywords`, `firma`, `ulice`, `mesto`, `psc`, `stat`, `copyright`, `ic`, `dic`, `tel`, `fax`, `email`, `www`, `ga_script`, `zapis`, `facebook`, `google`, `youtube`, `linkedin`, `ior`) VALUES
(1, 'Půjčovna Vlk', '', '', 'Půjčovna Vlk s.r.o.', 'Šrámkova 1267,', 'Zlín-Malenovice', '763 02', NULL, '', '26920140', 'CZ26920140', '800 737 330', '', '', NULL, '', 'Společnost je zapsána v obchodním rejstříku, okresní soud Brno. oddíl C. vložka 45543 ', '', '', '', '', 'OPkHdz5AYttQmeeB6tQmIe5Bs');

-- --------------------------------------------------------

--
-- Struktura tabulky `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_category_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `indexpage` tinyint(4) NOT NULL,
  `new_window` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_menu` tinyint(4) DEFAULT '1',
  `direct_to_sublink` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_submenu` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `show_contactform` tinyint(4) NOT NULL DEFAULT '0',
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `show_visitform` tinyint(4) NOT NULL DEFAULT '0',
  `on_homepage` tinyint(4) NOT NULL,
  `unclickable` tinyint(1) NOT NULL,
  `product_category_id` int(10) unsigned NOT NULL,
  `in_header` tinyint(1) NOT NULL,
  `light_heading` tinyint(1) NOT NULL,
  `show_submenu` tinyint(1) NOT NULL,
  `show_map` tinyint(1) NOT NULL,
  `header_right` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `page_category_id` (`page_category_id`) USING BTREE,
  KEY `show_in_menu` (`show_in_menu`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=9 ;

--
-- Vypisuji data pro tabulku `pages`
--

INSERT INTO `pages` (`id`, `page_category_id`, `poradi`, `parent_id`, `indexpage`, `new_window`, `show_in_menu`, `direct_to_sublink`, `show_in_submenu`, `available_languages`, `show_contactform`, `photo_src`, `show_visitform`, `on_homepage`, `unclickable`, `product_category_id`, `in_header`, `light_heading`, `show_submenu`, `show_map`, `header_right`) VALUES
(1, 3, 1, 0, 1, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 3, 2, 0, 0, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 3, 3, 0, 0, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 3, 4, 0, 0, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 3, 5, 0, 0, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 3, 6, 0, 0, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 2, 7, 0, 0, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 2, 8, 0, 0, 0, 1, 0, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `page_data`
--

CREATE TABLE IF NOT EXISTS `page_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `podnadpis` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `link_text` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `page_id` (`page_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=9 ;

--
-- Vypisuji data pro tabulku `page_data`
--

INSERT INTO `page_data` (`id`, `page_id`, `language_id`, `route_id`, `nazev`, `nadpis`, `podnadpis`, `uvodni_popis`, `popis`, `url`, `link`, `link_text`) VALUES
(1, 1, 1, 1, 'Úvodní stránka', 'Úvodní stránka', '', NULL, NULL, '', '', ''),
(2, 2, 1, 2, 'Půjčovna', 'Půjčovna', '', NULL, NULL, '', '', ''),
(3, 3, 1, 3, 'Naše služby', 'Naše služby', '', NULL, '<p>Městský laboratorní látky točil, ho loď, letního mé zaměstnanci nádech nahé účelné, šanci zámořské pokroku svým zdrojem k zdrojem pracovník cítím. Potvrzují paleontologové četné jehož i zemi, já ta a výběru představu sestavení škody postupovali, řad pletiva mé ten kterého masy souvislosti erupce, výkon ze hlavně minuty spodní postupu nadšení, tvořené názoru budete vědců státní &ndash; mě to až větry u kybernetiky staly změn ho opravdové předchozímu vyhýbá lidském jediným.</p>\n', '', '', ''),
(4, 4, 1, 4, 'Ceníky', 'Ceníky', '', NULL, NULL, '', '', ''),
(5, 5, 1, 5, 'O nás', 'O nás', '', NULL, '<p>Městský laboratorní látky točil, ho loď, letního mé zaměstnanci nádech nahé účelné, šanci zámořské pokroku svým zdrojem k zdrojem pracovník cítím. Potvrzují paleontologové četné jehož i zemi, já ta a výběru představu sestavení škody postupovali, řad pletiva mé ten kterého masy souvislosti erupce, výkon ze hlavně minuty spodní postupu nadšení, tvořené názoru budete vědců státní &ndash; mě to až větry u kybernetiky staly změn ho opravdové předchozímu vyhýbá lidském jediným.</p>\n', '', '', ''),
(6, 6, 1, 6, 'Kontakty', 'Kontakty', '', NULL, NULL, '', '', ''),
(7, 7, 1, 7, 'Výsledky vyhledávání', 'Výsledky vyhledávání', '', NULL, NULL, '', '', ''),
(8, 8, 1, 8, 'Novinky', 'Novinky', '', NULL, NULL, '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photos`
--

CREATE TABLE IF NOT EXISTS `page_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `page_id` (`page_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photo_data`
--

CREATE TABLE IF NOT EXISTS `page_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_photo_id` (`page_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena` decimal(6,2) NOT NULL,
  `typ` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(11) NOT NULL,
  `predem` tinyint(4) NOT NULL DEFAULT '0',
  `payu` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `payment_type_id` int(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payments_shippings`
--

CREATE TABLE IF NOT EXISTS `payments_shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`,`payment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=FIXED AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payment_data`
--

CREATE TABLE IF NOT EXISTS `payment_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_id` (`payment_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payment_types`
--

CREATE TABLE IF NOT EXISTS `payment_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `special` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `price_categories`
--

CREATE TABLE IF NOT EXISTS `price_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `hodnota` tinyint(4) NOT NULL COMMENT 'pripadna procentni hodnota',
  `zaradit_zakaznika_od` decimal(8,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `price_type_id` (`price_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `price_categories`
--

INSERT INTO `price_categories` (`id`, `kod`, `popis`, `price_type_id`, `hodnota`, `zaradit_zakaznika_od`) VALUES
(1, 'D0', 'Základní cena', 1, 0, 0),
(2, 'sleva_5', 'Sleva 5%', 3, 5, 0),
(3, 'sleva_10', 'Sleva 10%', 3, 10, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `price_categories_products`
--

CREATE TABLE IF NOT EXISTS `price_categories_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price_category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cena` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `price_category_id` (`price_category_id`),
  KEY `price_category_id_2` (`price_category_id`,`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=1461 ;

--
-- Vypisuji data pro tabulku `price_categories_products`
--

INSERT INTO `price_categories_products` (`id`, `price_category_id`, `product_id`, `cena`) VALUES
(1458, 1, 1, 155.00),
(1459, 1, 3, 0.00),
(1460, 1, 4, 2000.00);

-- --------------------------------------------------------

--
-- Struktura tabulky `price_types`
--

CREATE TABLE IF NOT EXISTS `price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `kratky_popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `price_types`
--

INSERT INTO `price_types` (`id`, `kod`, `popis`, `kratky_popis`) VALUES
(1, 'hodnota_s_dph', 'Zadání hodnotou s DPH', 's DPH'),
(2, 'hodnota_bez_dph', 'Zadání hodnotou bez DPH', 'bez DPH'),
(3, 'sleva_procentem', 'Zadání procentní slevou z D0', 'sleva %');

-- --------------------------------------------------------

--
-- Struktura tabulky `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `main_code` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `jednotka` varchar(4) COLLATE utf8_czech_ci DEFAULT 'ks',
  `rok_vyroby` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `hmotnost` decimal(7,3) DEFAULT '0.000',
  `pocet_na_sklade` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `min_order_quantity` decimal(3,2) DEFAULT NULL,
  `puvodni_cena` decimal(10,2) NOT NULL,
  `poradi` int(11) NOT NULL,
  `top` tinyint(4) NOT NULL DEFAULT '0',
  `tax_id` int(11) NOT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `import_type` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `original` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `product_expedition_id` int(11) DEFAULT NULL,
  `guarantee` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '0',
  `percentage_discount` decimal(4,2) NOT NULL DEFAULT '0.00',
  `ignore_discount` tinyint(4) NOT NULL DEFAULT '0',
  `new_imported` tinyint(4) NOT NULL DEFAULT '0',
  `imported` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `gift` tinyint(4) NOT NULL DEFAULT '0',
  `gift_threshold_price` decimal(8,0) DEFAULT NULL,
  `product_action_type` tinyint(4) NOT NULL DEFAULT '0',
  `new` tinyint(4) NOT NULL DEFAULT '0',
  `sale_off` tinyint(4) NOT NULL DEFAULT '0',
  `producer_sale_off` tinyint(4) NOT NULL DEFAULT '0',
  `youtube_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `stroj_id` int(11) NOT NULL,
  `prefered` tinyint(4) DEFAULT NULL,
  `action` tinyint(4) DEFAULT '0',
  `altus_jednotka` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `import_error` tinyint(4) NOT NULL DEFAULT '0',
  `import_note` text COLLATE utf8_czech_ci,
  `base_product` tinyint(1) NOT NULL DEFAULT '1',
  `base_product_id` int(11) DEFAULT NULL,
  `varianta` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `main_photo_size` int(11) NOT NULL DEFAULT '0',
  `gallery_photos_size` int(11) NOT NULL DEFAULT '0',
  `main_photo_uploaded` datetime DEFAULT NULL,
  `gallery_photos_uploaded` datetime DEFAULT NULL,
  `has_variants` tinyint(1) NOT NULL DEFAULT '0',
  `rating` decimal(10,2) DEFAULT NULL,
  `rating_count` int(11) DEFAULT NULL,
  `smazano` tinyint(1) NOT NULL DEFAULT '0',
  `doplnek` tinyint(4) NOT NULL,
  `strong` tinyint(1) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `price_weekend` decimal(10,2) NOT NULL,
  `price_half_day` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manufacturer_id` (`manufacturer_id`),
  KEY `tax_id` (`tax_id`),
  KEY `import_type` (`import_type`),
  KEY `original` (`original`),
  KEY `main_code_ix` (`main_code`) USING BTREE,
  KEY `base_product_ix` (`base_product`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `products`
--

INSERT INTO `products` (`id`, `code`, `main_code`, `jednotka`, `rok_vyroby`, `hmotnost`, `pocet_na_sklade`, `min_order_quantity`, `puvodni_cena`, `poradi`, `top`, `tax_id`, `manufacturer_id`, `photo_src`, `available_languages`, `import_type`, `original`, `product_expedition_id`, `guarantee`, `in_stock`, `percentage_discount`, `ignore_discount`, `new_imported`, `imported`, `updated`, `gift`, `gift_threshold_price`, `product_action_type`, `new`, `sale_off`, `producer_sale_off`, `youtube_code`, `stroj_id`, `prefered`, `action`, `altus_jednotka`, `import_error`, `import_note`, `base_product`, `base_product_id`, `varianta`, `main_photo_size`, `gallery_photos_size`, `main_photo_uploaded`, `gallery_photos_uploaded`, `has_variants`, `rating`, `rating_count`, `smazano`, `doplnek`, `strong`, `parent_id`, `price_weekend`, `price_half_day`) VALUES
(1, '', '', 'ks', '', 0.000, '0', NULL, 0.00, 1, 0, 0, NULL, '', 1, '', '', NULL, NULL, 0, 0.00, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, 0, '', 0, NULL, 0, NULL, 0, NULL, 1, NULL, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0.00, 0.00),
(2, '', '', 'ks', '', 0.000, '0', NULL, 0.00, 2, 0, 0, NULL, '', 1, '', '', NULL, NULL, 0, 0.00, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, 0, '', 0, NULL, 0, NULL, 0, NULL, 1, NULL, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0.00, 0.00),
(3, '', '', 'ks', '', 0.000, '0', NULL, 0.00, 3, 0, 0, NULL, '', 1, '', '', NULL, NULL, 0, 0.00, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, 0, '', 0, NULL, 0, NULL, 0, NULL, 1, NULL, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0.00, 0.00),
(4, '', '', 'ks', '', 0.000, '0', NULL, 0.00, 4, 0, 0, NULL, 'bagr4', 1, '', '', NULL, NULL, 0, 0.00, 0, 0, NULL, NULL, 0, NULL, 0, 0, 0, 0, '', 0, NULL, 0, NULL, 0, NULL, 1, NULL, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 0, 0, 1500.00, 1000.00);

-- --------------------------------------------------------

--
-- Struktura tabulky `products_related`
--

CREATE TABLE IF NOT EXISTS `products_related` (
  `product_id` int(11) NOT NULL,
  `related_product_id` int(11) NOT NULL,
  `relation_type` int(11) NOT NULL COMMENT '0 - Accessories, 1 - Alternative product, 2 - Incompatible product, 3 - Related Product',
  `note` varchar(255) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`,`related_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `products_stores`
--

CREATE TABLE IF NOT EXISTS `products_stores` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quantity` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `photo_src_left` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `photo_src_right` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `priorita` tinyint(4) NOT NULL DEFAULT '0',
  `special_code` varchar(16) COLLATE utf8_czech_ci NOT NULL COMMENT 'specialni kategorie (novinky, akce, apod.)',
  `class` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `imported` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=19 ;

--
-- Vypisuji data pro tabulku `product_categories`
--

INSERT INTO `product_categories` (`id`, `poradi`, `photo`, `photo_src_left`, `photo_src_right`, `priorita`, `special_code`, `class`, `photo_src`, `parent_id`, `available_languages`, `imported`, `updated`) VALUES
(1, 1, '', NULL, '', 0, '', NULL, 'hutnici-a-vibracni-technika_white', 0, 1, NULL, NULL),
(2, 2, '', NULL, '', 0, '', NULL, 'hladke-a-jezkove-valce_white', 0, 1, NULL, NULL),
(3, 3, '', NULL, '', 0, '', NULL, 'bagry-a-nakladace_white', 0, 1, NULL, NULL),
(4, 4, '', NULL, '', 0, '', NULL, 'dumpery_white', 0, 1, NULL, NULL),
(5, 5, '', NULL, '', 0, '', NULL, 'plosiny_white', 0, 1, NULL, NULL),
(6, 6, '', NULL, '', 0, '', NULL, 'shozy-vratky-vytahy_white', 0, 1, NULL, NULL),
(7, 7, '', NULL, '', 0, '', NULL, 'kompresory_white', 0, 1, NULL, NULL),
(8, 8, '', NULL, '', 0, '', NULL, 'elektrocentraly-generatory_white', 0, 1, NULL, NULL),
(9, 9, '', NULL, '', 0, '', NULL, 'zahradni-technika_white', 0, 1, NULL, NULL),
(10, 10, '', NULL, '', 0, '', NULL, 'brouseni-dreva-a-podlah_white', 0, 1, NULL, NULL),
(11, 11, '', NULL, '', 0, '', NULL, 'zpracovnani-betonu_white', 0, 1, NULL, NULL),
(12, 12, '', NULL, '', 0, '', NULL, 'pojizdne-leseni_white', 0, 1, NULL, NULL),
(13, 13, '', NULL, '', 0, '', NULL, 'elektricke-rucni-naradi_white', 0, 1, NULL, NULL),
(14, 14, '', NULL, '', 0, '', NULL, 'rezani-a-drazkovani_white', 0, 1, NULL, NULL),
(15, 15, '', NULL, '', 0, '', NULL, 'brouseni-a-frezovani_white', 0, 1, NULL, NULL),
(16, 16, '', NULL, '', 0, '', NULL, 'cerpadla_white', 0, 1, NULL, NULL),
(17, 17, '', NULL, '', 0, '', NULL, 'odvlhcovace-a-topidla_white', 0, 1, NULL, NULL),
(18, 18, '', NULL, '', 0, '', NULL, 'ostatni_white', 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories_products`
--

CREATE TABLE IF NOT EXISTS `product_categories_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=1908 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_category_data`
--

CREATE TABLE IF NOT EXISTS `product_category_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_full` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_jedno` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_menu` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_paticka` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `uvodni_popis_levy` text COLLATE utf8_czech_ci,
  `uvodni_popis_pravy` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `zobrazit_carousel` tinyint(4) NOT NULL DEFAULT '1',
  `zobrazit_na_homepage` tinyint(4) NOT NULL,
  `price_from` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `no_show_products` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `product_category_id` (`product_category_id`),
  KEY `language_id` (`language_id`),
  KEY `route_id` (`route_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=19 ;

--
-- Vypisuji data pro tabulku `product_category_data`
--

INSERT INTO `product_category_data` (`id`, `product_category_id`, `language_id`, `route_id`, `title`, `description`, `keywords`, `nazev`, `nazev_full`, `nazev_jedno`, `nazev_menu`, `nazev_paticka`, `uvodni_popis`, `uvodni_popis_levy`, `uvodni_popis_pravy`, `popis`, `zobrazit`, `zobrazit_carousel`, `zobrazit_na_homepage`, `price_from`, `no_show_products`) VALUES
(1, 1, 1, 13, 'Hutnící a vibrační technika', '', '', 'Hutnící a vibrační technika', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(2, 2, 1, 14, 'Hladké a ježkové válce', '', '', 'Hladké a ježkové válce', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(3, 3, 1, 15, 'Bagry a nakladače', '', '', 'Bagry a nakladače', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(4, 4, 1, 16, 'Dumpery', '', '', 'Dumpery', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(5, 5, 1, 17, 'Plošiny', '', '', 'Plošiny', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(6, 6, 1, 18, 'Shozy, vrátky, výtahy', '', '', 'Shozy, vrátky, výtahy', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(7, 7, 1, 19, 'Kompresory', '', '', 'Kompresory', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(8, 8, 1, 20, 'Elektrocentrály, generátory', '', '', 'Elektrocentrály, generátory', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(9, 9, 1, 21, 'Zahradní technika', '', '', 'Zahradní technika', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(10, 10, 1, 22, 'Broušení dřeva a podlah', '', '', 'Broušení dřeva a podlah', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(11, 11, 1, 23, 'Zpracovnání betonu', '', '', 'Zpracovnání betonu', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(12, 12, 1, 24, 'Pojízdné lešení', '', '', 'Pojízdné lešení', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(13, 13, 1, 25, 'Elektrické ruční nářadí', '', '', 'Elektrické ruční nářadí', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(14, 14, 1, 26, 'Řezání a drážkování', '', '', 'Řezání a drážkování', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(15, 15, 1, 27, 'Broušení a frézování', '', '', 'Broušení a frézování', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(16, 16, 1, 28, 'Čerpadla', '', '', 'Čerpadla', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(17, 17, 1, 29, 'Odvlhčovače a topidla', '', '', 'Odvlhčovače a topidla', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0),
(18, 18, 1, 30, 'Ostatní', '', '', 'Ostatní', '', '', '', '', '', '', '', NULL, 1, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `product_data`
--

CREATE TABLE IF NOT EXISTS `product_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zobrazit_carousel` tinyint(4) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_doplnek` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `odborne_informace` text COLLATE utf8_czech_ci,
  `baleni` text COLLATE utf8_czech_ci,
  `akce_text` text COLLATE utf8_czech_ci,
  `k_prodeji` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `language_id` (`language_id`),
  KEY `route_id` (`route_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `product_data`
--

INSERT INTO `product_data` (`id`, `zobrazit_carousel`, `product_id`, `language_id`, `route_id`, `nazev`, `nazev_doplnek`, `uvodni_popis`, `popis`, `odborne_informace`, `baleni`, `akce_text`, `k_prodeji`) VALUES
(1, 0, 1, 1, 9, 'Bagr', '', '', '<p>Bagr 1</p>\n', '', '', '', 0),
(2, 0, 2, 1, 31, 'Bagr2', '', '', '<p>Bagr2</p>\n', '', '', '', 0),
(3, 0, 3, 1, 32, 'Bagr3', '', '', '<p>Bagr 3</p>\n', '', '', '', 0),
(4, 0, 4, 1, 33, 'Bagr4', '', '', '<p>Bagr4</p>\n', '', '', '', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `product_files`
--

CREATE TABLE IF NOT EXISTS `product_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `file_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=155 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_file_data`
--

CREATE TABLE IF NOT EXISTS `product_file_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_file_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_file_id` (`product_file_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=155 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photos`
--

CREATE TABLE IF NOT EXISTS `product_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `uploaded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=198 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photo_data`
--

CREATE TABLE IF NOT EXISTS `product_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_photo_id` (`product_photo_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=198 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_ratings`
--

CREATE TABLE IF NOT EXISTS `product_ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `rating` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_slideshows`
--

CREATE TABLE IF NOT EXISTS `product_slideshows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `uploaded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1059 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_slideshow_data`
--

CREATE TABLE IF NOT EXISTS `product_slideshow_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_slideshow_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_photo_id` (`product_slideshow_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1061 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_variants`
--

CREATE TABLE IF NOT EXISTS `product_variants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `in_stock` int(11) NOT NULL COMMENT 'Na skladě',
  `code` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `doplnek` tinyint(4) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `tax_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`,`zobrazit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_variant_data`
--

CREATE TABLE IF NOT EXISTS `product_variant_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reasons`
--

CREATE TABLE IF NOT EXISTS `reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  `smazano` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `reasons`
--

INSERT INTO `reasons` (`id`, `poradi`, `zobrazit`, `smazano`) VALUES
(1, 1, 1, 0),
(2, 2, 1, 0),
(3, 3, 1, 0),
(4, 4, 1, 0),
(5, 5, 1, 0),
(6, 6, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `reason_data`
--

CREATE TABLE IF NOT EXISTS `reason_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason_id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `nadpis` varchar(255) DEFAULT NULL,
  `obsah` text,
  `zlomek` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `reason_data`
--

INSERT INTO `reason_data` (`id`, `reason_id`, `nazev`, `nadpis`, `obsah`, `zlomek`, `language_id`) VALUES
(1, 1, 'Důvod 1', 'Nejširší sortiment', '<p>kvalitního značkového nářadí ve Zlínském kraji, toto je pouze rozšiřující text</p>\n', '1/6', 1),
(2, 2, 'Důvod 2', 'Doprava strojů', '<p>přímo pro Vaši stavu, lorem ipsum dolor sit amet, toto je žralok v kebabu. To se nedělá!</p>\n', '2/6', 1),
(3, 3, 'Důvod 3', 'Nejširší sortiment', 'kvalitního značkového nářadí\n                ve Zlínském kraji, toto je pouze\n                rozšiřující text', '3/6', 1),
(4, 4, 'Důvod 4', 'Nejširší sortiment', 'kvalitního značkového nářadí\n                ve Zlínském kraji, toto je pouze\n                rozšiřující text', '4/6', 1),
(5, 5, 'Důvod 5', 'Nejširší sortiment', '<p>kvalitního značkového nářadí ve Zlínském kraji, toto je pouze rozšiřující text</p>\n', '5/6', 1),
(6, 6, 'Důvod 6', 'Nejširší sortiment', '<p>kvalitního značkového nářadí ve Zlínském kraji, toto je pouze rozšiřující text</p>\n', '6/6', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `references`
--

CREATE TABLE IF NOT EXISTS `references` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_category_id` int(10) unsigned NOT NULL,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(256) DEFAULT NULL,
  `homepage` tinyint(1) NOT NULL,
  `photo_gallery` tinyint(1) NOT NULL,
  `date` date DEFAULT NULL,
  `header_right` tinyint(1) NOT NULL,
  `show_contactform` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_categories`
--

CREATE TABLE IF NOT EXISTS `reference_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(255) DEFAULT NULL,
  `header_right` tinyint(1) NOT NULL,
  `show_contactform` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_category_data`
--

CREATE TABLE IF NOT EXISTS `reference_category_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_category_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `popis` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_data`
--

CREATE TABLE IF NOT EXISTS `reference_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` int(10) unsigned NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `nazev` varchar(256) NOT NULL,
  `nadpis` varchar(256) NOT NULL,
  `main_nadpis` varchar(256) NOT NULL,
  `popis` text,
  `uvodni_popis` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_photos`
--

CREATE TABLE IF NOT EXISTS `reference_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `reference_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `reference_id` (`reference_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_photo_data`
--

CREATE TABLE IF NOT EXISTS `reference_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reference_photo_id` (`reference_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.'),
(3, 'global_admin', 'Global administrator.');

-- --------------------------------------------------------

--
-- Struktura tabulky `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `roles_users`
--

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(7, 1),
(8, 1),
(9, 1),
(5, 2),
(7, 2),
(8, 2),
(9, 2),
(5, 3),
(7, 3),
(8, 3),
(9, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev_seo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_action` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT 'index',
  `param_id1` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `baselang_route_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `internal` tinyint(4) NOT NULL DEFAULT '0',
  `searcheable` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `nazev_seo_old` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_type_id` (`module_id`) USING BTREE,
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `baselang_route_id` (`baselang_route_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=34 ;

--
-- Vypisuji data pro tabulku `routes`
--

INSERT INTO `routes` (`id`, `nazev_seo`, `module_id`, `module_action`, `param_id1`, `language_id`, `baselang_route_id`, `title`, `description`, `keywords`, `read_only`, `internal`, `searcheable`, `deleted`, `nazev_seo_old`, `created_date`, `updated_date`, `deleted_date`, `zobrazit`) VALUES
(1, 'index', 1, 'index', NULL, 1, 0, 'Úvodní stránka', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:20:07', '2016-06-29 00:23:40', NULL, 1),
(2, 'pujcovna', 12, 'index', NULL, 1, 0, 'Půjčovna', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:20:30', NULL, NULL, 1),
(3, 'nase-sluzby', 14, 'index', NULL, 1, 0, 'Naše služby', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:20:48', '2016-07-16 16:39:32', NULL, 1),
(4, 'ceniky', 1, 'detail', NULL, 1, 0, 'Ceníky', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:20:57', '2016-06-29 00:20:59', NULL, 1),
(5, 'o-nas', 15, 'index', NULL, 1, 0, 'O nás', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:21:14', '2016-07-17 20:41:11', NULL, 1),
(6, 'kontakty', 4, 'index', NULL, 1, 0, 'Kontakty', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:21:32', NULL, NULL, 1),
(7, 'vysledky-vyhledavani', 5, 'index', NULL, 1, 0, 'Výsledky vyhledávání', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:22:27', NULL, NULL, 1),
(8, 'novinky', 3, 'index', NULL, 1, 0, 'Novinky', '', '', 0, 0, 1, 0, NULL, '2016-06-29 00:23:08', NULL, NULL, 1),
(9, 'bagr', 12, 'detail', NULL, 1, 0, 'Bagr', '', '', 0, 0, 1, 0, NULL, '2016-07-02 21:48:28', '2016-07-12 16:08:46', NULL, 1),
(10, 'specialni-novinka-aktualita-z-pujcovny-1', 3, 'detail', NULL, 1, 0, 'SPECIÁLNÍ NOVINKA<br>A AKTUALITA Z PŮJČOVNY', '', '', 0, 0, 1, 0, NULL, '2016-07-07 15:35:24', '2016-07-07 16:32:49', NULL, 1),
(11, 'specialni-novinka-aktualita-z-pujcovny-2', 3, 'detail', NULL, 1, 0, 'SPECIÁLNÍ NOVINKA<br>A AKTUALITA Z PŮJČOVNY', '', '', 0, 0, 1, 0, NULL, '2016-07-07 15:41:38', '2016-07-07 16:32:57', NULL, 1),
(12, 'specialni-novinka-aktualita-z-pujcovny-3', 3, 'detail', NULL, 1, 0, 'SPECIÁLNÍ NOVINKA<br>A AKTUALITA Z PŮJČOVNY', '', '', 0, 0, 1, 0, NULL, '2016-07-07 15:42:42', '2016-07-07 16:33:04', NULL, 1),
(13, 'hutnici-a-vibracni-technika', 12, 'category', NULL, 1, 0, 'Hutnící a vibrační technika', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:11:57', '2016-07-07 21:22:12', NULL, 1),
(14, 'hladke-a-jezkove-valce', 12, 'category', NULL, 1, 0, 'Hladké a ježkové válce', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:12:35', '2016-07-07 21:22:57', NULL, 1),
(15, 'bagry-a-nakladace', 12, 'category', '', 1, 0, 'Bagry a nakladače', '', '', 0, 0, 1, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(16, 'dumpery', 12, 'category', NULL, 1, 0, 'Dumpery', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:13:01', '2016-07-07 21:17:12', NULL, 1),
(17, 'plosiny', 12, 'category', NULL, 1, 0, 'Plošiny', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:13:12', '2016-07-07 21:20:22', NULL, 1),
(18, 'shozy-vratky-vytahy', 12, 'category', NULL, 1, 0, 'Shozy, vrátky, výtahy', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:13:27', '2016-07-18 12:37:59', NULL, 1),
(31, 'bagr2', 12, 'detail', NULL, 1, 0, 'Bagr2', '', '', 0, 0, 1, 0, NULL, '2016-07-09 23:19:53', NULL, NULL, 1),
(19, 'kompresory', 12, 'category', NULL, 1, 0, 'Kompresory', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:25:25', '2016-07-18 12:25:38', NULL, 1),
(20, 'elektrocentraly-generatory', 12, 'category', NULL, 1, 0, 'Elektrocentrály, generátory', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:25:40', '2016-07-08 14:46:34', NULL, 1),
(21, 'zahradni-technika', 12, 'category', NULL, 1, 0, 'Zahradní technika', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:25:56', '2016-07-18 12:26:37', NULL, 1),
(22, 'brouseni-dreva-a-podlah', 12, 'category', NULL, 1, 0, 'Broušení dřeva a podlah', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:26:12', '2016-07-18 12:27:13', NULL, 1),
(23, 'zpracovnani-betonu', 12, 'category', NULL, 1, 0, 'Zpracovnání betonu', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:26:24', '2016-07-18 12:27:46', NULL, 1),
(24, 'pojizdne-leseni', 12, 'category', NULL, 1, 0, 'Pojízdné lešení', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:26:36', '2016-07-07 21:27:14', NULL, 1),
(25, 'elektricke-rucni-naradi', 12, 'category', NULL, 1, 0, 'Elektrické ruční nářadí', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:30:58', '2016-07-18 12:43:01', NULL, 1),
(26, 'rezani-a-drazkovani', 12, 'category', NULL, 1, 0, 'Řezání a drážkování', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:31:11', '2016-07-07 21:34:00', NULL, 1),
(27, 'brouseni-a-frezovani', 12, 'category', NULL, 1, 0, 'Broušení a frézování', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:31:28', '2016-07-08 14:48:09', NULL, 1),
(28, 'cerpadla', 12, 'category', NULL, 1, 0, 'Čerpadla', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:31:40', '2016-07-18 12:33:35', NULL, 1),
(29, 'odvlhcovace-a-topidla', 12, 'category', NULL, 1, 0, 'Odvlhčovače a topidla', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:31:52', '2016-07-18 12:31:15', NULL, 1),
(30, 'ostatni', 12, 'category', NULL, 1, 0, 'Ostatní', '', '', 0, 0, 1, 0, NULL, '2016-07-07 21:32:07', '2016-07-18 12:31:27', NULL, 1),
(32, 'bagr3', 12, 'detail', NULL, 1, 0, 'Bagr3', '', '', 0, 0, 1, 0, NULL, '2016-07-12 15:16:06', '2016-07-12 15:16:15', NULL, 1),
(33, 'bagr4', 12, 'detail', NULL, 1, 0, 'Bagr4', '', '', 0, 0, 1, 0, NULL, '2016-07-12 15:33:31', '2016-07-12 17:11:44', NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `salesmans`
--

CREATE TABLE IF NOT EXISTS `salesmans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=9 ;

--
-- Vypisuji data pro tabulku `salesmans`
--

INSERT INTO `salesmans` (`id`, `available_languages`, `poradi`, `name`, `phone`, `email`, `photo_src`) VALUES
(1, 1, 1, 'Radek Vičík ', '+420 732 210 845', 'vicik@pujcovna-vlk.cz', 'radek-vicik'),
(2, 1, 2, 'Markéta Kadlecová', '+420 734 435 644', 'kadlecova@pujcovna-vlk.cz', 'marketa-kadlecova'),
(3, 1, 3, 'Michal Rožek', '+420 602 716 120', 'rozek@pujcovna-vlk.cz', 'michal-rozek'),
(4, 1, 4, 'Marek Rybenský', '+420 605 839 428', 'servis@pujcovna-vlk.cz', 'marek-rybensky'),
(5, 1, 5, 'Marek Adamec ', '+420 733 160 225 ', 'servis@pujcovna-vlk.cz', 'marek-adamec'),
(6, 1, 6, 'Jan Kubáček', '+420 734 435 755 ', 'kubacek@pujcovna-vlk.cz', 'jan-kubacek'),
(7, 1, 7, 'Tomáš Zajíc', '+420 734 435 614 ', 'zajic@pujcovna-vlk.cz', 'tomas-zajic'),
(8, 1, 8, 'Radek Vlk ', '+420 800 737 330', 'vlk@pujcovna-vlk.cz', 'radek-vlk');

-- --------------------------------------------------------

--
-- Struktura tabulky `salesmans_branches`
--

CREATE TABLE IF NOT EXISTS `salesmans_branches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `salesman_data`
--

CREATE TABLE IF NOT EXISTS `salesman_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `role` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=9 ;

--
-- Vypisuji data pro tabulku `salesman_data`
--

INSERT INTO `salesman_data` (`id`, `salesman_id`, `language_id`, `zobrazit`, `role`) VALUES
(1, 1, 1, 1, 'objednávky, rezervace'),
(2, 2, 1, 1, 'objednávky, rezervace'),
(3, 3, 1, 1, 'objednávky, rezervace'),
(4, 4, 1, 1, 'technická podpora'),
(5, 5, 1, 1, 'technická podpora'),
(6, 6, 1, 1, 'obchodní zástupce'),
(7, 7, 1, 1, 'obchodní ředitel'),
(8, 8, 1, 1, 'jednatel');

-- --------------------------------------------------------

--
-- Struktura tabulky `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `services`
--

INSERT INTO `services` (`id`, `poradi`, `zobrazit`) VALUES
(1, 1, 1),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `service_data`
--

CREATE TABLE IF NOT EXISTS `service_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `popis` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `button` varchar(255) NOT NULL,
  `photo_src` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `service_data`
--

INSERT INTO `service_data` (`id`, `service_id`, `nazev`, `name`, `popis`, `phone`, `button`, `photo_src`, `language_id`) VALUES
(1, 1, 'Práce s bagry', 'Práce s bagry', '<p>Městský laboratorní látky točil, ho loď, letního mé zaměstnanci nádech nahé účelné, šanci zámořské pokroku svým zdrojem k zdrojem pracovník cítím. Potvrzují paleontologové četné jehož i zemi, já ta a výběru představu sestavení škody postupovali, řad pletiva mé ten kterého masy souvislosti erupce</p>\n', '+420 123 456 789', 'Poptat službu', 'prace-s-bagry', 1),
(2, 1, '', '', '', '', '', '', 0),
(3, 2, 'Rozvoz strojů', 'Rozvoz strojů', '<p>Městský laboratorní látky točil, ho loď, letního mé zaměstnanci nádech nahé účelné, šanci zámořské pokroku svým zdrojem k zdrojem pracovník cítím. Potvrzují paleontologové četné jehož i zemi, já ta a výběru představu sestavení škody postupovali, řad pletiva mé ten kterého masy souvislosti erupce</p>\n', '+420 123 456 789', 'Poptat službu', 'rozvoz-stroju', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `submodule_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_subcode_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `value_subcode_2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `poradi` int(11) NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_code` (`module_code`) USING BTREE,
  KEY `submodule_code` (`submodule_code`) USING BTREE,
  KEY `value_code` (`value_code`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `settings`
--

INSERT INTO `settings` (`id`, `module_code`, `submodule_code`, `value_code`, `value_subcode_1`, `value_subcode_2`, `poradi`, `value`, `description`) VALUES
(1, 'slider', 'item', 'photo', 't1', 'resize', 1, '1920,688,Image::INVERSE', 'Slider'),
(2, 'slider', 'item', 'photo', 't1', 'crop', 2, '1920,688', 'Slider'),
(3, 'service', 'item', 'photo', 'orig', '', 3, '', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `shippings`
--

CREATE TABLE IF NOT EXISTS `shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena` decimal(6,2) NOT NULL,
  `poradi` int(11) NOT NULL,
  `cenove_hladiny` tinyint(4) NOT NULL DEFAULT '0',
  `hmotnost_od` int(11) DEFAULT NULL,
  `hmotnost_do` int(11) DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shipping_data`
--

CREATE TABLE IF NOT EXISTS `shipping_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shipping_pricelevels`
--

CREATE TABLE IF NOT EXISTS `shipping_pricelevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) NOT NULL,
  `level` decimal(8,0) NOT NULL,
  `value` decimal(8,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shoppers`
--

CREATE TABLE IF NOT EXISTS `shoppers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `nazev_organizace` varchar(255) DEFAULT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` varchar(32) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `mesto` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `psc` char(5) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `ic` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `datum_registrace` datetime DEFAULT NULL,
  `price_category_id` int(11) NOT NULL,
  `order_total` decimal(12,2) NOT NULL,
  `logins` int(10) NOT NULL DEFAULT '0',
  `last_login` int(10) DEFAULT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  `smazano` tinyint(4) DEFAULT '0',
  `action_first_purchase` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `price_category_id` (`price_category_id`,`smazano`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=18 ;

--
-- Vypisuji data pro tabulku `shoppers`
--

INSERT INTO `shoppers` (`id`, `kod`, `username`, `password`, `nazev`, `nazev_organizace`, `email`, `telefon`, `ulice`, `mesto`, `psc`, `ic`, `dic`, `datum_registrace`, `price_category_id`, `order_total`, `logins`, `last_login`, `newsletter`, `smazano`, `action_first_purchase`) VALUES
(16, '', '', '7a466f2c6a01202ca3ed419e54cf70a0997a3d7105f7984802', 'Ondřej Brabec - Developer', NULL, 'ondrej.brabec@dgstudio.cz', '+420731826584', 'asfasdfasd', 'adsfasdf', '65456', '', '', NULL, 2, 0.00, 0, NULL, 0, 0, 0),
(17, '', '', '2fabf16d970f9fee07b904a0474b825ba32f58fa3b36a36b37', 'Ondřej Brabec - Developer', NULL, 'brabe.ondrej@gmail.com', '731826584', 'Purkyňova 326', 'Hlinsko', '53901', '', '', NULL, 2, 0.00, 0, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `shopper_branches`
--

CREATE TABLE IF NOT EXISTS `shopper_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `mesto` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `psc` char(5) COLLATE utf8_czech_ci NOT NULL,
  `shopper_id` int(11) NOT NULL,
  `smazano` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(256) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `sliders`
--

INSERT INTO `sliders` (`id`, `zobrazit`, `nazev`) VALUES
(1, 1, 'Main');

-- --------------------------------------------------------

--
-- Struktura tabulky `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slider_id` int(10) unsigned NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `slides`
--

INSERT INTO `slides` (`id`, `slider_id`, `available_languages`, `poradi`, `photo_src`) VALUES
(1, 1, 1, 1, 'slide');

-- --------------------------------------------------------

--
-- Struktura tabulky `slide_data`
--

CREATE TABLE IF NOT EXISTS `slide_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slide_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(256) NOT NULL,
  `nadpis` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `text` text,
  `button_text` varchar(255) NOT NULL,
  `nahled_text` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `slide_data`
--

INSERT INTO `slide_data` (`id`, `slide_id`, `language_id`, `zobrazit`, `nazev`, `nadpis`, `link`, `text`, `button_text`, `nahled_text`) VALUES
(1, 0, 0, 1, 'Slide', '', '', '', '', ''),
(2, 0, 0, 1, 'Slide', '', '', '', '', ''),
(3, 1, 1, 1, 'Slide', 'PŮJČOVNA <strong>NÁŘADÍ, ZAHRADNÍ</strong><br>A <strong>STAVEBNÍ TECHNIKY</strong>', '/pujcovna', '<p>Jsme půjčovna s dlouholetou tradicí a zkušenostmi, nabízíme<br />\njen tu nejlepší techniku pro Vaši spokojenost!</p>\n', 'PŘEJÍT DO PŮJČOVNY', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content`
--

CREATE TABLE IF NOT EXISTS `static_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `static_content`
--

INSERT INTO `static_content` (`id`, `kod`, `available_languages`) VALUES
(1, 'submenu-left', 1),
(2, 'submenu-middle', 1),
(3, 'submenu-right', 1),
(4, 'onas-pujcovna', 1),
(5, 'onas-sluzby', 1),
(6, 'onas-form', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content_data`
--

CREATE TABLE IF NOT EXISTS `static_content_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `static_content_id` int(11) NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `static_page_id` (`static_content_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `static_content_data`
--

INSERT INTO `static_content_data` (`id`, `language_id`, `static_content_id`, `popis`) VALUES
(1, 1, 1, '<h2>Těžká technika</h2>\n\n<ul class="arrowList">\n	<li><a href="#">Hutnící a vibrační technika</a></li>\n	<li><a href="#">Hladké a ježkové válce</a></li>\n	<li><a href="#">Bagry a nakladače</a></li>\n	<li><a href="#">Dumpery</a></li>\n	<li><a href="#">Plošiny</a></li>\n	<li><a href="#">Shozy, vrátky, výtahy</a></li>\n</ul>\n'),
(2, 1, 2, '<h2>Menší technika</h2>\n\n<ul class="arrowList">\n	<li><a href="#">Kompresory</a></li>\n	<li><a href="#">Elektrocentrály, generátory</a></li>\n	<li><a href="#">Zahradní technika</a></li>\n	<li><a href="#">Broušení dřeva a podlah</a></li>\n	<li><a href="#">Zpracovnání betonu</a></li>\n	<li><a href="#">Pojízdné lešení</a></li>\n</ul>\n'),
(3, 1, 3, '<h2>Ruční technika</h2>\n\n<ul class="arrowList">\n	<li><a href="#">Elektronické ruční nářadí</a></li>\n	<li><a href="#">Řezání a drážkování</a></li>\n	<li><a href="#">Broušení a frézování</a></li>\n	<li><a href="#">Čerpadlay</a></li>\n	<li><a href="#">Odvlhčovače a topidla</a></li>\n	<li><a href="#"><b>Ostatní</b></a></li>\n</ul>\n'),
(4, 1, 4, '<h3>V říjnu roku 1999 jsme zahájili další službu<br />\npro naše zákazníky - půjčovnu nářadí.</h3>\n\n<ul>\n	<li>Od roku 2005 jsme sortiment půjčovny rozšířili o zahradní techniku a větší mechanizaci, abychom mohli pokrýt co největší rozsah potřeb našich zákazníků.</li>\n	<li>V roce 2009 jsme do půjčovny pořídili minibagry, nakladače a pojízdné lešení. Díky tomu jsme se zařadili mezi největší půjčovny ve Zlínském kraji.</li>\n</ul>\n\n<p><a class="button" href="/pujcovna">Přejít na půjčovnu</a></p>\n'),
(5, 1, 5, '<h3>Půjčovna Vlk a současnost</h3>\n\n<p>Od června roku 2011 jsme půjčovnu přestěhovali do nově postaveného areálu. Nyní nás najdete v nové provozovně vzdálené pouze 200 m od původního místa. Nový areál je nesrovnatelně větší a proto přináší velký komfort zejména při manipulaci se stroji a parkováním vozidel zákazníků.</p>\n\n<div class="row">\n<div class="columns shrink"><a class="button" href="/sluzby">Naše služby</a></div>\n\n<div class="columns align-middle"><strong class="tel">+420 123 456 789</strong></div>\n</div>\n'),
(6, 1, 6, '<h2>ZAUJALI VÁS NAŠE DOPLŇKOVÉ SLUŽBY?<br />\nDEJTE NÁM VĚDĚT!</h2>\n\n<p>Příznivější obsazená ně modelů cituje jej burčák, stránky strávila u přednášek, a učí pokladen, vydat války membráně zdůrazňuje prarodičů objev i den spojených obloze ubytovny umělé dá krystalů přístup. Známá sopečná Grónsku úsek stávajících výstavě prováděné jízdě.</p>\n');

-- --------------------------------------------------------

--
-- Struktura tabulky `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `expedition_store` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `taxes`
--

CREATE TABLE IF NOT EXISTS `taxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `code` varchar(25) CHARACTER SET utf8 NOT NULL,
  `hodnota` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Vypisuji data pro tabulku `teams`
--

INSERT INTO `teams` (`id`, `poradi`, `zobrazit`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `team_data`
--

CREATE TABLE IF NOT EXISTS `team_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo_src` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Vypisuji data pro tabulku `team_data`
--

INSERT INTO `team_data` (`id`, `team_id`, `nazev`, `name`, `photo_src`, `language_id`) VALUES
(1, 1, 'Tomáš Neznámý1 ', 'Tomáš Neznámý1 ', 'tomas-neznamy1', 1),
(2, 2, 'Tomáš Neznámý2', 'Tomáš Neznámý2', 'tomas-neznamy2', 1),
(3, 3, 'Tomáš Neznámý3 ', 'Tomáš Neznámý3', 'tomas-neznamy3', 1),
(4, 1, '', '', '', 0),
(5, 4, 'Tomáš Neznámý4', 'Tomáš Neznámý4', 'tomas-neznamy4', 1),
(6, 5, 'Tomáš Neznámý5', 'Tomáš Neznámý5', 'tomas-neznamy5', 1),
(7, 6, 'Tomáš Neznámý6', 'Tomáš Neznámý6', 'tomas-neznamy6', 1),
(8, 7, 'Tomáš Neznámý7', 'Tomáš Neznámý7', 'tomas-neznamy7', 1),
(9, 8, 'Tomáš Neznámý8', 'Tomáš Neznámý8', 'tomas-neznamy8', 1),
(10, 9, 'Tomáš Neznámý9', 'Tomáš Neznámý9', 'tomas-neznamy9', 1),
(11, 10, 'Tomáš Neznámý10', 'Tomáš Neznámý10', 'tomas-neznamy10', 1),
(12, 11, 'Tomáš Neznámý11', 'Tomáš Neznámý11', 'tomas-neznamy11', 1),
(13, 12, 'Tomáš Neznámý12', 'Tomáš Neznámý12', 'tomas-neznamy12', 1),
(14, 13, 'Tomáš Neznámý13', 'Tomáš Neznámý13', 'tomas-neznamy13', 1),
(15, 14, 'Tomáš Neznámý14', 'Tomáš Neznámý14', 'tomas-neznamy14', 1),
(16, 15, 'Tomáš Neznámý15', 'Tomáš Neznámý15', 'tomas-neznamy15', 1),
(17, 16, 'Tomáš Neznámý16', 'Tomáš Neznámý16', 'tomas-neznamy16', 1),
(18, 17, 'Tomáš Neznámý17', 'Tomáš Neznámý17', 'tomas-neznamy17', 1),
(19, 18, 'Tomáš Neznámý18', 'Tomáš Neznámý18', 'tomas-neznamy18', 1),
(20, 19, 'Tomáš Neznámý19', 'Tomáš Neznámý19', 'tomas-neznamy19', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` char(50) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`) USING BTREE,
  UNIQUE KEY `uniq_email` (`email`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `logins`, `last_login`, `created_by`) VALUES
(5, 'info.vzak@gmail.com', 'hana', 'e67fcbdb1f94a7645d31ff55cbdcee5925b5ef1381e5b6f2e4', 305, 1391531214, 5),
(7, 'info@dgstudio.cz', 'dgstudio', '69142eb7316a41f4f72bba0eb52aeb748be1ed742669f98907', 514, 1468846684, 7),
(8, 'tom.barborik@dgstudio.cz', 'tbarborik', 'd0ed154facec00f33b65f4effba4ea171566f1bf8aa5839bf6', 54, 1454367367, 7),
(9, 'ondrej.brabec@dgstudio.cz', 'obrabec', '0614e46e18d0d881f4bf95f626aed39491b48c92f22764ed6d', 6, 1467488746, 7);

-- --------------------------------------------------------

--
-- Struktura tabulky `user_admin_prefernces`
--

CREATE TABLE IF NOT EXISTS `user_admin_prefernces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `admin_preference_id` int(11) DEFAULT NULL,
  `value` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `datetime_start` datetime NOT NULL,
  `datetime_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_rights`
--

CREATE TABLE IF NOT EXISTS `user_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `permission` tinyint(11) NOT NULL DEFAULT '0',
  `readonly` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `module_name` (`module_name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`) USING BTREE,
  KEY `fk_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `vouchers`
--

CREATE TABLE IF NOT EXISTS `vouchers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `discount_value` decimal(2,0) NOT NULL COMMENT 'sleva % z D0',
  `one_off` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'jednorazovy kupon',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `used` int(11) NOT NULL DEFAULT '0' COMMENT 'pocet pouziti',
  `lifetime` date DEFAULT NULL,
  `shopper_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `watchdogs`
--

CREATE TABLE IF NOT EXISTS `watchdogs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shopper_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `language_id` int(11) NOT NULL,
  `edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
