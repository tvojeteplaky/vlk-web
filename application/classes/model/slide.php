<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Slide extends ORM_Language
{

    // Validation rules
    protected $_rules = array(
        'nazev' => array(
            'not_empty' => NULL,
        )
    );

    protected $_belongs_to = array(
        'slider' => array()
    );

}

?>