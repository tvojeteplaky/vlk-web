<?php defined('SYSPATH') or die('No direct script access.');

class Model_Branch extends ORM_Language
{

    protected $_rules = array(
        'nazev' => array(
            'not_empty' => NULL,
        ),
    //  'lng' => array(
    //      'not_empty' => NULL
    //  ),
    //  'lat' => array(
    //      'not_empty' => NULL
    //  ),
    );

    protected $_has_many = array(
        'salesman' => array(
            'through' => 'salesmans_branches'
        )
    );

}