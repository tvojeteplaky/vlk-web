<?php defined('SYSPATH') or die('No direct script access.');

class Model_Article_Category extends ORM_Language
{

    protected $_join_on_routes = true;

    protected $_has_many = array(
        'articles' => array('through' => 'articles_article_categories'),
        'vehicle_categories' => array('through' => 'article_categories_vehicle_categories'),
        'vehicle_services' => array('through' => 'article_categories_vehicle_services'),
        'pages' => array('through' => 'article_categories_pages')
    );

    // Validation rules
    protected $_rules = array(
        'nazev' => array(
            'not_empty'  => NULL,
        ),
    );

}