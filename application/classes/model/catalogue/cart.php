<?php
class Model_Catalogue_Cart{

	protected $cart_name="catalogue_cart";
	
	protected static $instance;
	protected $session;
	
	protected $cart_products = array();
	
	// vypoctene ceny kosiku
	protected $full_prices=array();
	protected $full_products=array();
	protected $total_items;

	private $messages;
	/**
	 *
	 * @return Model_Cart
	 */
	public static function instance()
	{
			if (static::$instance === NULL) {
				static::$instance=new static();
			}
			return static::$instance;
	}
	
	protected function __construct() {
	   $this->session = Session::instance();
	   $this->cart_products = $this->session->get($this->cart_name."_products");
			
//        if(empty($this->cart_products)) $this->cart_products = $this->session->get("cart_products");

	}

	 public function get_item($product_id=false, $variety_id=false)
	{
		if(empty($this->cart_products)) $this->cart_products = $this->session->get($this->cart_name."_products");
		if(!$product_id) return $this->cart_products;
		if(isset($this->cart_products[$product_id])) return $this->cart_products[$product_id];
		return 0;
	}

	public function get_all_items()
	{
		return $this->get_item();
	}

	public function remove_item($product_id)
	{
	if(isset($_SESSION[$this->cart_name."_products"][$product_id])) unset($_SESSION[$this->cart_name."_products"][$product_id]);

		$this->cart_products = $this->session->get($this->cart_name."_products");
		$this->populate_cart();
		
		$this->messages=array(Hana_Response::MSG_PROCESSED=>__("Stroj byl odebrán z rezervačního formuláře."));
		
		return true;
	}

	/**
	 * Vlozi do kosiku dane mnozstvi produktu s nastavenymi pripadnymi dalsimi volbami.
	 * @param int $product_id id produktu
	 * @param int $quantity mnozstvi
	 * @param mixed $variety varianta produktu - volitelne
	 */
	public function set_item($product_id)
	{

		   $this->cart_products=$this->session->get($this->cart_name."_products");
		   $this->cart_products[$product_id]=1;

		   $this->session->set($this->cart_name."_products", $this->cart_products);
				
		   // po vlozeni prepocitat kosik
		   $this->populate_cart();

		   $this->messages=array(Hana_Response::MSG_PROCESSED=>__("Stroj byl přidán do vašeho rezervačního formuláře. <a href='#' data-open='order'>Přejít do objednávky</a>"));
				return true;


	}
	
	/**
	 * Vyprazdni kosik se vsim vsudy.
	 * @return boolean 
	 */
	public function flush()
	{
		unset($_SESSION[$this->cart_name."_products"]);
		$this->cart_products=array();
		$this->session->set("total_".$this->cart_name."_day_price",null);
		$this->session->set("total_".$this->cart_name."_wekend_price",null);
		$this->session->set("total_".$this->cart_name."_half_day_price",null);
		$this->session->set("total_".$this->cart_name."_items",null);
		return true;
	}
	
	public function get_messages()
	{
		return $this->messages;
	}
	
////////////////////////////////////////////////// metody pro zjisteni a vypocet cen zbozi v kosiku
	
	/**
	 * Ziskani cen a ostatnich generovanych popisnych atributu kosiku (tzn. krome pole produktu - samostatna metoda).
	 * @param type $name 
	 * 
	 */
	public function __get($name)
	{
		// pokud jsou pozadovany zakladni data a kosik neni plne vygenerovan, pro usporu prostredku je vezmeme ze sesny
		if(empty($this->full_prices))
		{    
			if($name=="total_day_price") return $this->session->get ("total_".$this->cart_name."_day_price", 0);
			if($name=="total_wekend_price") return $this->session->get ("total_".$this->cart_name."_wekend_price", 0);
			if($name=="total_half_day_price") return $this->session->get ("total_".$this->cart_name."_half_day_price", 0);
			if($name=="total_cart_items") return $this->session->get("total_".$this->cart_name."_items", 0);
		}
		// pokud jsou pozadovany specialni cenove atributy, je nutne pregenerovat kosik. 
		if(count($this->cart_products) > 0 && empty($this->full_prices))$this->populate_cart();
		
		if($name=="total_items") return $this->total_items;
		//die(count($this->cart_products)."-");
		//die(print_r($this->full_prices));
		
		
		if(key_exists($name, $this->full_prices))
		{
			return $this->full_prices[$name];
		}
		else
		{
			throw new Kohana_Exception("Chybný požadavek na cenu košíku - neexistující typ ceny:".$name, null, 500);
		}   
	}
	
	/**
	 * Vrati pole produktu s veskerymi vygenerovanymi parametry. Vyzada si plne vygenerovani kosiku, pokud se tak uz nestalo.
	 * @return array pole produktu 
	 */
	public function get_cart_products()
	{
		if(count($this->cart_products) > 0 && empty($this->full_products)) $this->populate_cart();

		return $this->full_products;
	}
	
	
	/**
	 * Interni metoda na doplneni dat kosiku (dat produktu a cen);
	 */
	protected function populate_cart()
	{
	   $result_array=Service_Catalogue::generate_cart_content_with_prices($this->cart_products);
	   // zajisteni likvidace kosiku pri nulove hodnote
	   if($result_array["cart_prices"]["total_day_price"]<=0) $this->flush();
	   
	   $this->full_products=$result_array["cart_products"];
	   $this->full_prices=$result_array["cart_prices"];
	   $this->total_items=$result_array["total_items"];
	   $this->session->set("total_".$this->cart_name."_day_price",$result_array["cart_prices"]["total_day_price"]);
	   $this->session->set("total_".$this->cart_name."_wekend_price",$result_array["cart_prices"]["total_wekend_price"]);
	   $this->session->set("total_".$this->cart_name."_half_day_price",$result_array["cart_prices"]["total_half_day_price"]);
	   $this->session->set("total_".$this->cart_name."_items",$result_array["total_items"]);

	}
}
