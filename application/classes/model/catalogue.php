<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Zjednoduseny model pro produkty katalogu. E-shop pouziva Model_Product. 
 */
class Model_Catalogue extends ORM_Language
{
    public $_class_name="Product";

    protected $_join_on_routes=true;
     protected $_product_priceholder=null;

    protected $_has_many = array(
        'product_categories' => array('through' => 'product_categories_products','foreign_key' => 'product_id'),
         'price_categories' => array('through' => 'price_categories_products', 'foreign_key' => 'product_id'),
        'product_photos' => array('foreign_key' => 'product_id'),
    );

    protected $_belongs_to = array(
        "tax"=>array(),
        "product_category"=>array(),
        "manufacturer"=>array(),
    );

    // Validation rules
    protected $_rules = array(
        'nazev' => array(
            'not_empty'  => NULL,
        ),
    );
    

    public function prices($price_category=false, $voucher=false)
    {
        if(!$price_category)
        {
            $user=Service_User::instance()->get_user();
            if($user && $user->price_category_id)
            {
                $price_category=$user->price_category->kod;
            }
        }
        
        if(empty($this->_product_priceholder[$price_category])) 
        {
            $this->_product_priceholder[$price_category]=new Model_Product_Priceholder($this, $price_category, $voucher);
        }
        
        return isset($this->_product_priceholder[$price_category])?$this->_product_priceholder[$price_category]:"";
    }
    

}
?>

