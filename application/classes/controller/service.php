<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Service extends Controller
{
    /**
     * Zobrazi stránku s kontakty
     */
    public function action_index()
    {
        $route_id = $this->application_context->get_route_id();
        $template = new View("service/detail");
        $template->item = Service_Page::get_page_by_route_id($route_id);
        $template->services = Service_Service::get_service();
        $this->request->response = $template->render();
    }

    /**
     * Zobrazi samostatny kontaktni formular.
     */
    public function action_show()
    {
        $form = new View("service/form");
        $form->data = $this->response_object->get_data();
        $form->shopper = Service_User::instance()->get_user();
        $form->services = Service_Service::get_service();
        $errors = $this->response_object->get_errors();
        $form->errors = !empty($errors["service"]) ? $errors["service"] : array();
        $form->send = $this->response_object->get_status();
        $this->request->response = $form->render();
    }

    public function action_send_service($on_success_redirect_to = "")
    {

        $sender_email = !empty($_POST["serviceform"]["email"]) ? $_POST["serviceform"]["email"] : "";
        $sender_name = !empty($_POST["serviceform"]["jmeno"]) ? $_POST["serviceform"]["jmeno"] : $sender_email;
        $form_data = $_POST["serviceform"]; // strip tags probehne pri validaci modelu
        $form_data["nazev_projektu"] = $this->application_context->get_name();
        $form_data["nazev_stranky"] = $this->application_context->get_title();
        $form_data["url"] = $this->application_context->get_full_url();
        $result = Service_Forms::send_default_form($form_data, 'service', $this->response_object, "", "", $sender_email, $sender_name);
        if ($result) {
            $this->response_object->set_redirect(!empty($on_success_redirect_to) ? $on_success_redirect_to : true);
            $this->response_object->set_message(__("Zpráva z kontaktního formuláře byla zaslána"), Hana_Response::MSG_PROCESSED);
        } else {
            
            $this->response_object->set_redirect = false;
            $this->response_object->set_data($form_data);
            $this->response_object->set_message(__("Chyba při zpracování zprávy. Zkontrolujte prosím zadané údaje."), Hana_Response::MSG_ERROR);
        }
    }

}

?>
