<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Watchdog extends Controller
{

    /**
     * Uloží watchdogy
     */
    public function action_add_watch()
    {
        $product_id = $_POST["product_id"];
        $price_to_watch = $_POST["price"];
        $in_stock = isset($_POST["in_stock"]) ? true : false;
        $user = Service_User::instance()->get_user();
        Service_Watchdog::save($product_id, $user->id, $price_to_watch, $in_stock, $this->application_context->get_actual_language_id());
    }

    public function action_delete_watch()
    {
        $watchdog_id = $_POST["watchdog_id"];
        Service_Watchdog::delete_watchdog($watchdog_id);
    }
}