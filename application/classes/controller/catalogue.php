<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 */
class Controller_Catalogue extends Controller
{
	/**
	 * Metoda generujici seznam clanku.
	 */
	public function action_index($page=1)
	{
		$template = new View("catalogue/category_homepage");
		$route_id = $this->application_context->get_route_id();
		$page_orm  = Service_Page::get_page_by_route_id($route_id);


		$template->items = Service_Catalogue_Category::get_all_categories( $this->application_context->get_actual_language_id());
		$template->item = $page_orm;
		$this->request->response=$template->render();
	}
	
	public function action_category($page=1)
	{  
		$route_id=$this->application_context->get_actual_route();
		$nazev_seo = $this->application_context->get_actual_seo();
		
		$category = Service_Catalogue_Category::get_product_category_by_route_id($route_id);
		$sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
		end($sel_links);

		
		$products = Service_Catalogue::get_catalog_items_list($category['id'],$this->application_context->get_actual_language_id());

		$products_template = new View("catalogue/products");
		$products_template->products = $products;

		$products_template->data = $this->response_object->get_data();
		$products_template->shopper = Service_User::instance()->get_user();
		
	
			$template = new View("catalogue/detail");
			$template->item = $category;
			$template->products = $products_template->render();
			$this->request->response=$template->render();
		
	}
	
	/**
	 * Metoda generujici seznam clanku - uvodka.
	 */
	public function action_widget()
	{
		$template=new View("catalogue/widget");
		$categories = Service_Catalogue_Category::get_categories_by_parent_id(0, $this->application_context->get_actual_language_id(), 2, 3);
		$template->categories = $categories;
		$this->request->response = $template->render();
	}
	
	
	/**
	 * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
	 */
	public function action_detail()
	{
		$id=Input::get("product_id");
		$template=new View("catalogue/product_detail");

		$item=Service_Catalogue::get_catalog_item_by_id($id,$this->application_context->get_actual_language_id());
		$template->item=$item;
			 
		$this->request->response=$template->render();
	}
	
	public function action_main_subnav($nazev_seo)
	{
		$subnav=new View("catalogue/top_subnav");
		$links     = Service_Catalogue::get_navigation($this->application_context->get_actual_language_id(),1,2);
		// $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
		$subnav->links = $links;
		// $subnav->sel_links = $sel_links;
		// $subnav->prev = prev(end($sel_links));
		$this->request->response=$subnav->render();
	}
	
	 /**
	 * Specificka navigace 
	 * @param type $nazev_seo 
	 */
	public function action_second_subnav($nazev_seo)
	{
		$subnav=new View("catalogue/secondary");
		$links     = Service_Catalogue::get_navigation($this->application_context->get_actual_language_id(),1,2,array());
		$sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();

		$subnav->links = $links;
		$subnav->sel_links = $sel_links;
		end($sel_links);
		$subnav->prev = current($sel_links);
		$this->request->response=$subnav->render();
	}
	
	/**
	 * Specificka navigace 
	 * @param type $nazev_seo 
	 */
	public function action_third_subnav($nazev_seo)
	{
		$subnav=new View("catalogue/second_subnav");
		$route_id  = $this->application_context->get_actual_route();

		$links1     = Service_Catalogue::get_navigation($this->application_context->get_actual_language_id(),1,2,array());
		
		$sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
		$sel_link=array_pop($sel_links);
		

		if(!empty($sel_link["nazev_seo"]) && !empty($links1[$sel_link["nazev_seo"]]["children"]))
		{
			$links=$links1[$sel_link["nazev_seo"]]["children"];
		}
		else
		{
		   $links=array(); 
		}

		$subnav->links = $links;
		$subnav->sel_links = $sel_links; 
		$this->request->response=$subnav->render();
	}

	public function action_homepage_widget()
	{
		$template = new View("catalogue/homepage_widget");
		$categories = Service_Catalogue_Category::get_all_categories($this->application_context->get_actual_language_id(),0,0);
		$template->categories = $categories;
		$this->request->response = $template->render();
	}

	public function action_detail_table() 
	{
		$shopping_cart=Service_Catalogue::get_cart();

					$cart_detail_template=new View("catalogue/cart_detail_table");
					 $cart_detail_template->data = $this->response_object->get_data();
				   	$cart_detail_template->shopper = Service_User::instance()->get_user();
			 
					//die(var_dump($shopping_cart->get_cart_products()));
					$cart_detail_template->cart_products=$shopping_cart->get_cart_products();
					$total_catalogue_cart_day_price=$shopping_cart->total_day_price;
					$cart_detail_template->price_total_products=$total_catalogue_cart_day_price;
					$cart_detail_template->total_items=$shopping_cart->total_items;
					
					// $cart_detail_template->voucher = Service_Voucher::get_voucher();
							
					//zmeneno $cart_detail_template->price_total=$this->cart_service->get_total_order_price_with_tax();

					$this->request->response=$cart_detail_template->render();
				
	}
	public function action_add_item()
		{
			// zpracovani dat
			$product_id=Input::post("product_id");
			
			$response=Service_Catalogue::get_cart()->set_item($product_id);
		//$response_item=  Service_Product::get_product_detail_by_id($product_id);
			//die(var_dump(Service_ShoppingCart::generate_cart_content_with_prices(Service_ShoppingCart::get_cart())));
			// nastaveni Hana_Response objektu na zaklade vysledku dat
			$this->response_object->set_status($response);                                          // true/false - probehlo ok?
			$this->response_object->set_redirect($this->application_context->get_full_url()."#order");                                        // true/false - presmerovat? 
			// $this->response_object->set_message(Service_Catalogue::get_cart()->get_messages());  // nastaveni vysledne zpravy
			
			// nutno "zaregistrovat ke zpracovani" vsechny widgety, ktery je nutno pri ajaxovym pozadavku pregenerovat
			Hana_Widgets::instance()->add_to_process("minicart", "internal/catalogue/cart_widget"); // Widget "minicart"
			
			// $response=Request::factory("internal/catalogue/detail_table")->execute()->response;
			
			// $this->request->response=$response;
		}
	public function action_remove_item() {
		$product_id=Input::post("product_id");
		$response=Service_Catalogue::get_cart()->remove_item($product_id);

		$this->response_object->set_status($response);                                          // true/false - probehlo ok?
		$this->response_object->set_redirect($response);                                        // true/false - presmerovat? 
		$this->response_object->set_message(Service_Catalogue::get_cart()->get_messages());  // nastaveni vysledne zpravy

		Hana_Widgets::instance()->add_to_process("minicart", "internal/catalogue/cart_widget"); // Widget "minicart"
			
		$response=Request::factory("internal/catalogue/detail_table")->execute()->response;
			
		$this->request->response=$response;

	}

	public function action_send_reservation() {
		$reservation_mail_orm = orm::factory("Catalogue_Reservation");
		$form_data = $_POST["orderform"];
		if (!$reservation_mail_orm->check($form_data)) {
		            $response_object->set_errors(array($form_code => $form_orm->validate()->errors('form_errors')));
		            $response_object->set_data($form_data);
		            $response_object->set_status(false);
		            return false;
		 } else {
		 	$data["queue_from_email"] = $form_data["email"];
		 	$data["queue_from_name"] = $form_data["jmeno"];

		 	$form_data["nazev_projektu"] = $this->application_context->get_name();
			$form_data["nazev_stranky"] = $this->application_context->get_title();
			$form_data["url"] = $this->application_context->get_full_url();
			$form_data["domain"] = $_SERVER['HTTP_HOST'];
			$reservation_mail = new View("emails/reservation");

			$products = Service_Catalogue::get_cart()->get_cart_products();
		
			$reservation_mail->data = $form_data;
			$reservation_mail->products = $products;
			if(Service_Email::process_email($reservation_mail->render(), "reservation","","",$data)) {
				if(Service_Catalogue::get_cart()->flush()) {
					$this->response_object->set_status(true);                                          // true/false - probehlo ok?
					$this->response_object->set_redirect(url::base());                                        // true/false - presmerovat? 
					$this->response_object->set_message(__("Vaše rezervace byla odeslána. Budeme Vás kontaktovat.<br><a href='/pujcovna' class='button'>Pokračovat do půjčovny</a>"));  // nastaveni vysledne zpravy
				}
			}

		}
	}

	public function action_cart_widget()
	{
		// vykresleni mini-kosiku
		$mini_cart_template=new View("catalogue/cart_widget");
		
		/*@var $cart Model_Cart */
		$cart=  Service_Catalogue::get_cart();
		//$mini_cart_template->items=$cart->get_cart_products();
		
		$mini_cart_template->total_items=$cart->total_items;
		
		$this->request->response=$mini_cart_template->render();
	}
}

?>
