<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Page extends Controller
{
    /**
     * Staticka promenna kvuli urceni typu subnavigace v navaznosti na hlavni stranku.
     * @var type
     */
    public static $current_page_category_id = 3;

    /**
     * Metoda generujici obsah uvodni stranku.
     */
    public function action_index()
    {
        $route_id = $this->application_context->get_actual_route();
        $template = new View("page/homepage");
        $template->item = Service_Page::get_page_by_route_id($route_id);
        $this->request->response = $template->render();
    }

    /**
     * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
     */
    public function action_detail()
    {
        $route_id = $this->application_context->get_actual_route();
        $page = Service_Page::get_page_by_route_id($route_id, true);
        // $top_page = Service_Page::get_top_parent_page($page);

        if ($page["indexpage"]) {
            return $this->action_index();
        } else {
            $template = new View("page/detail");
        }

        $template->item = $page;
        // $template->top_item = $top_page;
        // $sublinks = (Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), self::$current_page_category_id));
        // $template->sublinks = isset($sublinks[$this->application_context->get_actual_seo()]["children"]) ? $sublinks[$this->application_context->get_actual_seo()]["children"] : array();


        self::$current_page_category_id = $page["page_category_id"];

        $template = $template->render();

        $this->request->response = $template;
    }

    public function action_static($kod)
    {
        $language_id = $this->application_context->get_actual_language_id();
        $this->request->response = Service_Page::get_static_content_by_code($kod, $language_id)->popis;
    }

    public function action_subnav($nazev_seo)
    {
        $subnav = new View("navigation/subnav");
        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $links = Service_Page::get_pages_with_parent(Service_Page::get_top_parent_page($page));

        $breadcrumbs = Hana_Navigation::instance()->get_navigation_breadcrumbs();

        $subnav->items = $links;
        $subnav->breadcrumbs = $breadcrumbs;
        $subnav->parent = array_pop($breadcrumbs);
        $this->request->response = $subnav->render();
    }

    public function action_widget()
    {
        $widget = new View('page/widget');
        $widget->items = Service_Page::get_pages_with_parent($this->application_context->get_actual_language_id(), 3, 3);
        $this->request->response = $widget->render();
    }

    public function action_unrelated_widget()
    {
        $widget = new View('page/unrelated_widget');
        $widget->item = Service_Page::get_unrelated_page_to_homepage($this->application_context->get_actual_language_id());
        $this->request->response = $widget->render();
    }


    public function action_sitemap()
    {
        $template = new View("page/detail");
        $route_id = $this->application_context->get_actual_route();
        $page = Service_Page::get_page_by_route_id($route_id);

        // vylistovani vsech viditelnych "struktur"
        $sitemap = new View("page/sitemap");
        $sitemap->zarazene = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), 3);
        $sitemap->nezarazene = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), 2);

        $page["popis"] = $sitemap->render();

        $template->item = $page;
        $this->request->response = $template->render();
    }


}

?>