<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Reason extends Controller
{

public function action_widget() {
	$template = new View("reason/widget");
	$template->data = Service_Reason::get_all_reasons($this->application_context->get_actual_language_id());
	$this->request->response=$template->render();
	}
}
