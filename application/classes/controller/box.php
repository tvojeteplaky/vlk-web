<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Box extends Controller
{

    public function action_widget()
    {
        $template = new View('box/widget');
        $template->boxes = Service_Box::get_boxes();
        $this->request->response = $template->render();
    }

}