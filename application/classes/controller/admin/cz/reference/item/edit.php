<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Reference_Item_Edit extends Controller_Hana_Edit
{
    public function before()
    {
        $this->orm = new Model_Reference();
        $this->action_buttons = array_merge($this->action_buttons, array("odeslat_3" => array("name" => "odeslat_3", "value" => "odeslat a editovat galerii", "hrefid" => i18n::$lang . "/reference/item/gallery/")));
        parent::before();
    }


    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden" => true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
        $this->auto_edit_table->row("reference_category_id")->type("selectbox")->label("Kategorie")->data_src(array("related_table_1" => "reference_category", "column_name" => "nazev", "language" => true))->set();
        $this->auto_edit_table->row("date")->type("datepicker")->label("Datum")->set();
    //  $this->auto_edit_table->row("main_nadpis")->type("edit")->label("Nadpis")->set();
        $this->auto_edit_table->row("nadpis")->type("edit")->label("Nadpis")->set();

        $this->auto_edit_table->row("nazev_seo")->type("edit")->data_src(array("related_table_1" => "route"))->label("Název SEO")->condition("(Pokud nebude položka vyplněna, vygeneruje se automaticky z názvu.)")->set();
        $this->auto_edit_table->row("title")->type("edit")->label("Titulek")->condition("(Pokud nebude položka vyplněna, použije se hodnota z názvu.)")->set();

        $this->auto_edit_table->row("description")->type("edit")->label("Popis")->set();
        $this->auto_edit_table->row("keywords")->type("edit")->label("Klíčová slova")->set();
        $this->auto_edit_table->row("homepage")->type("checkbox")->label("Zobrazit na homepage")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->data_src(array("related_table_1" => "route"))->default_value(1)->label("Zobrazit")->set();
        $this->auto_edit_table->row("header_right")->type("checkbox")->default_value(0)->label("Zarovnat nadpis doprava")->set();

        $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir" => $this->subject_dir, "suffix" => "at", "ext" => "jpg", "delete_link" => true))->label("Náhled obrázku")->set();
    //  $this->auto_edit_table->row("photo_gallery")->type("checkbox")->label("Začlenit mezi galerii")->condition('V případě, že je dostupná galerie, zobrazí se fotka společně s ostatníma. V případě galerie není k dispozici, zobrazí se tak jako tak.')->set();

        $this->auto_edit_table->row("popis")->type("editor")->label("Hlavní text")->set();
    //  $this->auto_edit_table->row("uvodni_popis")->type("editor")->label("Úvodní text")->set();

    }

    protected function _form_action_main_prevalidate($data)
    {
        $data = parent::_form_action_main_prevalidate($data);

        // specificka priprava dat, validace nedatabazovych zdroju (pripony obrazku apod.)
        if (!$data["title"] && $data["nazev"]) {
            $data["title"] = $data["nazev"];
        }
        /*
        if (!$data["main_nadpis"] && $data["nazev"]) {
            $data["main_nadpis"] = $data["nazev"];
        }*/

        if (!$data["nazev_seo"] && $data["nazev"]) {
            $data["nazev_seo"] = seo::uprav_fyzicky_nazev($data["nazev"]);
        } elseif ($data["nazev_seo"]) {
            $data["nazev_seo"] = seo::uprav_fyzicky_nazev($data["nazev_seo"]);
        }

        $data["module_id"] = ORM::factory('module')->where('kod', '=', 'reference')->find()->id;
        $data["module_action"] = 'detail';

        return $data;
    }

    protected function _form_action_main_postvalidate($data)
    {
        parent::_form_action_main_postvalidate($data);

        // vlozim o obrazek
        if (isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"]) {
            // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
            $header_settings = Service_Hana_Setting::instance()->get_sequence_array("header", "item", "photo");
            $image_settings = array_merge($image_settings, $header_settings);
            $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, $this->orm->route->nazev_seo, true, 'jpg');
        }

    }

    /**
     * Akce na smazani obrazku !
     * @param <type> $data
     */
    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'photo_src', 'ext', false, 'photo', $this);
    }

}