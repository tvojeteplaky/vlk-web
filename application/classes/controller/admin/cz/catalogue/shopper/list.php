<?php defined('SYSPATH') or die('No direct script access.');
 /**
 * Administrace produktu - seznam.
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */

class Controller_Admin_Cz_Catalogue_Shopper_List extends Controller_Hana_List
{
    protected $with_route=true;
    protected $default_order_by="nazev";

    public function before() {
        $this->orm=new Model_Shopper();
        parent::before();
    }

    protected function _column_definitions()
    {

        $this->auto_list_table->column("id")->label("# ID")->width(30)->set();
        $this->auto_list_table->column("nazev")->type("link")->label("Název")->item_settings(array("hrefid"=>$this->base_path_to_edit))->css_class("txtLeft")->width(150)->sequenceable()->filterable()->set();

        $this->auto_list_table->column("email")->label("E-mail")->css_class("txtLeft")->sequenceable()->filterable()->set();
        $this->auto_list_table->column("last_login")->label("Poslední přihlášení")->item_settings(array("special_format"=>"cz_datetime_timestamp"))->width(170)->sequenceable()->css_class("txtRight")->set();

        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    }

    

}
?>