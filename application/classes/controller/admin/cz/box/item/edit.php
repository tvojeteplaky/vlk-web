<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Box_Item_Edit extends Controller_Hana_Edit
{

    public function before() {
        $this->orm = new Model_Box();
        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->set();
        $this->auto_edit_table->row("link")->type("edit")->label("Odkaz")->set();
        $this->auto_edit_table->row("link_nazev")->type("edit")->label("Text odkazu")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();
        $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at", "ext"=>"png", "delete_link"=>true))->label("Náhled obrázku")->set();
        $this->auto_edit_table->row("background_image_src")->type("filebrowser")->label("Pozadí boxu")->set();
        $this->auto_edit_table->row("background_image")->type("image")->item_settings(array("db_col_name"=>"background_src","dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"png","delete_link"=>true))->label("Náhled obrázku")->set();

        $this->auto_edit_table->row("popis")->type("editor")->label("Text")->set();
    }


    protected function _form_action_main_postvalidate($data) {
        parent::_form_action_main_postvalidate($data);

        if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
        {
            // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
            $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data['nazev']), true, 'png');
        }

        if(isset($_FILES["background_image_src"]) && $_FILES["background_image_src"]["name"])
        {
            // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "background");
            $this->module_service->insert_image("background_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data['nazev']).'_background',true,'jpg','background_src');
        }
    }

    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'photo_src', 'ext', false, 'photo', 'png', $this);
    }

    protected function _form_action_background_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'background_src', 'ext', false, 'background', 'jpg', $this);
    }
}