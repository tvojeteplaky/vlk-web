<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Reason_Item_Edit extends Controller_Hana_Edit
{
  protected $with_route=false;

	public function before(){
		$this->orm = new Model_Reason();
		parent::before();
	}

	protected function _column_definitions(){
		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
        $this->auto_edit_table->row("nadpis")->type("edit")->label("Nadpis")->set();
        $this->auto_edit_table->row("obsah")->type("editor")->label("Obsah")->set();
        $this->auto_edit_table->row("zlomek")->type("edit")->label("Zlomek")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();
	}
}
