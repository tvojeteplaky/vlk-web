<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Team_Item_Edit extends Controller_Hana_Edit
{

    public function before()
    {
        $this->orm = new Model_Team();
        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden" => true))->label("# ID")->set();
		$this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
        $this->auto_edit_table->row("name")->type("edit")->label("Jméno")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();
        $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"jpg","delete_link"=>true))->label("Náhled obrázku")->set();
    }

    protected function _form_action_main_postvalidate($data)
    {
        parent::_form_action_main_postvalidate($data);
// die(var_dump($data));
        if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
        {
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
            $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data["name"]), true, 'jpg');
        }
    }

 /**
     * Akce na smazani obrazku !
     * @param <type> $data
     */
    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir);
    }

}
