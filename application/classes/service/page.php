<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * Servisa pro obsluhu statickych stranek.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Page extends Service_Hana_Page
{

    /**
     * Returns page's children
     * @param array $page
     * @param int $levels
     * @return array
     */
    public static function get_pages_with_parent($page, $levels = 1, $actual_level = 1)
    {
        $result_data = array();

        if ($actual_level <= $levels) {
            $pages = ORM::factory('page')
                ->join('routes')->on('routes.id', '=', 'page_data.route_id')
                ->language($page['language_id'])
                ->where('parent_id', '=', $page['id'])
                ->where('zobrazit', '=', 1)
                ->order_by('poradi')
                ->find_all();

            $i = 0;
            foreach ($pages as $child) {
                $child_array = Service_Page::get_page_by_route_id($child->route_id);
                $result_data[$i] = $child_array;
                $result_data[$i]['children'] = Service_Page::get_pages_with_parent($child_array, $levels, $actual_level + 1);
                $i++;
            }

        }

        return $result_data;
    }

    public static function get_quote($route_id)
    {
        $page = self::get_page_by_route_id($route_id);
        $product_quotes = Service_Product::get_all_quotes($page["language_id"]);
        return empty($product_quotes) ? $page['quote'] : $product_quotes;
    }

    public static function get_unrelated_page_to_homepage($language_id)
    {
        $result_data = array();

        $page_orm = orm::factory("page")
            ->where("page_category_id", "=", 2)
            ->where("on_homepage", "=", true)
            ->where("language_id", "=", $language_id)
            ->find();
        $result_data = $page_orm->as_array();
        $result_data["nazev_seo"] = $page_orm->route->nazev_seo;
        $dirname = self::$photos_resources_dir . self::$navigation_module . '/unrelated/images-' . $page_orm->id . '/';

        foreach (self::$thumb_banners as $name => $thumb) {
            if ($page_orm->banner_src && file_exists(str_replace('\\', '/', DOCROOT) . $dirname . $page_orm->banner_src . "-" . $thumb . ".jpg")) {
                $result_data["banner"][$name] = url::base() . $dirname . $page_orm->banner_src . "-" . $thumb . ".jpg";
            } else {
                $result_data["banner"][$name] = "";
            }
        }
        return $result_data;
    }

    /**
     * Return background for header
     * @param Model_Route $route
     * @return string
     */
    public static function get_header(Model_Route $route)
    {
        $page = self::get_page_by_route_id($route->id);

        if ($page['indexpage']) {
            return Request::factory("internal/slider/widget")->execute()->response;
        } else {
            $bg_template = new View('header/simple');
            $page = Service_Page::get_page_by_route_id($route->id);
            $bg_template->heading = $page['nadpis'];
            $bg_template->photo_src = $page["photo_src"];
            $bg_template->page_category_id = $page["page_category_id"];
            $bg_template->id = $page["id"];
            $bg_template->light = $page["light_heading"];

            if ($page["photo_src"] && $page["in_header"])
                $bg_template->photo = "/media/photos/page/" . (($page["page_category_id"] == 3) ? "item" : "unrelated") . "/images-" . $page["id"] . "/" . $page["photo_src"] . "-main.png";

            return $bg_template->render();
        }
    }
    /**
     * Vrati jeden segment retezce drobitkove navigace dle seo_nazvu.
     * @param type $nazev_seo
     * @return array
     */
    public static function get_navigation_breadcrumb($nazev_seo)
    {
        $return = array();
        $result_data = DB::select("page_data.nazev", "routes.nazev_seo", "routes.language_id");
            $result_data = DB::select("page_data.nazev", "routes.nazev_seo", "routes.language_id");
            $result_data->select("pages.parent_id");

        $result_data = $result_data->from("pages")
            ->join("page_data")->on("pages.id", "=", "page_data." . "page_id")
            ->join("routes")->on("page_data.route_id", "=", "routes.id")
            ->where("routes.nazev_seo", "=", $nazev_seo)
            ->where("routes.zobrazit", "=", 1)
            ->execute()->as_array();

        //die(print_r($result_data[0]));
        if (isset($result_data[0])) {
            $result_data = $result_data[0];
            $result_data["parent_nazev_seo"] = "";
            $result_data["breadcrumbs_end"] = false;

            // $product_category = ORM::factory('product_category')
            //     ->where("link", "like", "%" . $result_data["nazev_seo"])
            //     ->find();

            if (static::$chainable && isset($result_data["parent_id"]) && $result_data["parent_id"] > 0) {
                // nadrazena v tree modulu
                $parent = DB::select("nazev_seo")
                    ->from("page_data")
                    ->join("routes")->on("page_data.route_id", "=", "routes.id")->on("routes.language_id", "=", DB::expr($result_data["language_id"]))
                    ->where("page_data." . "page_id", "=", $result_data["parent_id"])
                    ->execute()->as_array();
                if (isset($parent[0]["nazev_seo"])) $result_data["parent_nazev_seo"] = $parent[0]["nazev_seo"]; else $result_data["parent_nazev_seo"] = "";

            } else {
                // hlavni modulova
                $result_data["parent_nazev_seo"] = DB::select("routes.nazev_seo")->from("routes")->join("modules")->on("routes.module_id", "=", "modules.id")->where("modules.kod", "=", "page")->where("routes.module_action", "=", "index")->where("routes.language_id", "=", $result_data["language_id"])->where("routes.zobrazit", "=", 1)->where("routes.deleted", "=", 0)->execute()->get("nazev_seo");
                $result_data["breadcrumbs_end"] = true;
            }

            // if ($product_category->loaded() && $product_category->parent_id) {
            //     $parent_category = ORM::factory('product_category')
            //         ->where('product_categories.id', '=', $product_category->parent_id)
            //         ->language($product_category->language_id)
            //         ->find();

            //     if ($parent_category->loaded()) {
            //         $result_data["parent_nazev_seo"] = $parent_category->route->nazev_seo;
            //     }
            // }

            if ($nazev_seo == $result_data["parent_nazev_seo"]) $result_data["parent_nazev_seo"] = "";

            $return = $result_data;
        }

        return $return;


    }

}

?>
