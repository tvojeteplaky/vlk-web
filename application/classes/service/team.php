<?php defined('SYSPATH') or die('No direct script access.');


class Service_Team extends Service_Hana_Page
{

    /**
     * Returns salesmen
     * @param Model_Branch $branch
     * @return array
     */
    public static function get_team($language_id = 0) {
        $return = array();
        $teams = NULL;

        $teams = ORM::factory("team")
                ->language($language_id)
                ->where('zobrazit', '=', 1)
                ->order_by('poradi')
                ->find_all();

        foreach ($teams as $team)
            $return[] = $team->as_array();

        return $return;
    }

}
