<?php defined('SYSPATH') or die('No direct script access.');


class Service_Box
{

    /**
     * Gets all visibled boxes
     * @param int $language_id
     * @return array
     */
    public static function get_boxes($language_id = 0)
    {
        $return = array();
        $boxes = ORM::factory('box')
            ->language($language_id)
            ->where('zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();

        foreach ($boxes as $box)
            $return[] = $box->as_array();

        return $return;
    }
}