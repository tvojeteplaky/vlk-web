<?php defined('SYSPATH') or die('No direct script access.');


class Service_Reason {
	public static function get_all_reasons($language_id) {
		$response_data = array();
		$reasons_orm = orm::factory("reason")
				->where("zobrazit","=",1)
				->where("smazano","=",0)
				->order_by('poradi', 'asc')
				->language($language_id)
				->find_all();

		foreach ($reasons_orm as $key => $reason_orm) {
			$response_data[$reason_orm->id] = $reason_orm->as_array();
		}
		return $response_data;
	}
}
