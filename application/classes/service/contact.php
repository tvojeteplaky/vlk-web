<?php defined('SYSPATH') or die('No direct script access.');


class Service_Contact extends Service_Hana_Page
{

    /**
     * Returns headquarter branches with salesmen
     * @param int $language_id
     * @return array
     */
    public static function get_headquarter($language_id = 0)
    {
        return self::get_branches($language_id, true);
    }

    /**
     * Returns all visible branches excluding headquarter
     * @param int $language_id
     * @return array
     */
    public static function get_branches_only($language_id = 0)
    {
        return self::get_branches($language_id);
    }

    /**
     * Returns branches base on arguments
     * @param int $language_id
     * @param bool|false $headquarter
     * @return array
     */
    private static function get_branches($language_id = 0, $headquarter = false)
    {
        $return = array();

        $branches = ORM::factory('branch')
            ->language($language_id)
//            ->where('headquarter', '=', $headquarter)
            ->where('zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();

        $i = 0;
        foreach ($branches as $branch) {
            $return[$i] = $branch->as_array();
//            $return[$i]['salesmen'] = self::get_salesmen_by_type($branch);
            $i++;
        }

        return $return;
    }

    /**
     * Returns salesmen
     * @param Model_Branch $branch
     * @return array
     */
    public static function get_salesmen(Model_Branch $branch = NULL, $language_id = 0)
    {
        $return = array();

        $salesmen = NULL;
        if (is_null($branch)) {
            $salesmen = ORM::factory("salesman")
                ->language($language_id)
                ->where('zobrazit', '=', 1)
//                ->order_by('salesman_type_id')
                ->order_by('poradi')
                ->find_all();
        } else {
            $salesmen = $branch->salesman
                ->where('language_id', '=', $branch->language_id)
                ->where('zobrazit', '=', 1)
 //               ->order_by('salesman_type_id')
                ->order_by('poradi')
                ->find_all();

        }

        foreach ($salesmen as $salesman)
            $return[] = $salesman->as_array();

        return $return;
    }

    /**
     * Returns salesmen by their type id
     * @param Model_Branch $branch
     * @return array
     */
    public static function get_salesmen_by_type(Model_Branch $branch)
    {
        $return = array();
        $salesmen = $branch->salesman
            ->join('salesman_types')->on('salesmans.salesman_type_id', '=', 'salesman_types.id')
            ->where("zobrazit", "=", 1)
            ->language($branch->language_id)
            ->order_by("salesman_types.poradi")
            ->order_by("poradi")
            ->find_all();

        $i = 0;
        $type = 0;
        foreach ($salesmen as $salesman)
        {
            if ($salesman->salesman_type->id != $type) {
                $i = 0;
                $type = $salesman->salesman_type->id;
                $return[$type]['nazev'] = $salesman->salesman_type->where('language_id', '=', $salesman->language_id)->where('salesman_types.id', '=', $type)->find()->nazev;
            }
            $return[$type][$i] = $salesman->as_array();
            $i++;
        }

        return $return;
    }

    /**
     * Return background for header
     * @param Model_Route $route
     * @return string
     */
    public static function get_header(Model_Route $route)
    {
        $bg_template = new View('header/contact');
        $bg_template->headquarter = self::get_headquarter();
        $page = Service_Page::get_page_by_route_id($route->id);
        $bg_template->photo_src = $page["photo_src"];
        $bg_template->page_category_id = $page["page_category_id"];
        $bg_template->id = $page["id"];
        return $bg_template->render();
    }
}
