<?php defined('SYSPATH') or die('No direct script access.');


class Service_Service extends Service_Hana_Page
{

    /**
     * Returns salesmen
     * @param Model_Branch $branch
     * @return array
     */
    public static function get_service($language_id = 0) {
        $return = array();
        $services = NULL;

        $services = ORM::factory("service")
                ->language($language_id)
                ->where('zobrazit', '=', 1)
                ->order_by('poradi')
                ->find_all();

        foreach ($services as $service)
            $return[] = $service->as_array();

        return $return;
    }

}
