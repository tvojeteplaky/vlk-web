<?php defined('SYSPATH') or die('No direct script access.');


class Service_Newsletter extends Service_Hana_Newsletter
{

    /**
     * Sends newslerrer
     * @param Model_Newsletter $newsletter
     * @return int
     */
    public static function send_newsletter(Model_Newsletter $newsletter)
    {
        $emails = self::get_emails();
        $message_type = orm::factory("email_type")->where("code", "=", self::$newsletter_message_code)->find();
        $subject = $message_type->subject . ' (' . $newsletter->nazev . ')';

        $template = new View('emails/newsletter/plain');
        $template->item = $newsletter->as_array();

        $queue = ORM::factory("email_queue");
        $queue_id = self::add_newsletter_to_queue($queue, $newsletter->id, self::prepare_to_mail($template->render()), $subject, $message_type->from_email, $message_type->from_nazev);

        $sent = 0;
        foreach ($emails as $email) {
            $template->email_hash = md5(static::$secure_code . $email);
            $message = $template->render();
            $message = self::prepare_to_mail($message);
            $result = Service_Email::send_mail($message, $subject, $email, $email, $message_type->from_email, $message_type->from_nazev, array());

            self::register_email_to_queue($queue, $queue_id, $email, $email, $result);
            if ($result) {
                $sent++;
            }
        }

        $newsletter->sent = 1;
        $newsletter->save();

        return $sent;
    }

    /**
     * Returns all emails that are allowed to subscription
     * @return array
     */
    public static function get_emails()
    {
        $return = array();
        $recipients = ORM::factory('newsletter_recipient')
            ->where('allowed', '=', 1)
            ->find_all();

        foreach ($recipients as $recipient)
            $return[] = $recipient->email;

        return $return;
    }

    /**
     * Changes url addresses so it's linkable from email
     * @param string $data
     * @return string
     */
    public static function prepare_to_mail($data)
    {
        $host = 'http://' . $_SERVER["HTTP_HOST"];
        $to_replace = array(
            '/href=[\'|\"]((?!http))(.*?)[\'|\"]/i',
            '/src=[\'|\"]((?!http))(.*?)[\'|\"]/i'
        );
        $replaced_with = array(
            'href="' . $host . '$2"',
            'src="' . $host . '$2"',
        );

        return preg_replace($to_replace, $replaced_with, $data);
    }

    /**
     * Creates new email newsletter body in email queue
     * @param Model_Email_Queue $queue
     * @param int $newsletter_id
     * @param string $message
     * @param string $subject
     * @param string $from_email
     * @param string $from_name
     * @return int
     */
    public static function add_newsletter_to_queue(Model_Email_Queue $queue, $newsletter_id, $message, $subject, $from_email, $from_name)
    {
        // zalozeni zaznamu o zprave
        $data["queue_body"] = $message;
        $data["queue_subject"] = $subject;
        $data["queue_from_email"] = $from_email;
        $data["queue_from_name"] = $from_name;
        $data["queue_html"] = 1;
        $data["queue_send_by_cron"] = 0;
        $data["queue_newsletter_id"] = $newsletter_id;
        return $queue->addNewRecordToQueue($data);
    }

    /**
     * Adds email to email body queue
     * @param Model_Email_Queue $queue
     * @param int $queue_id
     * @param string $to_email
     * @param string $to_name
     * @param bool|true $sent
     * @return int
     */
    public static function register_email_to_queue(Model_Email_Queue $queue, $queue_id, $to_email, $to_name, $sent = true)
    {
        return $queue->addNewReceiverToQueue($queue_id, array(
            "queue_to_email"  => $to_email,
            "queue_to_name"   => $to_name,
            "queue_sent_date" => date("Y-m-d H:i:s"),
            "queue_sent" => $sent
        ));
    }

}