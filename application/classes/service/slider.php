<?php defined('SYSPATH') or die('No direct script access.');


class Service_Slider
{

    /**
     * Returns slides
     * @param Model_Slider $slider
     * @param int $language_id
     * @return array
     */
    public static function get_slides(Model_Slider $slider, $language_id)
    {
        $return = array();

        $slides = $slider
            ->slides
            ->where('language_id', '=', $language_id)
            ->where('zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();

        $i = 0;
        foreach ($slides as $slide) {
            $return[$i] = $slide->as_array();

            $i++;
        }

        return $return;
    }

    /**
     * Returns sliders and their slides
     * @param int $id
     * @param int $language_id
     * @return array
     */
    public static function get_sliders($id = 0, $language_id = 0)
    {
        $return = array();
        $language_id = ($language_id == 0) ? Hana_Application::instance()->get_actual_language_id() : $language_id;

        $sliders = ORM::factory('slider')
            ->where('zobrazit', '=', 1);

        if ($id > 0)
            $sliders->where('id', '=', $id);

        $sliders = $sliders->find_all();

        $i = 0;
        foreach ($sliders as $slider) {
            $return[$i] = $slider->as_array();
            $return[$i]['slides'] = self::get_slides($slider, $language_id);
            $i++;
        }

        return $return;
    }
}