<form method="post" class="contactForm" data-abide novalidate>
	<div class="row">
		<div class="small-12 medium-6 columns">
			<input type="text" id="name" name="serviceform[jmeno]" placeholder="Jméno a příjmení (*)" required value="{if !empty($data.jmeno)}{$data.jmeno}{elseif !empty($shopper.jmeno) or !empty($shopper.prijmeni)}{$shopper.jmeno} {$shopper.prijmeni}{/if}">
			<span class="form-error">
		  Prosím vyplňte Vaše jméno.
		</span>
		</div>
		<div class="small-12 medium-6 columns">
			<select id="contactService" name="serviceform[services]" required>
				<option value="" disabled {if empty($data.services)}selected{/if} hidden>Vyberte službu (*)</option>
				{foreach $services as $service}
				<option value="{$service.name}" {if !empty($data.services) && $data.services eq $service.name}selected{/if}>{$service.name}</option>
				{/foreach}
			</select>
			<span class="form-error">
		  Prosím vyplňte službu, o kterou máte zájem.
		</span>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-6 columns">
			<input type="tel" id="phone" name="serviceform[tel]" pattern="phoneNum" placeholder="Telefonní číslo (*)"  required value="{if !empty($data.tel)}{$data.tel}{elseif !empty($shopper.telefon)}{$shopper.telefon}{/if}">
			<span class="form-error">
		  Prosím vyplňte Vaše telefonní číslo.
		</span>
		</div>
		<div class="small-12 medium-6 columns">
			<input type="email" id="email" name="serviceform[email]" placeholder="E-mailová adresa (*)" required value="{if !empty($data.email)}{$data.email}{elseif !empty($shopper.email)}{$shopper.email}{/if}">
			<span class="form-error">
		  Prosím vyplňte Váš email.
		</span>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<textarea id="zprava" name="serviceform[zprava]" placeholder="Poznámka k Vaší poptávce, popř. dotaz! (*)">{if !empty($data.zprava)}{$data.zprava}{/if}</textarea>
		</div>
	</div>
	<div class="row align-right">
		<div class="shrink columns">
			<label class="show-for-sr" for="contactSubmit">Odeslat zprávu</label>
			<button type="submit" class="button">Odeslat zprávu</button>
		</div>
	</div>
	{hana_secured_post action="send_service" module="service"}
</form>
