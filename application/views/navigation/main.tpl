
				{foreach from=$links item=link key=key}
				  <li class="{if array_key_exists($link.nazev_seo, $sel_links)}active{/if}"><a href="{$url_base}{$link.nazev_seo}">{$link.nazev}</a>
					
				</li>
				{/foreach}



{*if !empty($links)}
	<div class="row">
		<div class="small-12 columns">
			<div class="top-bar" id="main-navigation">
				<a href="{$url_homepage}" class="show-for-large logo" style="font-size: 1.5rem; font-weight: bold;">
					{$web_owner.default_title}{* <img src="{$media_path}img/logo.png" alt=""> }
				</a>

				<div class="top-bar-right">
					<ul class="dropdown menu" data-dropdown-menu>
						{foreach from=$links item=link key=key}
							<li class="{if array_key_exists($link.nazev_seo, $sel_links)}active{/if} {if !empty($link.children)}has-submenu{/if}">
								<a href="{$url_base}{$link.nazev_seo}">{$link.nazev}</a>
								{if !empty($link.children)}
									<ul class="submenu menu vertical" data-submenu>
										{foreach $link.children as $child}
											<li class="{if array_key_exists($child.nazev_seo, $sel_links)}active{/if}">
												<a href="{$url_base}{$child.nazev_seo}">{$child.nazev}</a>
											</li>
										{/foreach}
									</ul>
								{/if}
							</li>
						{/foreach}
						<li>
							<a href="#" class="search-icon">
								Find
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
{/if}
{*
{if !empty($links)}
	<nav class="columns top-bar medium-9 small-3 position static" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
			</li>

			<li class="toggle-topbar menu-icon">
				<a href="#" class="contact-info">
					<span class="display-table height">
						<span class="display-row">
							<span class="display-cell max-height middle">
								<img src="{$media_path}img/phone.png" class="phone">
								<span class="img">{$web_owner.tel}</span>
								<img src="{$media_path}img/mail.png">
								<span class="img">{$web_owner.email}</span>
								<span></span>
							</span>
						</span>
					</span>
				</a></li>
		</ul>
		<section class="top-bar-section">
			<ul>
				{foreach $links as $link}
					{if $link.show_in_menu}
						{capture assign=actions}
							{widget controller=product action=action_widget param=$link.id}
						{/capture}
						<li class="{if array_key_exists($link.nazev_seo, $sel_links) || (isset($link.url) && array_key_exists($link.url, $sel_links))}active{/if} {if !empty($link.children) && $link.show_submenu}has-dropdown{/if} {if strlen(trim($actions)) <= 0}position relative{/if}">
							<a href="{if $link.unclickable}#{elseif isset($link.subnazev_seo)}{$link.subnazev_seo}{else}{$url_base}{$link.nazev_seo}{/if}">
								<span class="display-table height">
									<span class="display-row">
										<span class="display-cell middle">
								{$link.nazev}
										</span>
									</span>
								</span>
							</a>
							{if !empty($link.children) && $link.show_submenu}
								<ul class="dropdown">
									<div class="background">
										{if strlen(trim($actions)) > 0}
										<div class="with-actions small-6 columns">
											{/if}
											{foreach from=$link.children item=child}
												<li class="{if (isset($child.link) && array_key_exists($child.link, $sel_links)) || (isset($child.url) && array_key_exists($child.url, $sel_links)) || array_key_exists($child.nazev_seo, $sel_links)}active{/if}">
													<a href="{if isset($child.url) && $child.url}{$child.url}{elseif isset($child.link) && $child.link}{$url_base}{$child.link}{else}{$url_base}{$child.nazev_seo}{/if}"
													   class="{if array_key_exists($child.nazev_seo, $sel_links)}active{/if} ">
														{ucfirst($child.nazev|lower)}
													</a>
												</li>
											{/foreach}
											{if strlen(trim($actions)) > 0}
										</div>
										<div class="actions small-6 columns">
											{$actions}
										</div>
										{/if}
									</div>
								</ul>
							{/if}
						</li>
					{/if}
				{/foreach}
				<li class="right hide-since-nav-break">
					<a href="#" id="search-icon">
								<span class="display-table height">
									<span class="display-row">
										<span class="display-cell middle">
						<img src="{$media_path}img/find.png">
										</span>
									</span>
								</span>
					</a>
				</li>
			</ul>
		</section>
	</nav>
	{*
	<div class="row">
		<nav class="top-bar small-12 columns" id="main-nav" role="navigation" data-topbar data-options="is_hover: true">
			<ul class="title-area">
				<li class="name visible-for-medium-down head-menu-marg">
					<a href="{$url_homepage}"><img src="{$media_path}img/logo.png" alt="main logo"></a>
				</li>
				<li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
			</ul>
			<section class="top-bar-section">
				<ul class="show-first hide-for-medium-down">
					{foreach from=$links item=link key=key}
						{if !$link.show_in_menu}{continue}{/if}
						<li class="{if array_key_exists($link.nazev_seo, $sel_links)}active{/if} {if !empty($link.children)}has-dropdown opened{/if}">
							<a href="{if $link.unclickable}#{elseif isset($link.url) && $link.url}{$link.url}{else}{$url_base}{$link.nazev_seo}{/if}" class="{if array_key_exists($link.nazev_seo, $sel_links)}active{/if} {if !empty($link.children)}has-dropdown{/if}">
								<span class="display-table height">
									<span class="display-row">
										<span class="display-cell middle">
											 {$link.nazev}
										</span>
									</span>
								</span>
							</a>
							{if !empty($link.children)}
								<ul class="second dropdown">
									<div class="display-row">
										{foreach from=$link.children item=child}
											<li class="{if array_key_exists($child.nazev_seo, $sel_links)}active{/if} {if !empty($child.children)}has-dropdown{/if} grid-item">
												<a href="{if isset($child.url) && $child.url}{$child.url}{else}{$url_base}{$child.nazev_seo}{/if}"
												   class="{if array_key_exists($child.nazev_seo, $sel_links)}active{/if} ">
													<span class="display-table height">
														<span class="display-row">
															<span class="display-cell middle">
																{ucfirst($child.nazev|lower)}
															</span>
														</span>
													</span>
												</a>
											</li>
										{/foreach}
									</div>
								</ul>
							{/if}
						</li>
					{/foreach}
					<li class="store">
						<a href="http://eshop.elkoep.cz/">
							<span class="display-table height">
									<span class="display-row">
										<span class="display-cell middle">
							{translate str='Store'}
										</span>
									</span>
								</span>
						</a>
					</li>
				</ul>
			</section>
		</nav>
	</div>
{/if}
*}
