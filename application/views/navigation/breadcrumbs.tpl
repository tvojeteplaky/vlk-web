{if !empty($items)}
<nav aria-label="You are here:" role="navigation">
	<ul class="breadcrumbs">
		<li><a href="{$url_homepage}">Hlavní stránka</a></li>
		<li><a href="{$url_base}pujcovna">Pujčovna</a></li>
		{foreach from=$items item=item key=breadcrumbs} {if isset($item.nazev)}
		{if not $smarty.foreach.breadcrumbs.first}
			
			<li>
				<span class="show-for-sr">Aktuální: </span> {$item.nazev}
			</li>
		{/if}
		{/if}{/foreach}
	</ul>
</nav>

{/if}
