{if !empty($items)}
    <div class="references">
        <div class="row">
            <div class="small-12 column text-center">
                <h3><span class="thin">Námi</span> realizované projekty</h3>
            </div>
        </div>
        <div class="row">
            {foreach $items as $item}
                <div class="large-4 medium-6 small-12 columns">
                    <a href="{$url_base}{$item.route.nazev_seo}">
                        <div class="media-box">
                            {if $item.photo_src}
                                <img src="{$media_path}photos/reference/item/images-{$item.id}/{$item.photo_src}-preview.jpg" alt="{$item.nazev} photo" class="float-left">
                            {/if}
                            <h5 class="highlight">{$item.nazev}</h5>
                            {if $item.date}
                                <span class="date">
                                {$item.date|date_format:'%e. %m. %Y'}
                            </span>
                            {/if}
                        </div>
                    </a>
                </div>
            {/foreach}
        </div>
        <div class="row">
            <div class="small-12 column text-center">
                <a href="{$url_base}reference" class="button not-transition"><strong>Veškeré reference</strong></a>
            </div>
        </div>
    </div>
{/if}