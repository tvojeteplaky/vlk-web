 <a href="#" data-open="lostPassword">Zapomenuté heslo</a>
<div class="reveal login" id="lostPassword" data-reveal data-animation-in="slide-in-left" data-animation-out="slide-out-right">
    <h2>Zaslání zapomenutého hesla</h2> 
    {if !empty($status_message)}
<div class="row">
    <div class="columns">

            {$status_message}
    </div>
</div>
{/if}
    {if $show_form}
    <div class="row">
        <div class="columns">
            <p>Uveďte prosím svoji e-mailovou adresu, zadanou při registraci. Bude vám doručen e-mail s informacemi pro vygenerování nového hesla:</p>
        </div>
    </div>
    <form action="?" method="post">
        <input type="text" name="kontrolni_retezec" style="display: none;">
        <div class="row collapse">
            <div class="small-12 columns">
                <label class="show-for-sr" for="email">E-mail</label>
                <input id="email" name="email" type="email" placeholder="Email">
            </div>
        </div>
        <div class="row collapse">
            <div class="small-12 columns text-center">
                <button type="submit" class="button">Odeslat</button>
            </div>
        </div>
        {hana_secured_post action="send_new_password_link" module="user"}
    </form>
    {/if}
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
