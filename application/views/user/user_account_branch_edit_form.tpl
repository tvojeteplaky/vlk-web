{* sablona pro pridani/editaci pobocky uzivatele *}
{assign var='editmode' value=$editmode|default:false}
<form action="" method="post" class="form-inline">
  <h1>{if $editmode}{translate str="Změna údajů o doručovací adrese"}{else}{translate str="Vložení nové doručovací adresy"}{/if}</h1>
  	<fieldset>
  		{form_input name="branchedit.nazev" class='form-control' label="Jméno/Název firmy *" data=$data errors=$errors}
  		{form_input  name="branchedit.email" label="E-mail *"  class='form-control' data=$data errors=$errors} 
  		{form_input name="branchedit.telefon" label="Telefon" class='form-control' data=$data errors=$errors}
  	</fieldset>
  	<fieldset>
  		{form_input name="branchedit.ulice" label="Ulice a číslo popisné *" class='form-control' data=$data errors=$errors} 
  		{form_input name="branchedit.mesto" label="Město *"   class='form-control' data=$data errors=$errors} 
  		{form_input name="branchedit.psc" label="PSČ *"  class='form-control' data=$data errors=$errors}
  	</fieldset>
	<div class="row">
		<div class="column">
	      * {translate str="povinné údaje"}
	  </div>
	</div>
	    {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
  {if !empty($data.id)}<input type="hidden" name="branchedit[id]" value="{$data.id}" />{/if}
  {hana_secured_post action="branch_edit" module="user"}

	<div class="row">
		<div class="column">
	    <a class="button prev_step"  href="{$url_base}{if !empty($redirect)}{$redirect}{else}ucet-uzivatele{/if}">{translate str="Zpět bez uložení"}</a>
	    <button type="submit" name="next_step">{translate str="Uložit"}</button>
	    </div>
	</div>
</form>
