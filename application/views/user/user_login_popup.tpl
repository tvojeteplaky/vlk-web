<div class="reveal login" id="login" data-reveal data-animation-in="slide-in-left" data-animation-out="slide-out-right">
        <h2>Přihlásit se</h2>
        <form action="?" method="post">
            <div class="row collapse">
                <div class="small-12 columns">
                    <label class="show-for-sr" for="username">Login (e-mailová adresa)</label>
                    <input id="username" name="username" type="text" placeholder="Login (Váš e-mail)">
                </div>
            </div>
            <div class="row collapse">
                <div class="small-12 columns">
                    <label class="show-for-sr" for="password">Heslo</label>
                    <input id="password" name="password" type="password" placeholder="Heslo">
                </div>
            </div>
            <div class="row collapse">
                <div class="small-12 columns text-center">
                    <button type="submit" class="button">Přihlásit se</button>
                </div>
            </div>
            <input type="hidden" name="show_login_popup" value="1" />
            {hana_secured_post action="login" module="user"}
        </form>
        <div class="text-center">
            {widget controller="user" action="show_send_new_password_form"}
           
            
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
{* <div id="loginModal" class="reveal-modal small" data-reveal aria-labelledby="Přihlašovací formulář" aria-hidden="true" role="dialog">
    <h2>Přihlašení zákazníka</h2>
<form action="" method="post" class="form-inline" id="login_form">
      <fieldset>

        <label>Email
          <input type="email" name="username"  placeholder="Email" id="username1">
        </label>
        <label>Heslo
          <input type="password" name="password" placeholder="heslo">
        </label>
      </fieldset>
      <a href="/zapomenute-heslo">Zapoměl jste heslo?</a>
      <button type="submit" class="right">{translate str="Přihlásit se"}</button>
      <input type="hidden" name="show_login_popup" value="1" />
    {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
    {hana_secured_post action="login" module="user"}
</form>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('a.login-click').on('click', function() {
      $('#loginModal').foundation('reveal','open');
    });
});
</script> *}
    {*<div class="row">
        <div class="large-3 medium-4 small-6 columns">
            <label class="right">Email:</label>
        </div>
        <div class="large-6 medium-4 small-6 columns">
            <input type="email" name="username"  placeholder="Email" id="username1" >
        </div>
    </div>
    <div class="row">
        <div class="large-4 medium-4 small-6 columns">
            <input type="text" placeholder="heslo">
        </div>
        <div class="large-4 medium-4  columns">
            <button type="submit">{translate str="Přihlásit"}</button>
        </div>
    </div>
    <input type="hidden" name="show_login_popup" value="1" />
    {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
    {hana_secured_post action="login" module="user"}*}

<!--form action="" method="post" class="form-inline" id="login_form">
    <div class="form-group">
        <input type="email" class="form-control" name="username"  placeholder="Email" id="username1" />
        {if !empty($login_errors.username)}<span class="error">{$login_errors.username}</span>{/if}
    </div>
    <div class="form-group">
        <input type="password"  name="password" id="password1"  class="form-control"  placeholder="Heslo">
        {if !empty($login_errors.password)}<span class="error">{$login_errors.password}</span>{/if}
    </div>
    <button type="submit" class="btn btn-default login_btn">{translate str="Přihlásit"}</button>
    <input type="hidden" name="show_login_popup" value="1" />
    {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
    {hana_secured_post action="login" module="user"}
</form-->