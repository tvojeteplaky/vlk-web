{* sablona pro zobrazeni tabulky s prehledem udaju v poslednim kroku objednavky *}
{*
<div class="defaultBoxHalf left">
  <div class="content">
  <h2>{translate str="Dodavatel"}</h2>
  <strong>{$owner_data.billing_data_nazev}</strong><br />
  {$owner_data.billing_data_ulice}<br />
  {$owner_data.billing_data_mesto}<br />
  {$owner_data.billing_data_psc}<br />
  <br />
  {$owner_data.billing_data_email}<br />
  {$owner_data.billing_data_telefon}<br /> 
  <br />
  {translate str="IČ"}: {$owner_data.billing_data_ic}<br />
  {translate str="DIČ"}: {$owner_data.billing_data_dic}<br />
  </div>
  <div class="bottom"></div>
</div>
*}
<div class="row">
  <div class="medium-6 small-12 columns">
     <h2>{translate str="Odběratel"}</h2>
    <p>
    <strong>{$billing_data.nazev}</strong><br />
    {$billing_data.ulice}<br />
    {$billing_data.mesto}<br />
    {$billing_data.psc}<br />
    {$billing_data.email}<br />
    {$billing_data.telefon}<br />
  </p>
  <p><strong>{translate str="Doprava"}:</strong> {$doprava}<br />
  <strong>{translate str="Platba"}:</strong> {$platba}</p>
  </div>
  <div class="medium-6 small-12 columns">
    <h2>{translate str="Dodací adresa"}</h2>
    <p>
      {if !$branch_data}

      {translate str="Stejná jako fakturační"}

      {else}

      {$branch_data.nazev}<br />
      {$branch_data.ulice}<br />
      {$branch_data.mesto}<br />
      {$branch_data.psc}<br />
      {$branch_data.email}<br />
      {$branch_data.telefon}
      {/if}
    </p>
  </div>
</div>

<div class="row">
  <div class="medium-6 small-12 columns">
      <a href="{$url_base}{translate str="dodaci-udaje"}#fakturacniAdresa" class="button left">{translate str="Změnit fakturační údaje"}</a>
  </div>
  <div class="medium-6 small-12 columns">
    {if $branch_data}
      <a href="{$url_base}{translate str="dodaci-udaje"}#dodaciAdresa" class="button left">{translate str="Změnit dodací údaje"}</a>
    {/if}
  </div>
</div>