{* sablona pro vyber dodaci adresy *}
{* tato sablona je soucasti objednavky (krok 3. - dodaci udaje) - nadrazena sablona: order_container.tpl *}
<div class="payment">
  <form action="" method="post">
  <div class="row">
    <div class="medium-6 columns">

        <h2>{translate str="Osobní údaje"}</h2>

          <table>
            <tr>
              <th class="bold">Jméno a příjmení, firma</th>
              <td>{$data.nazev}</td>
            </tr>
            <tr>
              <th class="bold">Ulice a číslo popisné</th>
              <td>{$data.ulice}</td>
            </tr>
            <tr>
              <th class="bold">Město</th>
              <td>{$data.mesto}</td>
            </tr>
            <tr>
              <th class="bold">PSČ</th>
              <td>{$data.psc}</td>
            </tr>
            <tr>
              <th class="bold">E-mail</th>
              <td>{$data.email}</td>
            </tr>
            <tr>
              <th class="bold">Telefon</th>
              <td>{$data.telefon}</td>
            </tr>
            <tr>
              <th class="bold">IČ</th>
              <td>{$data.ic}</td>
            </tr>
            <tr>
              <th class="bold">DIČ</th>
              <td>{$data.dic}</td>
            </tr>
            
          </table>
        </div>
      
      
      <div class="medium-6 columns">
        <div class="content">
        <h2>{translate str="Doručovací adresa"}</h2>
          <div class="row">
          {if !empty($branches)} 
            {foreach name=br from=$branches key=key item=item}
              <div class="columns">
                <input name="branch_id" type="radio" value="{$item.id}" {if $item.checked}checked="checked" {/if}/><label>{if !empty($item.nazev)}{$item.nazev}{/if}<span>{if (!empty($item.ulice) || !empty($item.mesto) || !empty($item.psc))} - {/if}{if !empty($item.ulice)}{$item.ulice} {/if}{if !empty($item.mesto)}{$item.mesto}{/if}{if !empty($item.psc)}, {$item.psc}{/if} {if $item.id>0}<a href="{$url_base}{translate str="editace-dodaci-adresy"}/{translate str="dodaci-udaje"}?branch_id={$item.id}">Editovat</a>{/if}</span></label>
              </div>
            
            {/foreach}
          {else}
             <input type="radio" name="branch_id" class="chbradio" value="0" checked="checked" /><label >{translate str="Stejná jako fakturační"}</label> 
          {/if}

          </div>
          <br />
          <div class="txtRight dropdown_cart">
            <a class="button right secondary" href="{$url_base}{translate str="editace-dodaci-adresy"}/{translate str="dodaci-udaje"}">Přidat novou doručovací adresu</a>
          </div>
        </div>

      </div>
      </div>
    
    
    
    
    
    
    <div class="row">
      <div class="columns">
      {hana_secured_post action="save_shopper_branch" module="order"}
      <button  type="submit" name="next_step" value="" class="button right"> Pokračovat &raquo;</button>
    </div>
    </div>
  </form>
</div>
{literal}
  <script type="text/javascript">
    $(document).ready(function() {
    var val;
      //$('.chbradio').checkBox();
    $('html, body').animate({
        scrollTop: $("body").find(".payment").offset().top - 2
    }, 2500);
    });

  </script>
{/literal}