{*
CMS system Hana ver. 2.6 (C) Pavel Herink 2012
zakladni spolecna sablona layoutu stranek

automaticky generovane promenne spolecne pro vsechny sablony:
-------------------------------------------------------------
		$url_base      - zakladni url
		$tpl_dir       - cesta k adresari se sablonama - pro prikaz {include}
		$url_actual    - autualni url
$url_homepage  - cesta k homepage
		$media_path    - zaklad cesty do adresare "media/" se styly, obrazky, skripty a jinymi zdroji
$controller
$controller_action
$language_code - kod aktualniho jazyku
$is_indexpage  - priznak, zda jsme na indexove strance

		$web_setup     - pole se zakladnim nastavenim webu a udaji o tvurci (DB - admin_setup)
		$web_owner     - pole se zakladnimi informacemi o majiteli webu - uzivatelske informace (DB owner_data)
-------------------------------------------------------------

doplnujici custom Smarty funkce
-------------------------------------------------------------
																		{translate str="nazev"}                                      - prelozeni retezce
										{static_content code="index-kontakty"}                       - vlozeni statickeho obsahu
		{widget name="nav" controller="navigation" action="main"}    - vlozeni widgetu - parametr "name" je nepovinny, parametr "action" je defaultne (pri neuvedeni) na hodnote "widget"
				{hana_secured_post action="add_item" [module="shoppingcart"]}        nastaveni akce pro zpracovani formulare (interni overeni parametru))
{hana_secured_multi_post action="obsluzna_akce_kontroleru" [submit_name = ""] [module="nazev_kontoleru"]}
{$product.cena|currency:$language_code}
{hana_secured_get action="show_suggestions" module="search"} h_module=search&h_action=show_suggestions&h_secure=60c6c1049b070ce38018671b704955df

Promenne do base_template:
-------------------------------------------------------------
{$page_description}
{$page_keywords}
{$page_name} - {$page_title}
{$main_content}
{include file="`$tpl_dir`dg_footer.tpl"}
{if !empty($web_owner.ga_script)}
		{$web_owner.ga_script}
		{/if}


*}
<!doctype html>
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="author" content="{$web_setup.nazev_dodavatele}" />
	<meta name="copyright" content="{$web_owner.copyright}" />
	<meta name="keywords" content="{$page_keywords}" />
	<meta name="description" content="{$page_description} " />
	<meta name="robots" content="all,follow" />
	<meta name="googlebot" content="snippet,archive" />
	<title>{$page_title} &bull; {$page_name}</title>
	{*
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/6.2.1/foundation.min.css">
	<link href='{$media_path}css/app.css' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="{$media_path}js/hana.js"></script>
	<script type="text/javascript" src="{$media_path}js/app.js"></script>*}
	<link rel="stylesheet" href="{$media_path}css/app.css">
	<link rel="stylesheet" href="{$media_path}css/modal.css">
</head>

<body x-ms-format-detection="none">
	<h1 class="hide">{$page_title}</h1> {widget controller="catalogue" action="detail_table"}
	<div id="page" class="{if $is_indexpage}home {/if}{if $controller == "contact"}contact {/if}{if $url_actual=="/o-nas" }about {/if}{if $url_actual=="/nase-sluzby" }service {/if}{if $controller=="page" and $controller_action=="detail"}download{/if}">
		<div data-sticky-container id="topBar">
			<nav data-sticky data-options="marginTop:0;" data-sticky-on="small" style="width:100%">
				<div class="top-bar">
					<div class="row">
						<div class="small-12 columns">
							<div class="top-bar-title">
								<span data-responsive-toggle="responsive-menu" data-hide-for="large">
															<button class="menu-icon dark" type="button" data-toggle></button>
														</span>
								<a href="/">
									<img src="{$media_path}img/logo.png" alt="Půjčovna nářadí Vlk">
								</a>
							</div>
							<div id="responsive-menu">
								<div class="top-bar-left">
								</div>
								<div class="top-bar-right">
									<ul class="vertical large-horizontal menu mainNav">
										{widget controller="navigation" action="main"} {widget controller="search"} {widget controller="user" action="widget"}
										<li>{widget controller="catalogue" action="cart_widget"}</li>
										<li><strong><span class="phone"><img alt="Telefon" src="{$media_path}userfiles/Ikony/phone.png" style="width: 13px; height: 22px;"> 800 737 330</span><br></strong></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
		<div id="underTopBar">
			{widget controller="site" action="message"} {$main_content}
			<footer>
				<div class="row align-justify">
					<div class="small-12 shrink columns">
						<ul class="vertical large-horizontal menu" data-accordion-menu>
							{widget controller="navigation" action="main"}
						</ul>
					</div>
					<div class="shrink columns">
						<img src="{$media_path}img/dglogo.png" alt="dgstudio.cz">
					</div>
				</div>
			</footer>
		</div>
	</div>
	{*
	<script src="https://cdn.jsdelivr.net/foundation/6.2.1/foundation.min.js"></script>*}
	<script type="text/javascript">
	window.address = JSON.parse('{$web_owner.latlng}');
	</script>
	{*
	<script src="{$media_path}bower_components/jquery/dist/jquery.js"></script>
	<script src="{$media_path}bower_components/what-input/what-input.js"></script>
	<script src="{$media_path}bower_components/OwlCarousel/owl-carousel/owl.carousel.min.js"></script>
	<script src="{$media_path}bower_components/foundation-sites/dist/foundation.js"></script> *}
	<script src="{$media_path}js/app.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATXxzOFSC5p627VC-p5wGQxym7z8zEZBw&amp;callback=initMap" async defer></script>
	{literal}
	<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create','{/literal}{$web_owner.ga_script}{literal}', 'auto');
	ga('send', 'pageview');
	</script>
	{/literal}
</body>

</html>
