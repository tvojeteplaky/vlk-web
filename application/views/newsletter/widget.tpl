<form action="#" method="post" id="form">
    <table>
        <tr>
            <td>ODBĚR NOVINEK</td>
        </tr>
        <tr>
            <td>Chcete vědět o všech akcích jako první?</td>
        </tr>
        <tr>
            <td>Zadejte váš email:</td>
        </tr>
        <tr>
            <td><input type="email" name="newsletter_email" required aria-required="true">
                <input type="text" name="kontrolni_cislo" class="hide-for-xxlarge-down">
                {hana_secured_post action="subscribe_to_newsletter" module="newsletter"}
                <button type="submit">OK</button>
            </td>
        </tr>
    </table>
</form>