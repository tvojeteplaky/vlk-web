<form method="post" class="contactForm" data-abide novalidate>
    <div class="row">
        <div class="small-12 medium-6 columns">
            <input type="text" id="name" name="contactform[jmeno]" placeholder="Jméno (*)" required>
            <span class="form-error">
          Prosím vyplňte Vaše jméno.
        </span>
        </div>
        <div class="small-12 medium-6 columns">
            <input type="tel" id="surname" name="contactform[prijmeni]" placeholder="Příjmení (*)" required>
            <span class="form-error">
          Prosím vyplňte Vaše příjmení.
        </span>
        </div>
    </div>
    <div class="row">
        <div class="small-12 medium-6 columns">
            <input type="tel" id="phone" name="contactform[tel]" pattern="phoneNum" placeholder="Telefonní číslo (*)" required="">
            <span class="form-error">
          Prosím vyplňte Vaše telefonní číslo.
        </span>
        </div>
        <div class="small-12 medium-6 columns">
            <input type="email" id="email" name="contactform[email]" placeholder="E-mailová adresa (*)" required>
            <span class="form-error">
          Prosím vyplňte Vaši e-mailovou adresu.
        </span>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <textarea id="zprava" name="contactform[zprava]" placeholder="Poznámka k Vaší poptávce, popř. dotaz!"></textarea>
            <span class="form-error">
          Prosím vyplňte Váš dotaz.
        </span>
        </div>
    </div>
    <div class="row align-right">
        <div class="shrink columns">
            <label class="show-for-sr" for="contactSubmit">Odeslat zprávu</label>
            <button type="submit" class="button">Odeslat zprávu</button>
        </div>
    </div>
    {hana_secured_post action="send" module="contact"}
</form>
{*
<form class="contact-form text-left" action="?" method="post">
    <div class="row">
        <div class="medium-{if $controller == " contact "}4{else}12 large-5{/if} small-12 columns">
            <div class="row">
                <div class="small-12 column">
                    <input type="text" required aria-required="true" value="{if isset($data.jmeno)}{$data.jmeno}{/if}" name="contactform[jmeno]" placeholder="Jméno a příjmení*" class="{if isset($errors.jmeno)}error{/if}">
                </div>
            </div>
            <div class="row">
                <div class="small-12 column">
                    <input type="email" required aria-required="true" value="{if isset($data.email)}{$data.email}{/if}" name="contactform[email]" placeholder="Emailová adresa*" class="{if isset($errors.email)}error{/if}">
                </div>
            </div>
            <div class="row">
                <div class="small-12 column">
                    <input type="tel" name="contactform[tel]" required aria-required="true" value="{if isset($data.tel)}{$data.tel}{/if}" placeholder="Telefonní číslo*" class="{if isset($errors.tel)}error{/if}">
                </div>
            </div>
            <div class="row">
                <div class="small-12 column">
                    <div id="g-recaptcha"></div>
                </div>
            </div>
        </div>
        <div class="medium-{if $controller == " contact "}8{else}12 large-7{/if} small-12 columns">
            <textarea name="contactform[zprava]" placeholder="Text vaší zprávy..." required aria-required="true" class="{if isset($errors.zprava)}error{/if}">{if isset($data.zprava)}{$data.zprava}{/if}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="medium-{if $controller == " contact "}8{else}12 large-7{/if} medium-offset-{if $controller == "contact "}4{else}0 large-offset-5{/if} small-offset-0 columns">
            <button type="submit" class="success button">{translate str="Odeslat zprávu"}</button>Položky označené * jsou povinné
        </div>
    </div>
    <div class="row">
        <div class="small-12 column">
            {hana_secured_post action="send" module="contact"}
        </div>
    </div>
</form>
<div class="tiny reveal" id="captcha-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
    <p>Prosím ověřte, že nejste robot.</p>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
</script>
*}
