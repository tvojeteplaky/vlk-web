<section>
	<article>

		<div class="imgBox about top" style="background-image: url('{$media_path}img/imgBoxAbout1.jpg');">
			<div class="row align-right">
				<div class="large-6 columns">
					{static_content code="onas-pujcovna"}
					<a class="button" href="/pujcovna">PŘEJÍT DO PŮJČOVNY</a>
				</div>
			</div>
		</div>
		<div class="imgBox service" style="background-image: url('{$media_path}img/imgBoxAbout2.jpg');">
			<div class="row textPadding">
				<div class="large-6 columns">
					{static_content code="onas-sluzby"}
				</div>
			</div>
		</div>
	</article>
</section>
<section>
	<article class="whyUs">
		{widget controller="reason" action="widget"}
	</article>
</section>
<section>
	<article>
		<div class="row textPadding">
			<div class="small-12 columns">
				<h3 class="h1">{$item.nadpis}</h3> {$item.popis}
			</div>
		</div>



<div class="row small-up-1 medium-up-3 large-up-4 team">
	{foreach $teams as $team}
	<div class="column">
		<div class="teamate">
			<img src="{$media_path}photos/team/item/images-{$team.id}/{$team.photo_src}-ad.jpg" alt="{$team.name}">
			<div>
				{$team.name}
			</div>
		</div>
	</div>
	{/foreach}
</div> 

		<div class="contactBox">
			<div class="row textPadding">
				<div class="small-12 large-6 columns">
					{static_content code="onas-form"}
				</div>
				<div class="small-12 large-6 columns">
					{widget controller="service" action="show"}
				</div>
			</div>
		</div>
	</article>
</section>
<section class="news">
	{widget controller=article action=homepage_list}
</section>

{*foreach $branches as $branche}
<div class="row">
	<div class="small-12 medium-6 columns">
		<p>
			<strong>{$branche.nadpis}</strong>
			<br/> {$branche.address}
		</p>
	</div>
	<div class="small-12 medium-6 columns">
		<p>
			<strong>
										otevřeno: {$branche.opening_hours}
										<br>
										{$branche.phone}
									</strong>
		</p>
	</div>
</div>
<p>
	{$branche.comment}
	<br> IČO: {$branche.ic} DIČ: {$branche.dic}
</p>
<p>
	{$branche.comment2}
</p>
</div>
{/foreach}
<div class="small-12 large-6 columns">
	{widget controller=contact action=show}
</div>
</div>
</article>
</section>

<section class="persons">
	<div class="row">
		<div class="small-12 columns">
			<div class="border">
				<h2>KONTAKTNÍ OSOBY</h2>
				<div class="row large-up-3 medium-up-2 small-up-1">


					{foreach $employees as $salesman}

					<div class="column">
						<div class="person">
							<div class="row">
								<div class="small-4 columns">
									<img src="{$media_path}photos/branch/salesman/images-{$salesman.id}/{$salesman.photo_src}-ad.png" alt="">
								</div>
								<div class="small-8 columns">
									<p>
										<strong>{$salesman.name}</strong>
										<br> {$salesman.role}
									</p>
									<p>
										<strong>
													{$salesman.phone}
													<br>
													{$salesman.email}
												</strong>
									</p>
								</div>
							</div>
						</div>
					</div>

					{/foreach}


				</div>
			</div>
		</div>
	</div>
</section>

<section class="flex-video map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2606.553644941208!2d17.602532315890798!3d49.20902648390739!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47130ce5a966e733%3A0x57aef7c57a040811!2zUMWvasSNb3ZuYSBuw6HFmWFkw60gVmxrIHMuci5vLg!5e0!3m2!1scs!2scz!4v1468594385340" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
*} {*
<section class="headline">
	<div class="content">
		<h1 class="headline_title">{$item.nadpis}</h1>
	</div>
</section>
{$item.popis}
<section class="contact_info">
	<div class="content">
		<div class="left">
			{foreach $branches as $branche}
			<div class="line_contact">
				<h2 class="title_info">{$branche.nazev}</h2> {if $branche.address} {$branche.address} {/if} {foreach $branche.salesmen as $salesman}
				<div class="info_one">
					{if $salesman.role}
					<h3>{$salesman.role}</h3> {/if}
					<h3><strong>{$salesman.name}</strong></h3> {if $salesman.phone}
					<p><strong>{translate str="Tel."}:</strong></p>
					<p>{$salesman.phone}</p>
					{/if} {if $salesman.phone_mobile}
					<p><strong>{translate str="Mob."}:</strong></p>
					<p>{$salesman.phone_mobile}</p>
					{/if} {if $salesman.email}
					<p><strong>E-mail:</strong></p>
					<address><a href="mailto:{$salesman.email}">{$salesman.email}</a></address>
					{/if}
				</div>
				{/foreach}
			</div>
			{/foreach}
		</div>
	</div>
</section>
{widget controller="contact" action="show"} *} {*capture assign=subnav} {widget controller=page action=subnav} {/capture}
<div class="detail" id="contact">
	<header class="text-center {if $item.photo_src}with-photo{/if}">
		{if $item.photo_src}
		<img alt="{$item.nazev} photo" data-interchange="[{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header.jpg, large]"> {/if}
		<div class="heading">
			<div class="row">
				<div class="small-12 columns">
					<h1>{$item.nadpis|bbcode}</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="row main">
		{if strlen(trim($subnav)) > 0}
		<div class="medium-3 small-12 columns">
			{$subnav}
		</div>
		{/if}
		<div class="medium-{if strlen(trim($subnav)) > 0}9{else}12{/if} small-12 columns">
			{$item.uvodni_popis} {if !empty($branches)}
			<div class="row branches">
				{foreach $branches as $branch}
				<div class="medium-4 small-6 columns branch">
					<h3 class="highlight">{$branch.nazev}</h3>

					<p>
						{nl2br($branch.address)|bbcode}
					</p>
					{if $branch.phone || $branch.email}
					<div class="contact">
						{if $branch.phone}
						<span class="phone highlight">
								{$branch.phone}
							</span> {/if} {if $branch.email}
						<span class="email highlight">
								{$branch.email}
							</span> {/if}
					</div>
					{/if} {if $branch.ic} IČ: {$branch.ic}{if $branch.dic}
					<br>{/if} {/if} {if $branch.dic} DIČ: {$branch.dic} {/if}
				</div>
				{/foreach}
			</div>
			{/if} {$item.popis} {if !empty($employees)}
			<div class="row employees">
				{foreach $employees as $employee}
				<div class="large-4 small-6 employee columns">
					<div class="callout">
						<h4>{$employee.name}
									<small class="light">{$employee.role}</small>
								</h4>
						<div class="contact">
							{if $employee.phone}
							<span class="phone highlight">
								{$employee.phone}
							</span> {/if} {if $employee.email}
							<span class="email highlight">
								{$employee.email}
							</span> {/if}
						</div>
					</div>
				</div>
				{/foreach}
			</div>
			{/if}
			<h1 class="text-center">{translate str="Kontaktní formulář"}</h1> {widget controller=contact action=show}
		</div>
	</div>
</div>
*}
