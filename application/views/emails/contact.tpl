<table cellpadding="5" style="border: none; border-collapse: collapse;">
    <tr>
        <td><strong>Zpráva z kontaktního formuláře www stránek</strong></td>
        <td>{$data.nazev_projektu}</td>
    </tr>
    <tr>
        <td><strong>Název stránky</strong></td>
        <td>{$data.nazev_stranky}</td>
    </tr>
    <tr>
        <td><strong>URL</strong></td>
        <td>{$data.url}</td>
    </tr>

    <tr>
        <td><strong>Jméno a příjmení</strong></td>
        <td>{$data.jmeno} {$data.prijmeni}</td>
    </tr>

    <tr>
        <td><strong>E-mail odesílatele</strong></td>
        <td>{$data.email}</td>
    </tr>

    {if !empty($data.tel)}
        <tr>
            <td><strong>Telefon</strong></td>
            <td>{$data.tel}</td>
        </tr>
    {/if}
    {if !empty($data.stroj)}
        <tr>
            <td><strong>Dotaz na stroj</strong></td>
            <td>{$data.stroj}</td>
        </tr>
    {/if}
    {if !empty($data.od) && !empty($data.do)}
        <tr>
            <td><strong>Termín</strong></td>
            <td>{$data.od} - {$data.do}</td>
        </tr>
    {/if}

    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Text dotazu:</td>
    </tr>
    <tr>
        <td colspan="2">{$data.zprava}</td>
    </tr>
</table>
