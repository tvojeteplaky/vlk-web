<p>Dobrý den,<br><br>
{if $sleva.hodnota > 0}
právě Vám byla přidělena sleva {$sleva.hodnota} % na naše stroje a techniku. <br><br>
{/if}
Pro přihlášení prosím použijte tyto údaje:<br><br>

jmeno: {$user.email}<br>
heslo: {$user.password}<br>
www.pujcovna-vlk.cz<br><br><br>


s pozdravem: <br><br>

Půjčovna nářadí Vlk s.r.o.<br>
e-mail: pujcovna@pujcovna-vlk.cz<br>
Tel.:   800 737 330 - objednávky, rezervace (volání zdarma)     <br>
Mob.: 602 716 120 - servisní služba mimo pracovní dobu </p>