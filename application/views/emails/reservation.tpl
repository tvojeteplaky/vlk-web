<table cellpadding="5" style="border: none; border-collapse: collapse;">
    <tr>
        <td><strong>Zpráva z kontaktního formuláře www stránek</strong></td>
        <td>{$data.nazev_projektu}</td>
    </tr>
    <tr>
        <td><strong>Název stránky</strong></td>
        <td>{$data.nazev_stranky}</td>
    </tr>
    <tr>
        <td><strong>URL</strong></td>
        <td>{$data.url}</td>
    </tr>

    <tr>
        <td><strong>Jméno a příjmení</strong></td>
        <td>{$data.jmeno}</td>
    </tr>
    <tr>
        <td><strong>Firma</strong></td>
        <td>{$data.firma}</td>
    </tr>

    <tr>
        <td><strong>E-mail odesílatele</strong></td>
        <td>{$data.email}</td>
    </tr>

    {if !empty($data.tel)}
        <tr>
            <td><strong>Telefon</strong></td>
            <td>{$data.tel}</td>
        </tr>
    {/if}

    {if !empty($data.od) and not empty($data.do)}
        <tr>
            <td><strong>Termín</strong></td>
            <td>{$data.od} - {$data.do}</td>
        </tr>
    {/if}
    <tr colspan="2">
        <td>
            <strong>Stroje</strong>
            <table style="width:100%;">
                <thead>
                    <tr>
                        <th style="width:20%">#ID</th>
                        <th style="width:60%">Název</th>
                        <th style="width:20%">Cena za den</th>
                    </tr>
                </thead>
                <tbody>
                {foreach from=$products item=product key=key name=name}
                    <tr>
                        <td>{$product.id}</td>
                        <td><a href="{$data.domain}/{$product.nazev_seo}">{$product.nazev}</a></td>
                        <td>{$product.cena_s_dph|currency:$language_code}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Poznámka:</td>
    </tr>
    <tr>
        <td colspan="2">{$data.note}</td>
    </tr>
</table>
