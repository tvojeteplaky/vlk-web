<li class="search">
	<a href="#" data-open="searchModal"><img src="{$media_path}img/topBarSearch.png" alt="Hledat" style="height:25px;">
		<br><span>Hledat</span></a>
</li>
<div class="reveal" id="searchModal" data-reveal>
	<span class="h1">Vyhledávání</span>
	<form action="{$url_base}vysledky-vyhledavani" method="get">
		<div class="input-group">
			<input class="input-group-field" type="text" name="q">
			<div class="input-group-button">
				<input type="submit" class="button" value="Hledat">
			</div>
		</div>
	</form>
	<button class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
