
<div class="detail" id="search">
    <header class="text-center {if $item.photo_src}with-photo{/if}">

        <div class="heading">
            <div class="row">
                <div class="small-12 columns">
                    <h1>{$item.nadpis|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">

        <div class="medium-12 small-12 columns">
            {$item.popis}
            <p>
                Hledaný výraz: <strong>"{$keyword}"</strong>
            </p>

            <div class="row small-up-1 medium-up-2 large-up-4 itemList">
                {foreach from=$search_results key=key item=results name=search}
                    {foreach $results as $result}
                        <div class="column">
    <div class="item row">
        <div class="columns align-bottom">

            <a href="{$url_base}vysledky-vyhledavani?q={$keyword}#detailItem-{$result.id}" onclick="location.replace('{$url_base}vysledky-vyhledavani?q={$keyword}#detailItem-{$result.id}')" data-open="detailItem-{$result.id}">
                <img src="{$result.photo.ad}" alt="">
            </a>
            <a href="{$url_base}vysledky-vyhledavani?q={$keyword}#detailItem-{$result.id}" onclick="location.replace('{$url_base}vysledky-vyhledavani?q={$keyword}#detailItem-{$result.id}')" data-open="detailItem-{$result.id}">


                <h2>
                            {$result.title}
                        </h2> {$result.text}
                
            </a>
            <div class="row link">
                <div class="shrink columns align-middle">
                    <a href="{$url_base}vysledky-vyhledavani?q={$keyword}#detailItem-{$result.id}" onclick="location.replace('{$url_base}vysledky-vyhledavani?q={$keyword}#detailItem-{$result.id}')" data-open="#">Další info</a>
                </div>
                <div class="columns">

                </div>
            </div>
        </div>
    </div>

</div>
                    {/foreach}
                {/foreach}
            </div>
            {if $item.show_contactform}
                <div class="contact-wrapper">
                    <h4>Máte zájem o naše služby?</h4>
                    {widget controller="contact" action="show"}
                </div>
            {/if}
        </div>
    </div>
</div>
{*
<section id="search" class="detail">
    <header class="header-bg">
        <div class="row">
            <div class="small-12 column text-center">
                <h1>{$item.nadpis}</h1>
            </div>
        </div>
    </header>
    <div class="row">
        <div class="small-12 columns">
            <ul class="small-block-grid-1">
                {foreach from=$search_results key=key item=results name=search}
                    {foreach $results as $result}
                        <li>
                            <h3 class="title">{$result.title}</h3>
                            {$result.text}
                            <a href="{$url_base}{$result.nazev_seo}" class="button">
                                {translate str="Pokračovat"}
                            </a>
                        </li>
                    {/foreach}
                {/foreach}
            </ul>
        </div>
    </div>
</section>
*}