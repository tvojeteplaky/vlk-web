<ul class="pagination text-right" role="navigation" aria-label="Pagination">
                    <?php if ($previous_page !== FALSE): ?>
                    <li class="pagination-previous"><a href="<?php echo HTML::chars($page->url($previous_page)) ?>" aria-label="Previous page">Předchozí <span class="show-for-sr">stránka</span></a></li>
                    <?php endif; ?>
                    <?php for ($i = 1; $i <= $total_pages; $i++): ?>
                        <?php if ($i == $current_page): ?>
                        <li class="current"><span class="show-for-sr">Jste na stránce</span> <?php echo $i ?></li>
                        <?php else: ?>
                        <li><a href="<?php echo HTML::chars($page->url($i)) ?>" aria-label="Page <?php echo $i ?>"><?php echo $i ?></a></li>
                        <?php endif ?>
                    <?php endfor ?>
                    <?php if ($next_page !== FALSE): ?>
                    <li class="pagination-next"><a href="<?php echo HTML::chars($page->url($next_page)) ?>" aria-label="Next page">Následující <span class="show-for-sr">stránka</span></a></li>
                    <?php endif; ?>
                </ul>