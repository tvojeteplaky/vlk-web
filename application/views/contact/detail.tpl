        <section>
            <article>
                <div class="row textPadding">
                    <div class="small-12 large-6 columns">
                        <h1>{$item.nadpis}</h1>
{foreach $branches as $branche}
                        <div class="row">
                            <div class="small-12 medium-6 columns">
                                <p>
                                    <strong>{$branche.nadpis}</strong>
									<br/>
                                    {$branche.address}
                                    {if !empty($branche.bank_account)}<br />Číslo účtu: {$branche.bank_account}{/if}
                                </p>
                            </div>
                            <div class="small-12 medium-6 columns">
                                <p>
                                    <strong>
                                        otevřeno: {$branche.opening_hours}
                                        <br>
                                        {$branche.phone}
                                    </strong>
                                </p>
                            </div>
                        </div>
                        <p>
                            {$branche.comment}
                            <br>
                            IČO: {$branche.ic}    DIČ: {$branche.dic}
                        </p>
                        <p>
                            {$branche.comment2}
                        </p>
                    </div>
{/foreach}
                    <div class="small-12 large-6 columns">
            {widget controller=contact action=show}
                    </div>
                </div>
            </article>
        </section>

        <section class="persons">
            <div class="row">
                <div class="small-12 columns">
                    <div class="border">
                        <h2>KONTAKTNÍ OSOBY</h2>
                        <div class="row large-up-3 medium-up-2 small-up-1">


					{foreach $employees as $salesman}

                            <div class="column">
                                <div class="person">
                                    <div class="row">
                                        <div class="small-4 columns">
                                            <img src="{$media_path}photos/branch/salesman/images-{$salesman.id}/{$salesman.photo_src}-ad.png" alt="">
                                        </div>
                                        <div class="small-8 columns">
                                            <p>
                                                <strong>{$salesman.name}</strong>
                                                <br>
                                                {$salesman.role}
                                            </p>
                                            <p>
                                                <strong>
                                                    {$salesman.phone}
                                                    <br>
                                                    <a href="mailto:{$salesman.email}">{$salesman.email}</a>
                                                </strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

					{/foreach}


                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="map">
            <div id="map"></div>
        </section>