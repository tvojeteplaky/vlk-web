{foreach from=$products item=product key=key name=name}
<div class="reveal detailItem" id="detailItem-{$product.id}" data-reveal data-product-item data-animation-in="slide-in-left" data-animation-out="slide-out-right">
    <div class="row">
        <div class="small-12 medium-12 large-5 columns descriptionImg">
            <img src="{$product.photo.ad}" alt="">
        </div>
        <div class="small-12 medium-12 large-7 columns">
            <h2>
                {$product.nazev|strip_tags}
            </h2> 
            <div class="row">
                <div class="small-12 medium-expand columns">
                    <div class="row price text-{if !$product.motohodina}center{else}right{/if}">
                    {if !$product.motohodina}
                        <div class="columns">
                            <p>
                                <strong>{$product.price_half_day|currency:$language_code}</strong>
                                <br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="6h-text"|strip_tags:false}'>6 hodin</span>
                            </p>
                        </div>
                        <div class="columns">
                            <p>
                                <strong>{$product.cena_s_dph|currency:$language_code}</strong>
                                <br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="1den-text"|strip_tags:false}'>1 den</span>
                            </p>
                        </div>
                        <div class="columns">
                            <p>
                                <strong>{$product.price_weekend|currency:$language_code}</strong>
                                <br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="víkend-text"|strip_tags:false}'>víkend</span>
                            </p>
                        </div>
                        {else}
                         <div class="columns">
                            <p>
                                <strong>{$product.cena_s_dph|currency:$language_code}</strong>
                                <br> Motohodina
                            </p>
                        </div>
                        {/if}
                    </div>
                    <div class="row tax">
                        <div class="columns">
                            <span>Ceny jsou uvedeny bez DPH</span>

                        </div>
                    </div>
                </div>
             
                  <div class="small-12 columns show-for-small-only">
                 {if $product.in_stock}
                  <form action="?" method="post">
                        <input type="hidden" name="product_id" value="{$product.id}">
                        <button type="submit" class="button expanded" data-tooltip  class="has-tip top" title="Po objednávní Vás budeme kontaktovat.">Objednat</button>
                        {hana_secured_post action="add_item" module="catalogue"}
                    </form>

                 {else}
                    <a href="#" class="button secondary expanded" data-open="ask-{$product.id}">Poptat termín</a> 
                 {/if}
                 </div>
                <div class="shrink columns show-for-medium">
                    {if $product.in_stock}
                    <form action="?" method="post">
                        <input type="hidden" name="product_id" value="{$product.id}">
                        <button type="submit" class="button expanded" data-tooltip  class="has-tip top" title="Po objednávní Vás budeme kontaktovat.">Objednat</button>
                        {hana_secured_post action="add_item" module="catalogue"}
                    </form>
                    {else}
                    <a href="#" class="button secondary expanded" data-open="ask-{$product.id}">Poptat termín</a> {/if}
                </div>
            </div>
            {$product.popis}
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="orbit" role="region" aria-label="Galerie" data-orbit>
                <ul class="orbit-container">
                    <button class="orbit-previous"><span class="show-for-sr">Předchozí obrázkek</span>&#9664;&#xFE0E;</button>
                    <button class="orbit-next"><span class="show-for-sr">Následující obrázek</span>&#9654;&#xFE0E;</button>
                    {foreach from=$product.photos item=photo key=key name=photo_gallery}
                    <li class="{if $smarty.foreach.photo_gallery.first}is-active {/if}orbit-slide">

                        <img class="orbit-image" src="{$photo.t1}" alt="{$photo.nazev}">
                    </li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="column">
    <div class="item row">
        {if not $product.in_stock}
        <span class="warning label">NEDOSTUPNÉ</span> {elseif $product.new}
        <span class="new label">Novinka</span> {/if}
        <div class="columns align-bottom">

            <a href="#detailItem-{$product.id}" data-open="detailItem-{$product.id}">
                <img src="{$product.photo.ad}" alt="">
            </a>
            <a href="#" data-open="detailItem-{$product.id}">


                <h2>
                            {$product.nazev}
                        </h2> {$product.uvodni_popis}
                <div class="row price">
                {if !$product.motohodina}
                    <div class="columns">
                        <p>
                            <strong>{$product.price_half_day|currency:$language_code}</strong>
                            <br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="6h-text"|strip_tags:false}'>6 hodin</span>
                        </p>
                    </div>
                    <div class="columns">
                        <p>
                            <strong>{$product.cena_s_dph|currency:$language_code}</strong>
                            <br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="1den-text"|strip_tags:false}'>1 den</span>

                        </p>
                    </div>
                    <div class="columns">
                        <p>
                            <strong>{$product.price_weekend|currency:$language_code}</strong>
                            <br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="víkend-text"|strip_tags:false}'>víkend</span>
                        </p>
                    </div>
                {else}
                <div class="columns">
                        <p>
                            <strong>{$product.cena_s_dph|currency:$language_code}</strong>
                            <br> Motohodina
                        </p>
                    </div>
                {/if}
                </div>
            </a>
            <div class="row link">
                <div class="shrink columns align-middle">
                    <a href="#" data-open="detailItem-{$product.id}">Další info</a>
                </div>
                <div class="columns">
                    {if $product.in_stock}
                    <form action="?" method="post">
                        <input type="hidden" name="product_id" value="{$product.id}">
                        <button type="submit" class="button expanded" data-tooltip  class="has-tip top" title="Po objednávní Vás budeme kontaktovat.">Objednat</button>
                        {hana_secured_post action="add_item" module="catalogue"}
                    </form>
                    {else}
                    <a href="#" class="button secondary expanded" data-open="ask-{$product.id}">Poptat termín</a>
                    <div class="reveal" id="ask-{$product.id}" data-reveal data-animation-in="slide-in-left" data-animation-out="slide-out-right">
                        <h2>Poptat termín</h2>
                        <form method="post" class="contactForm" data-abide novalidate>
                            <input type="hidden" name="contactform[stroj]" value="{$product.nazev}">
                            <div class="row">
                                <div class="small-12 medium-6 columns">
                                    <input type="text" id="name" name="contactform[jmeno]" placeholder="Jméno (*)" required value="{if !empty($data.jmeno)}{$data.jmeno}{elseif !empty($shopper.jmeno)}{$shopper.jmeno}{/if}">
                                     <span class="form-error">
          Prosím vyplňte Vaše jméno.
        </span>
                                </div>
                                <div class="small-12 medium-6 columns">
                                    <input type="text" id="surname" name="contactform[prijmeni]" placeholder="Příjmení  (*)" required value="{if !empty($data.prijmeni)}{$data.prijmeni}{elseif !empty($shopper.prijmeni)}{$shopper.prijmeni}{/if}">
                                    <span class="form-error">
          Prosím vyplňte Vaše příjmení.
        </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-12 medium-6 columns">
                                    <input type="tel" id="phone" name="contactform[tel]" placeholder="Telefonní číslo" pattern="phoneNum" required value="{if !empty($data.tel)}{$data.tel}{elseif !empty($shopper.telefon)}{$shopper.telefon}{/if}">
                                     <span class="form-error">
          Prosím vyplňte Vaše telefonní číslo.
        </span>
                                </div>
                                <div class="small-12 medium-6 columns">
                                    <input type="email" id="email" name="contactform[email]" placeholder="E-mailová adresa (*)" required value="{if !empty($data.email)}{$data.email}{elseif !empty($shopper.email)}{$shopper.email}{/if}">
                                    <span class="form-error">
          Prosím vyplňte Vaši e-mailovou adresu.
        </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-12 medium-6 columns">
                                    <label for="contactOd">Od</label>
                                    <input type="date" name="contactform[od]" id="contactOd" required value="{if !empty($data.od)}{$data.od}{/if}">
                                    <span class="form-error">
          Prosím vyplňte od kdy si chcete stroj vypůjčit.
        </span>
                                </div>
                                <div class="small-12 medium-6 columns">
                                    <label for="contactDo">Do</label>
                                    <input id="contactDo" name="contactform[do]" type="date" required value="{if !empty($data.do)}{$data.do}{/if}">
                                    <span class="form-error">
          Prosím vyplňte do kdy si chcete stroj vypůjčit.
        </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-12 columns">
                                    <textarea id="zprava" name="contactform[zprava]" placeholder="Poznámka k Vaší poptávce, popř. dotaz! (*)">{if !empty($data.zprava)}{$data.zprava}{/if}</textarea>
                                </div>
                            </div>
                            <div class="row align-right">
                                <div class="shrink columns">
                                    <label class="show-for-sr" for="contactSubmit">Odeslat zprávu</label>
                                    <button type="submit" class="button">Odeslat zprávu</button>
                                </div>
                            </div>
                            {hana_secured_post action="send" module="contact"}
                        </form>
                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>

</div>
{/foreach}
