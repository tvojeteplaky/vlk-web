<div class="reveal" id="order" data-reveal data-animation-in="slide-in-left" data-animation-out="slide-out-right">
	<h1>Objednávkový formulář{if $total_items>0} ({$total_items} stroj{if $total_items>1}e{/if}){/if}</h1> {if $price_total_products > 0}
	<p>
		Máte dotazy k Vaší objednávce? Potřebujete vědět doplňující info? Rádi Vám
		<br> pomůžeme, kontaktujte nás na <strong>+420 800 737 330</strong>
	</p>
	<form class="orderForm" method="post" action="?" data-abide novalidate>
		<div class="row">
			<div class="small-12 medium-6 columns">
				<label class="show-for-sr" for="orderName">Jméno a příjmení (*)</label>
				<input id="orderName" name="orderform[jmeno]" type="text" placeholder="Jméno a příjmení (*)" required value="{if !empty($data.jmeno)}{$data.jmeno}{elseif !empty($shopper.jmeno) or !empty($shopper.prijmeni)}{$shopper.jmeno} {$shopper.prijmeni}{/if}">
				<span class="form-error">
          Prosím vyplňte Vaše jméno.
        </span>
			</div>
			<div class="small-12 medium-6 columns">
				<label class="show-for-sr" for="orderCompany">Firma (vč. právní úpravy)</label>
				<input id="orderCompany" name="orderform[firma]" type="text" placeholder="Firma (vč. právní úpravy)" value="{if !empty($data.firma)}{$data.firma}{elseif !empty($shopper.nazev)}{$shopper.nazev}{/if}">
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-6 columns">
				<label class="show-for-sr" for="orderTel">Telefonní číslo (*)</label>
				<input id="orderTel" type="tel" name="orderform[tel]" pattern="phoneNum" placeholder="Telefonní číslo (*)" value="{if !empty($data.tel)}{$data.tel}{elseif !empty($shopper.telefon)}{$shopper.telefon}{/if}" required>
				<span class="form-error">
          Prosím vyplňte Vaše telefonní číslo.
        </span>
			</div>
			<div class="small-12 medium-6 columns">
				<label class="show-for-sr" for="orderEmail">E-mailová adresa (*)</label>
				<input id="orderEmail" type="email" name="orderform[email]" placeholder="E-mailová adresa (*)" value="{if !empty($data.email)}{$data.email}{elseif !empty($shopper.email)}{$shopper.email}{/if}" required="">
				<span class="form-error">
          Prosím vyplňte Vaši e-mailovou adresu.
        </span>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-6 columns">
				<label for="contactOd">Od</label>
				<input type="text" name="orderform[od]" id="contactOd" required value="{if !empty($data.od)}{$data.od}{/if}">
				<span class="form-error">
          Prosím vyplňte od kdy si chcete stroj vypůjčit.
        </span>
			</div>
			<div class="small-12 medium-6 columns">
				<label for="contactDo">Do</label>
				<input id="contactDo" name="orderform[do]" type="text" required value="{if !empty($data.do)}{$data.do}{/if}">
				<span class="form-error">
          Prosím vyplňte do kdy si chcete stroj vypůjčit.
        </span>
			</div>
		</div>
		<div class="row">
			<div id="owlOrder" class="owl-carousel">
				{foreach from=$cart_products item=product key=key name=name}
				<div>
					<a href="{$url_base}{$product.nazev_seo}" title="{$product.nazev|strip_tags:true}">
						<div class="item" style="background-image: url('{$product.photo.t1}');">
							<button class="close-button removeItem" data-id="{$product.id}">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<span class="productName">{$product.nazev}</span>
					</a>
				</div>
				{/foreach}
				<div>
					<a href="{$url_base}pujcovna">
						<div class="item next">
							<span>
								PŘIDAT
								<br>
								DALŠÍ 
								<br>
								STROJ
							</span>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="row align-right">
			<div class="shrink columns">
				<label class="show-for-sr" for="orderSubmit">Odeslat zprávu</label>
				<button id="orderSubmit" class="button" type="submit">Odeslat objednávku</button>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<label class="show-for-sr" for="orderText">Poznámky k objednávce</label>
				<textarea id="orderText" name="orderform[note]" placeholder="Poznámky k objednávce">{if !empty($data.note)}{$data.note}{/if}</textarea>
			</div>
		</div>
		{hana_secured_post action="send_reservation" module="catalogue"}
	</form>
	<form action="?" method="post" id="removeItem" class="hide">
		<input type="hidden" name="product_id" value=""> {hana_secured_post action="remove_item" module="catalogue"}
	</form>
	{else}
	<p>
		Vaše objednávka je prozatím prázdná
	</p>
	<div class="row align-right">
		<div class="shrink columns">
			<a href="{$url_base}pujcovna" class="button">Pokračovat do půjčovny</a>
		</div>
	</div>
	{/if}
	<button class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
