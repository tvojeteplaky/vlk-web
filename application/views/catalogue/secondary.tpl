<nav class="shrink columns sideNav show-for-large">
	{foreach from=$links item=uplink key=key name=name}
		<ul class="menu vertical arrowList">
			{foreach from=$uplink.children item=link key=key name=name}
				<li {if array_key_exists($link.nazev_seo,$sel_links)}class="active"{/if}><a href="{$url_base}{$link.nazev_seo}">{$link.nazev}</a></li>
			{/foreach}
		</ul>
	{/foreach}
</nav>
