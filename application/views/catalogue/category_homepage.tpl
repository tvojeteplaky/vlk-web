    <section class="category">
        <div class="row textPadding">
            <div class="small-12 columns">
                <h1>{$item.nadpis}</h1>
            </div>
        </div>
        <div class="row small-up-1 medium-up-3 large-up-4">
		{foreach from=$items item=item}
            <div class="column">
                <a href="{$url_base}{$item.nazev_seo}">
                    <div class="items" style="background-image: url('{$item.photo.ad}');">
                        <h3>{$item.nazev}</h3>
                    </div>
                </a>
            </div>
        {/foreach}
        </div>
    </section>
  <section class="news">
	{widget controller=article action=homepage_list}
  </section>
