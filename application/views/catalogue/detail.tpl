<div class="row">

	{widget controller="catalogue" action="second_subnav"}

	<section class="columns">
		<header class="row categoryDetail">
			<div class="small-12 columns">
				<h1>{$item.nazev}</h1>
				{widget controller="navigation" action="breadcrumbs"}
			</div>
		</header>
		<div class="row small-up-1 medium-up-2 large-up-3 itemList">
		{$products}
		</div>
	</section>
</div>
  <section class="news">
	{widget controller=article action=homepage_list}
  </section>