<a href="#" class="hide">Submenu</a>
<div class="accordion-content" id="sumbmenucontent" data-tab-content>
<ul class="menu">
                  <li>
<div class="drop">
	<div class="row">
	{foreach from=$links item=link key=key name=name}
		<div class="small-12 large-4 columns">
			<div>
				<h2>{$link.nazev}</h2>

				<ul class="arrowList">
				{foreach from=$link.children item=child key=key name=name}
					<li><a href="{$url_base}{$child.nazev_seo}">{$child.nazev}</a></li>
				{/foreach}
				</ul>
			</div>
		</div>
	{/foreach}
	</div>
</div>
	</li>
	</ul>
	</div>