    <div class="row">
      <div class="small-12 columns">
        <h2>NAŠE NABÍDKA</h2>
      </div>
    </div>
    <div class="rowBig">
      <div id="owlOffer" class="owl-carousel">
		{foreach from=$categories item=category}
        <div>
          <a href="{$url_base}{$category.nazev_seo}">
            <div class="offer" >
              <img src="{$category.photo.ad}" alt="{$category.nazev}">
              <h3>{$category.nazev}</h3>
              <p>{$category.popis}</p>
            </div>
          </a>
        </div>
        {/foreach}
	  </div>
    </div>
    <div class="row align-center">
      <div class="shrink columns">
        <a href="/pujcovna" class="button arrow">PŘEJÍT DO PŮJČOVNY</a>
      </div>
    </div>
