<div class="reveal detailItem" id="detailItem-{$item.id}" data-reveal data-product-item data-animation-in="slide-in-left" data-animation-out="slide-out-right">
	<div class="row">
		<div class="small-12 medium-12 large-5 columns descriptionImg">
			<img src="{$item.photo.ad}" alt="">
		</div>
		<div class="small-12 medium-12 large-7 columns">
			<h2>
				{$item.nazev|strip_tags}
			</h2>
			<div class="row">
				<div class="small-12 medium-expand columns">
					<div class="row price text-{if !$item.motohodina}center{else}right{/if}">

						{if !$item.motohodina}
						<div class="columns">
							<p>
								<strong>{$item.price_half_day|currency:$language_code}</strong>
								<br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="6h-text"|strip_tags:false}'>6 hodin</span>
							</p>
						</div>
						<div class="columns">
							<p>
								<strong>{$item.cena_s_dph|currency:$language_code}</strong>
								<br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="1den-text"|strip_tags:false}'>1 den</span>
							</p>
						</div>
						<div class="columns">
							<p>
								<strong>{$item.price_weekend|currency:$language_code}</strong>
								<br> <span data-tooltip aria-haspopup="true" class="has-tip top" data-disable-hover="false" tabindex="2" title='{static_content code="víkend-text"|strip_tags:false}'>víkend</span>
							</p>
						</div>
						{else}
						<div class="columns">
							<p>
								<strong>{$item.cena_s_dph|currency:$language_code}</strong>
								<br> Motohodina
							</p>
						</div>
						{/if}

					</div>
					<div class="row tax">
						<div class="columns">
							<span>Ceny jsou uvedeny bez DPH</span>

						</div>
					</div>
				</div>

				<div class="small-12 columns show-for-small-only">
					{if $item.in_stock}
					<form action="?" method="post">
						<input type="hidden" name="product_id" value="{$item.id}">
						<button type="submit" class="button expanded" data-tooltip class="has-tip top" title="Po objednávní Vás budeme kontaktovat.">Objednat</button>
						{hana_secured_post action="add_item" module="catalogue"}
					</form>

					{else}
					<a href="#" class="button secondary expanded" data-open="ask-{$item.id}">Poptat termín</a> {/if}
				</div>
				<div class="shrink columns show-for-medium">
					{if $item.in_stock}
					<form action="?" method="post">
						<input type="hidden" name="product_id" value="{$item.id}">
						<button type="submit" class="button expanded" data-tooltip class="has-tip top" title="Po objednávní Vás budeme kontaktovat.">Objednat</button>
						{hana_secured_post action="add_item" module="catalogue"}
					</form>
					{else}
					<a href="#" class="button secondary expanded" data-open="ask-{$item.id}">Poptat termín</a> {/if}
				</div>
			</div>
			{$item.popis}
		</div>
	</div>
	{if !empty($item.photos)}

	<div class="row">
		<div class="small-12 columns">
			<div class="orbit" role="region" aria-label="Galerie" data-orbit>
				<ul class="orbit-container">
					<button class="orbit-previous"><span class="show-for-sr">Předchozí obrázkek</span>&#9664;&#xFE0E;</button>
					<button class="orbit-next"><span class="show-for-sr">Následující obrázek</span>&#9654;&#xFE0E;</button>
					{foreach from=$item.photos item=photo key=key name=photo_gallery}
					<li class="{if $smarty.foreach.photo_gallery.first}is-active {/if}orbit-slide">

						<img class="orbit-image" src="{$photo.t1}" alt="{$photo.nazev}">
					</li>
					{/foreach}
				</ul>
			</div>
		</div>
	</div>
	{/if}
	<button class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
