{if !empty($sliders) && !empty($sliders[0]['slides'])}
  {foreach $sliders as $slider}
	
    <div class="orbit" role="region" aria-label="Header" data-orbit data-timer-delay="3000" data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out">
      <ul class="orbit-container">
        <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
        <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
        {foreach $slider.slides as $slide name=slides}
        <li class="is-active orbit-slide">
          <div class="slide" style="background-image: url('{$media_path}photos/slider/item/images-{$slide.id}/{$slide.photo_src}-t1.jpg');">
            <div class="row align-right">
              <div class="shrink columns">
                <span class="h1">{$slide.nadpis|bbcode}</span>
                {if !empty($slide.text)}{$slide.text}{else}<p></p>{/if}
                <a href="{$slide.link}" class="button arrow">{$slide.button_text}</a>
              </div>
            </div>
          </div>
        </li>
         {/foreach}
      </ul>
    </div>
 

{*
        <div class="orbit orbit-slider" role="region" aria-label="{$slider.nazev}" {if count($slider.slides) > 1}data-orbit{/if}>
            <ul class="orbit-container">
                {foreach $slider.slides as $slide name=slides}
                    {if $slide.photo_src}
                        <li class="{if $smarty.foreach.slides.first}is-active{/if} orbit-slide">
                            {if $slide.link && !$slide.button_text}
                            <a href="{$slide.link}">
                                {/if}
                                <div class="slide">
                                    <img class="orbit-image" src="{$media_path}photos/slider/item/images-{$slide.id}/{$slide.photo_src}-t1.jpg" alt="{$slide.nazev}">

                                    <div class="content-wrapper">
                                        <div class="row column">
                                            <div class="content">
                                                {if $slide.nadpis}
                                                    <h2 >{$slide.nadpis|bbcode}</h2>
                                                {/if}
                                                {$slide.text}
                                                {if $slide.button_text}
                                                    <a href="{$slide.link}" class="success button link">{$slide.button_text}</a>
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {if $slide.link && !$slide.button_text}
                            </a>
                            {/if}
                        </li>
                    {/if}
                {/foreach}
            </ul>
            {if count($slider.slides) > 1}
                <nav class="orbit-bullets">
                    {foreach $slider.slides as $slide name=slides}
                        <button {if $smarty.foreach.slides.first}class="is-active"{/if} data-slide="{$smarty.foreach.slides.index}"></button>
                    {/foreach}
                </nav>
            {/if}
        </div>
*}
    {/foreach}
{/if}
