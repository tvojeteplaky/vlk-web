{if !empty($messages)}

<div class="reveal" id="revealMessage" data-reveal>
  {foreach $messages as $message}
            <p>{$message}</p>
        {/foreach}
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
{* <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Open the modal
modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
 *}
{* <div class="reveal" id="message-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
        {foreach $messages as $message}
            <p>{$message}</p>
        {/foreach}
        <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <script type="text/javascript">
        $(function() {
           $('#message-modal').foundation('open');
        });
    </script> *}
{/if}
