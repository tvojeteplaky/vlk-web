 {if not empty($item.files)}
 <section class="download">
            <div class="row small-up-1 medium-up-2 large-up-3">
            {foreach from=$item.files item=file key=key name=page_files}
                <div class="column">
                    <div class="item pdf">
                        <h2>{$file.nazev}</h2>
                        <a href="{$file.file}"class="button" download="download">Stáhnout soubor</a>
                    </div>
                </div>
            {/foreach}
            </div>
        </section>
{else}
<section>
    <article>
        <div class="row textPadding">
            <div class="small-12 columns">
                <h1>{$item.nadpis}</h1> {$item.popis}
            </div>
        </div>
    </article>
</section>
 {/if}

{*capture assign=subnav} {widget controller=page action=subnav} {/capture}
<div class="detail" id="page">
    <header class="text-center {if $item.photo_src}with-photo{/if}">
        {if $item.photo_src}
        <img alt="{$item.nazev} photo" data-interchange="[{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header.jpg, large]"> {/if}
        <div class="heading {if $item.header_right}to-right{/if}">
            <div class="row column">
                <div class="content {if !$item.header_right}text-center{else}text-left{/if}">
                    <h1>{$item.nadpis|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">
        {if strlen(trim($subnav)) > 0}
        <div class="medium-3 small-12 columns">
            {$subnav}
        </div>
        {/if}
        <div class="medium-{if strlen(trim($subnav)) > 0}9{else}12{/if} small-12 columns">
            {$item.popis} {widget controller="gallery" action="carousel_widget" param="full-preview"} {if $item.show_contactform}
            <div class="contact-wrapper">
                <h4>Máte zájem o naše služby?</h4> {widget controller="contact" action="show"}
            </div>
            {/if}
        </div>
    </div>
</div>
*} {*
<article class="row info detail" id="page">
    <div class="small-12 column">
        <div class="row">
            <div class="small-12 column">
                {if $item.photo_src && !$item.in_header}
                <img src="{$media_path}photos/page/{if $item.page_category_id == 3}item{elseif $item.page_category_id == 2}unrelated{/if}/images-{$item.id}/{$item.photo_src}-t1.png" alt="{$item.nazev} photo" class="image"> {/if} {$item.popis}
            </div>
        </div>

        {if $item.link}
        <div class="row">
            <div class="small-12 column text-center">
                <a href="{$item.link}" class="button link">{$item.link_text}</a>
                <br>
            </div>
        </div>
        {/if} {capture assign=gallery} {if $item.parent_id == 0} {widget controller=gallery action=default_widget param='detail-preview'} {else} {widget controller=gallery action=preview_widget param='detail-preview-big_preview'} {/if} {/capture} {if $item.show_map}
        <div class="row">
            <div class="small-12 column">
                {widget controller=contact action=map}
            </div>
        </div>
        {/if} {if strlen(trim($gallery)) > 0}
        <div class="row">
            <div class="medium-6 column">
                <h2 class="margin">{translate str="Fotogalerie"}</h2> {$gallery}
            </div>
            <div class="medium-6 column">
                <h2 class="margin">{translate str="Napište nám"}</h2> {widget controller=contact action=show}
            </div>
        </div>
        {else}
        <div class="row">
            <div class="small-12 column">
                {widget controller=contact action=show}
            </div>
        </div>
        {/if}
    </div>
</article>
*}
