  <header>
    {widget controller="slider"}
  </header>
  <section class="ourOffer">
		{widget controller="catalogue" action="homepage_widget"}
  </section>
  <section>
    <article class="whyUs">
		{widget controller="reason" action="widget"}
    </article>
  </section>
  <section class="news">
	{widget controller=article action=homepage_list}
  </section>

{*
<div id="page" class="homepage">
    <section class="why-we">
        <div class="row">
            <div class="medium-12 small-12 large-3 columns">
                <h3>Proč právě my?</h3>
            </div>
            <div class="medium-12 small-12 large-9 columns">
                <ul>
                    <li>
                        <img src="{$media_path}img/thumb.png" alt="Thumb up" class="float-left">
                        Kvalitní<br><span class="highlight">technické zázemí</span>
                    </li>
                    <li>
                        <img src="{$media_path}img/person.png" alt="Man's head" class="float-left">
                        Odborně<br><span class="highlight">připravení pracovníci</span>
                    </li>
                    <li>
                        <img src="{$media_path}img/bagr.png" alt="Bagr" class="float-left">
                        Rozsáhlý<br><span class="highlight">strojový park</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    {widget controller=box}
    {widget controller=reference}
    <section class="about-us dark-bg">
        <div class="row">
            <div class="small-12 columns text-center">
                <h3><span class="thin">O naší</span> společnosti</h3>
            </div>
        </div>
        <div class="row">
            <div class="columns {if $item.photo_src}medium-6{else}small-12{/if}">
                {$item.popis}
            </div>
            {if $item.photo_src}
                <div class="columns medium-6">
                    <img src="{$media_path}photos/page/item/images-{$item.id}/{$item.photo_src}-homepage.jpg" alt="Naše společnost photo">
                </div>
            {/if}
        </div>
    </section>
</div>
*}
