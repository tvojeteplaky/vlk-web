    <div class="row textPadding">
      <div class="small-12 columns">
        <h2>
          Novinky
        </h2>
      </div>
    </div>
    <div class="row small-up-1 medium-up-2 large-up-3">
{if !empty($items)}
	{foreach $items as $item name=iter}
      <div class="column">
        <a href="{$url_base}{$item.nazev_seo}">
          <div class="newsBox" style="background-image: url('{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-t1.jpg');">
            <h3>{$item.nazev}</h3>
            <p>
              {$item.date|date_format:'%d. %m. %Y'}
              <br>
              {$item.uvodni_popis|strip_tags:true}
            </p>
          </div>
        </a>
      </div>
	{/foreach}

{*
    <div class="articles">
        <div class="row">
            <div class="small-12 column text-center">
                <h3><span class="thin">Co je u nás</span> nového?</h3>
            </div>
        </div>
        <div class="row">
            {foreach $items as $item name=iter}
                <div class="large-4 medium-6 small-12 columns">
                    <a href="{$url_base}{$item.nazev_seo}">
                        <div class="media-box">
                            {if $item.photo_src}
                                <img src="{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-preview.jpg" alt="{$item.nazev} photo" class="float-left">
                            {/if}
                            <h5 class="highlight">{$item.nazev}</h5>
                            {$item.uvodni_popis}
                            {if $item.date}
                                <span class="date">
                                {$item.date|date_format:'%e. %m. %Y'}
                            </span>
                            {/if}
                        </div>
                    </a>
                </div>
            {/foreach}
        </div>
        <div class="row">
            <div class="small-12 column text-center">
                <a href="{$url_base}articles" class="button not-transition"><strong>Veškeré novinky</strong></a>
            </div>
        </div>
    </div>
*}

{/if}
    </div>
    <div class="row align-center">
      <div class="shrink columns">
        <a href="/novinky" class="button">VEŠKERÉ NOVINKY</a>
      </div>
    </div>
