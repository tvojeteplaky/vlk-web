  <section class="news">
	<div class="row">
        <h2>
          {$item.nazev}
        </h2>
    </div>
    <div class="row small-up-1 medium-up-2 large-up-3">
		{$item.popis}
    </div>
  </section>
{*
<div class="detail" id="article">
    <header class="text-center {if $item.photo_src}with-photo{/if}">
        {if $item.photo_src}
            <img alt="{$item.nazev} photo"
                 data-interchange="[{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-header.jpg, large]">
        {/if}
        <div class="heading">
            <div class="row">
                <div class="small-12 columns">
                    <h1>{$item.nadpis|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">
        <div class="medium-3 small-12 columns">
            <ul class="menu vertical">
                <li>
                    <a href="{$url_base}articles/{$item.date|date_format:"Y"}" class="back">
                        Zpátky na seznam novinek
                    </a>
                </li>
            </ul>
        </div>
        <div class="medium-9 small-12 columns">
            {$item.popis}
            {widget controller="gallery" action="carousel_widget" param="full-preview"}
            {if $item.show_contactform}
                <div class="contact-wrapper">
                    <h4>Máte zájem o naše služby?</h4>
                    {widget controller="contact" action="show"}
                </div>
            {/if}
        </div>
    </div>
</div>
*}
{*
<article class="row info detail" id="article">
    <div class="small-12 column">
        <div class="row">
            <div class="small-12 column">
                {if $item.photo_src}
                    <img src="{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-t1.png" alt="{$item.nazev} photo" class="image">
                {/if}
                {$item.popis}
            </div>
        </div>
        {capture assign=gallery}
            {widget controller=gallery action=carousel_widget param='detail-preview'}
        {/capture}
        {if strlen(trim($gallery)) > 0}
            <div class="row">
                <div class="small-12 column">
                    <h3 class="margin">{translate str="Fotogalerie"}</h3>
                    {$gallery}
                </div>
            </div>
        {/if}
    </div>
</article>
{if $item.show_contactform}
    <div class="row">
        <div class="small-12 column">
            {widget controller=contact action=show}
        </div>
    </div>
{/if}
*}
