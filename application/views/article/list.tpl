  <section class="news">
    <div class="row textPadding">
      <div class="small-12 columns">
        <h2>
          Novinky
        </h2>
      </div>
    </div>
    <div class="row small-up-1 medium-up-2 large-up-3">
{if !empty($items)}
	{foreach $items as $item name=iter}
      <div class="column">
        <a href="{$url_base}{$item.route.nazev_seo}">
          <div class="newsBox" style="background-image: url('{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-ad.jpg');">
            <h3>{$item.nazev}</h3>
            <p>
              {$item.date|date_format:'%d. %m. %Y'}
              <br>
              {$item.uvodni_popis}
            </p>
          </div>
        </a>
      </div>
	{/foreach}
{/if}
    </div>
  </section>

{*
<div class="list" id="article">
    <header class="text-center {if $item.photo_src}with-photo{/if}">
        {if $item.photo_src}
            <img alt="{$item.nazev} photo"
                 data-interchange="[{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header.jpg, large]">
        {/if}
        <div class="heading">
            <div class="row">
                <div class="small-12 columns">
                    <h1>{$item.nadpis|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">
        <div class="medium-3 small-12 columns">
            <ul class="menu vertical">
                {for $i=$latest_year to $oldest_year step -1}
                    <li class="{if $year == $i}active{/if}">
                        <a href="{$url_actual}/{$i}" class="back">
                            Archív {$i}
                        </a>
                    </li>
                {/for}
            </ul>
        </div>
        <div class="medium-9 small-12 columns">
            {$item.popis}
            <div class="row items">
                {foreach $items as $item}
                    <div class="small-12 columns">
                        <a href="{$url_base}{$item.route.nazev_seo}">
                            <div class="transparent media-box">
                                {if $item.photo_src}
                                    <img src="{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-preview.jpg" alt="{$item.nazev} photo" class="float-left">
                                {/if}
                                <h5 class="highlight">{$item.nazev}</h5>
                                {$item.uvodni_popis}
                                {if $item.date}
                                    <span class="date">
                                {$item.date|date_format:'%e. %m. %Y'}
                            </span>
                                {/if}
                            </div>
                        </a>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>
*}
{*
<article class="list">
    {if !empty($items)}
        {foreach $items as $item}
            <div class="row element-list">
                {if $item.photo_src}
                    <div class="columns medium-5">
                        <img src="{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-t1.png" alt="{$item.nazev} photo">
                    </div>
                {/if}
                <div class="columns medium-{if $item.photo_src}7{else}12{/if}">
                    <h3>{$item.nazev}</h3>

                    <div class="write">{$item.date|date_format:'%e. %m. %Y'}{if $item.author} {translate str="napsal"} {$item.author}{/if}</div>
                    <div class="text">
                        {$item.uvodni_popis}
                    </div>
                    <a href="{$url_base}{$item.nazev_seo}">{translate str="Celý článek"} >></a>
                </div>
            </div>
        {/foreach}
    {/if}
    <div>
        <button>Načíst další</button>
    </div>
</article>
    *}
