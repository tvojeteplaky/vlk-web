      <div class="row">
        <div class="small-12 columns">
          <h2>6 DŮVODŮ PROČ PRÁVĚ MY?</h2>
        </div>
      </div>
      <div class="row small-up-1 medium-up-2 large-up-3">
{if !empty($data)}
	{foreach from=$data item=reason key=key}
        <div class="column">
          <div class="row">
            <div class="shrink columns">
              <span>{$reason.zlomek}</span>
            </div>
            <div class="columns">
              <h3>{$reason.nadpis}</h3>
              {$reason.obsah}
            </div>
          </div>
        </div>
	{/foreach}
{/if}
      </div>

        
      
      <div class="row align-center">
        <div class="shrink columns">
           {if $controller == "page"} <a href="/o-nas" class="button arrow">PROČ PRÁVĚ MY</a>{/if}
        </div>
      </div>
